﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PointsTutorial
struct PointsTutorial_t2107975041;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// UnityEngine.AudioClip
struct AudioClip_t794140988;

#include "codegen/il2cpp-codegen.h"

// System.Void PointsTutorial::.ctor()
extern "C"  void PointsTutorial__ctor_m678185018 (PointsTutorial_t2107975041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PointsTutorial::Start()
extern "C"  void PointsTutorial_Start_m3920290106 (PointsTutorial_t2107975041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PointsTutorial::showCards()
extern "C"  Il2CppObject * PointsTutorial_showCards_m3809626006 (PointsTutorial_t2107975041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PointsTutorial::Update()
extern "C"  void PointsTutorial_Update_m1275761171 (PointsTutorial_t2107975041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioClip PointsTutorial::getTutorialClip()
extern "C"  AudioClip_t794140988 * PointsTutorial_getTutorialClip_m1261249464 (PointsTutorial_t2107975041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PointsTutorial::OnDestroy()
extern "C"  void PointsTutorial_OnDestroy_m2617995315 (PointsTutorial_t2107975041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
