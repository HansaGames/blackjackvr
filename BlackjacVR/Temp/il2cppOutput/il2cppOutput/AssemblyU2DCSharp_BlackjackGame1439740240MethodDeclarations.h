﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BlackjackGame
struct BlackjackGame_t1439740240;
// Blackjack.Game
struct Game_t539648204;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// Blackjack.Card[]
struct CardU5BU5D_t1346973999;
// System.Collections.Generic.List`1<Blackjack.Decision>
struct List_1_t2307438470;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Blackjack_Game539648204.h"
#include "AssemblyU2DCSharp_Blackjack_Card539529194.h"
#include "AssemblyU2DCSharp_Blackjack_GameResult3329670025.h"

// System.Void BlackjackGame::.ctor()
extern "C"  void BlackjackGame__ctor_m957622811 (BlackjackGame_t1439740240 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BlackjackGame::newGame(Blackjack.Game,System.Int32)
extern "C"  void BlackjackGame_newGame_m3304798190 (BlackjackGame_t1439740240 * __this, Game_t539648204 * ___game0, int32_t ___bid1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator BlackjackGame::initCardActionUpdate()
extern "C"  Il2CppObject * BlackjackGame_initCardActionUpdate_m2253292960 (BlackjackGame_t1439740240 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator BlackjackGame::drawNewPlayerCards(Blackjack.Card[])
extern "C"  Il2CppObject * BlackjackGame_drawNewPlayerCards_m846017669 (BlackjackGame_t1439740240 * __this, CardU5BU5D_t1346973999* ___cards0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator BlackjackGame::showDealerCards()
extern "C"  Il2CppObject * BlackjackGame_showDealerCards_m777934318 (BlackjackGame_t1439740240 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator BlackjackGame::drawNewDealerCards(Blackjack.Card[],System.Boolean)
extern "C"  Il2CppObject * BlackjackGame_drawNewDealerCards_m1460387216 (BlackjackGame_t1439740240 * __this, CardU5BU5D_t1346973999* ___cards0, bool ___showResult1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BlackjackGame::updateGameButtonStates(System.Collections.Generic.List`1<Blackjack.Decision>)
extern "C"  void BlackjackGame_updateGameButtonStates_m2526493030 (BlackjackGame_t1439740240 * __this, List_1_t2307438470 * ___options0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject BlackjackGame::cardGameObject(Blackjack.Card,System.Boolean)
extern "C"  GameObject_t3674682005 * BlackjackGame_cardGameObject_m2141063592 (BlackjackGame_t1439740240 * __this, Card_t539529194  ___card0, bool ___isPlayerCard1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String BlackjackGame::getCardGameObjectTitle(Blackjack.Card)
extern "C"  String_t* BlackjackGame_getCardGameObjectTitle_m1109689639 (BlackjackGame_t1439740240 * __this, Card_t539529194  ___card0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BlackjackGame::clearObjects()
extern "C"  void BlackjackGame_clearObjects_m3976937456 (BlackjackGame_t1439740240 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator BlackjackGame::playerDidWonGame()
extern "C"  Il2CppObject * BlackjackGame_playerDidWonGame_m904035787 (BlackjackGame_t1439740240 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator BlackjackGame::playerDidLostGame()
extern "C"  Il2CppObject * BlackjackGame_playerDidLostGame_m773920725 (BlackjackGame_t1439740240 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator BlackjackGame::playerPlayDrawGame()
extern "C"  Il2CppObject * BlackjackGame_playerPlayDrawGame_m2028383180 (BlackjackGame_t1439740240 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator BlackjackGame::endGame()
extern "C"  Il2CppObject * BlackjackGame_endGame_m3760595918 (BlackjackGame_t1439740240 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator BlackjackGame::playerDidFinishGame(Blackjack.GameResult)
extern "C"  Il2CppObject * BlackjackGame_playerDidFinishGame_m899909733 (BlackjackGame_t1439740240 * __this, GameResult_t3329670025  ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BlackjackGame::updateAllAfterAction()
extern "C"  void BlackjackGame_updateAllAfterAction_m676437283 (BlackjackGame_t1439740240 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BlackjackGame::hit()
extern "C"  void BlackjackGame_hit_m224569388 (BlackjackGame_t1439740240 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BlackjackGame::stand()
extern "C"  void BlackjackGame_stand_m2534940079 (BlackjackGame_t1439740240 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BlackjackGame::doubleAction()
extern "C"  void BlackjackGame_doubleAction_m4063397712 (BlackjackGame_t1439740240 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
