﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<Blackjack.Card>
struct List_1_t1907714746;
// System.Func`2<Blackjack.Card,Blackjack.Card>
struct Func_2_t1086561661;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Blackjack.Game
struct  Game_t539648204  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<Blackjack.Card> Blackjack.Game::deck
	List_1_t1907714746 * ___deck_0;
	// System.Collections.Generic.List`1<Blackjack.Card> Blackjack.Game::dealerCards
	List_1_t1907714746 * ___dealerCards_1;
	// System.Collections.Generic.List`1<Blackjack.Card> Blackjack.Game::playerCards
	List_1_t1907714746 * ___playerCards_2;
	// System.Double Blackjack.Game::chips
	double ___chips_3;
	// System.Boolean Blackjack.Game::canDouble
	bool ___canDouble_4;
	// System.Boolean Blackjack.Game::playerOver
	bool ___playerOver_5;
	// System.Int32 Blackjack.Game::playerPoints
	int32_t ___playerPoints_6;
	// System.Int32 Blackjack.Game::dealerPoints
	int32_t ___dealerPoints_7;

public:
	inline static int32_t get_offset_of_deck_0() { return static_cast<int32_t>(offsetof(Game_t539648204, ___deck_0)); }
	inline List_1_t1907714746 * get_deck_0() const { return ___deck_0; }
	inline List_1_t1907714746 ** get_address_of_deck_0() { return &___deck_0; }
	inline void set_deck_0(List_1_t1907714746 * value)
	{
		___deck_0 = value;
		Il2CppCodeGenWriteBarrier(&___deck_0, value);
	}

	inline static int32_t get_offset_of_dealerCards_1() { return static_cast<int32_t>(offsetof(Game_t539648204, ___dealerCards_1)); }
	inline List_1_t1907714746 * get_dealerCards_1() const { return ___dealerCards_1; }
	inline List_1_t1907714746 ** get_address_of_dealerCards_1() { return &___dealerCards_1; }
	inline void set_dealerCards_1(List_1_t1907714746 * value)
	{
		___dealerCards_1 = value;
		Il2CppCodeGenWriteBarrier(&___dealerCards_1, value);
	}

	inline static int32_t get_offset_of_playerCards_2() { return static_cast<int32_t>(offsetof(Game_t539648204, ___playerCards_2)); }
	inline List_1_t1907714746 * get_playerCards_2() const { return ___playerCards_2; }
	inline List_1_t1907714746 ** get_address_of_playerCards_2() { return &___playerCards_2; }
	inline void set_playerCards_2(List_1_t1907714746 * value)
	{
		___playerCards_2 = value;
		Il2CppCodeGenWriteBarrier(&___playerCards_2, value);
	}

	inline static int32_t get_offset_of_chips_3() { return static_cast<int32_t>(offsetof(Game_t539648204, ___chips_3)); }
	inline double get_chips_3() const { return ___chips_3; }
	inline double* get_address_of_chips_3() { return &___chips_3; }
	inline void set_chips_3(double value)
	{
		___chips_3 = value;
	}

	inline static int32_t get_offset_of_canDouble_4() { return static_cast<int32_t>(offsetof(Game_t539648204, ___canDouble_4)); }
	inline bool get_canDouble_4() const { return ___canDouble_4; }
	inline bool* get_address_of_canDouble_4() { return &___canDouble_4; }
	inline void set_canDouble_4(bool value)
	{
		___canDouble_4 = value;
	}

	inline static int32_t get_offset_of_playerOver_5() { return static_cast<int32_t>(offsetof(Game_t539648204, ___playerOver_5)); }
	inline bool get_playerOver_5() const { return ___playerOver_5; }
	inline bool* get_address_of_playerOver_5() { return &___playerOver_5; }
	inline void set_playerOver_5(bool value)
	{
		___playerOver_5 = value;
	}

	inline static int32_t get_offset_of_playerPoints_6() { return static_cast<int32_t>(offsetof(Game_t539648204, ___playerPoints_6)); }
	inline int32_t get_playerPoints_6() const { return ___playerPoints_6; }
	inline int32_t* get_address_of_playerPoints_6() { return &___playerPoints_6; }
	inline void set_playerPoints_6(int32_t value)
	{
		___playerPoints_6 = value;
	}

	inline static int32_t get_offset_of_dealerPoints_7() { return static_cast<int32_t>(offsetof(Game_t539648204, ___dealerPoints_7)); }
	inline int32_t get_dealerPoints_7() const { return ___dealerPoints_7; }
	inline int32_t* get_address_of_dealerPoints_7() { return &___dealerPoints_7; }
	inline void set_dealerPoints_7(int32_t value)
	{
		___dealerPoints_7 = value;
	}
};

struct Game_t539648204_StaticFields
{
public:
	// System.Func`2<Blackjack.Card,Blackjack.Card> Blackjack.Game::<>f__am$cache8
	Func_2_t1086561661 * ___U3CU3Ef__amU24cache8_8;
	// System.Func`2<Blackjack.Card,Blackjack.Card> Blackjack.Game::<>f__am$cache9
	Func_2_t1086561661 * ___U3CU3Ef__amU24cache9_9;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache8_8() { return static_cast<int32_t>(offsetof(Game_t539648204_StaticFields, ___U3CU3Ef__amU24cache8_8)); }
	inline Func_2_t1086561661 * get_U3CU3Ef__amU24cache8_8() const { return ___U3CU3Ef__amU24cache8_8; }
	inline Func_2_t1086561661 ** get_address_of_U3CU3Ef__amU24cache8_8() { return &___U3CU3Ef__amU24cache8_8; }
	inline void set_U3CU3Ef__amU24cache8_8(Func_2_t1086561661 * value)
	{
		___U3CU3Ef__amU24cache8_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache8_8, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache9_9() { return static_cast<int32_t>(offsetof(Game_t539648204_StaticFields, ___U3CU3Ef__amU24cache9_9)); }
	inline Func_2_t1086561661 * get_U3CU3Ef__amU24cache9_9() const { return ___U3CU3Ef__amU24cache9_9; }
	inline Func_2_t1086561661 ** get_address_of_U3CU3Ef__amU24cache9_9() { return &___U3CU3Ef__amU24cache9_9; }
	inline void set_U3CU3Ef__amU24cache9_9(Func_2_t1086561661 * value)
	{
		___U3CU3Ef__amU24cache9_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache9_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
