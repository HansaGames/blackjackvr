﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TestBlackjackGame/<endGame>c__Iterator11
struct U3CendGameU3Ec__Iterator11_t584020149;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void TestBlackjackGame/<endGame>c__Iterator11::.ctor()
extern "C"  void U3CendGameU3Ec__Iterator11__ctor_m3990743430 (U3CendGameU3Ec__Iterator11_t584020149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TestBlackjackGame/<endGame>c__Iterator11::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CendGameU3Ec__Iterator11_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3253031702 (U3CendGameU3Ec__Iterator11_t584020149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TestBlackjackGame/<endGame>c__Iterator11::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CendGameU3Ec__Iterator11_System_Collections_IEnumerator_get_Current_m4141630122 (U3CendGameU3Ec__Iterator11_t584020149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TestBlackjackGame/<endGame>c__Iterator11::MoveNext()
extern "C"  bool U3CendGameU3Ec__Iterator11_MoveNext_m2207699606 (U3CendGameU3Ec__Iterator11_t584020149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestBlackjackGame/<endGame>c__Iterator11::Dispose()
extern "C"  void U3CendGameU3Ec__Iterator11_Dispose_m3891891267 (U3CendGameU3Ec__Iterator11_t584020149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestBlackjackGame/<endGame>c__Iterator11::Reset()
extern "C"  void U3CendGameU3Ec__Iterator11_Reset_m1637176371 (U3CendGameU3Ec__Iterator11_t584020149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
