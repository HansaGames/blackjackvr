﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Autowalk
struct Autowalk_t1504404504;

#include "codegen/il2cpp-codegen.h"

// System.Void Autowalk::.ctor()
extern "C"  void Autowalk__ctor_m4131943811 (Autowalk_t1504404504 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Autowalk::Start()
extern "C"  void Autowalk_Start_m3079081603 (Autowalk_t1504404504 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Autowalk::Update()
extern "C"  void Autowalk_Update_m968101354 (Autowalk_t1504404504 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
