﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_BaseTutorial2983299439.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TestTutorial
struct  TestTutorial_t1071937072  : public BaseTutorial_t2983299439
{
public:
	// System.Int32 TestTutorial::numberOfTest
	int32_t ___numberOfTest_3;

public:
	inline static int32_t get_offset_of_numberOfTest_3() { return static_cast<int32_t>(offsetof(TestTutorial_t1071937072, ___numberOfTest_3)); }
	inline int32_t get_numberOfTest_3() const { return ___numberOfTest_3; }
	inline int32_t* get_address_of_numberOfTest_3() { return &___numberOfTest_3; }
	inline void set_numberOfTest_3(int32_t value)
	{
		___numberOfTest_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
