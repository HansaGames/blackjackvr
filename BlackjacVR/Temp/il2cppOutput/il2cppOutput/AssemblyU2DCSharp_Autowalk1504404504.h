﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t1659122786;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Autowalk
struct  Autowalk_t1504404504  : public MonoBehaviour_t667441552
{
public:
	// System.Boolean Autowalk::isWalking
	bool ___isWalking_3;
	// UnityEngine.Transform Autowalk::mainCamera
	Transform_t1659122786 * ___mainCamera_4;
	// System.Single Autowalk::speed
	float ___speed_5;
	// System.Boolean Autowalk::walkWhenTriggered
	bool ___walkWhenTriggered_6;
	// System.Boolean Autowalk::walkWhenLookDown
	bool ___walkWhenLookDown_7;
	// System.Double Autowalk::thresholdAngle
	double ___thresholdAngle_8;
	// System.Boolean Autowalk::freezeYPosition
	bool ___freezeYPosition_9;
	// System.Single Autowalk::yOffset
	float ___yOffset_10;

public:
	inline static int32_t get_offset_of_isWalking_3() { return static_cast<int32_t>(offsetof(Autowalk_t1504404504, ___isWalking_3)); }
	inline bool get_isWalking_3() const { return ___isWalking_3; }
	inline bool* get_address_of_isWalking_3() { return &___isWalking_3; }
	inline void set_isWalking_3(bool value)
	{
		___isWalking_3 = value;
	}

	inline static int32_t get_offset_of_mainCamera_4() { return static_cast<int32_t>(offsetof(Autowalk_t1504404504, ___mainCamera_4)); }
	inline Transform_t1659122786 * get_mainCamera_4() const { return ___mainCamera_4; }
	inline Transform_t1659122786 ** get_address_of_mainCamera_4() { return &___mainCamera_4; }
	inline void set_mainCamera_4(Transform_t1659122786 * value)
	{
		___mainCamera_4 = value;
		Il2CppCodeGenWriteBarrier(&___mainCamera_4, value);
	}

	inline static int32_t get_offset_of_speed_5() { return static_cast<int32_t>(offsetof(Autowalk_t1504404504, ___speed_5)); }
	inline float get_speed_5() const { return ___speed_5; }
	inline float* get_address_of_speed_5() { return &___speed_5; }
	inline void set_speed_5(float value)
	{
		___speed_5 = value;
	}

	inline static int32_t get_offset_of_walkWhenTriggered_6() { return static_cast<int32_t>(offsetof(Autowalk_t1504404504, ___walkWhenTriggered_6)); }
	inline bool get_walkWhenTriggered_6() const { return ___walkWhenTriggered_6; }
	inline bool* get_address_of_walkWhenTriggered_6() { return &___walkWhenTriggered_6; }
	inline void set_walkWhenTriggered_6(bool value)
	{
		___walkWhenTriggered_6 = value;
	}

	inline static int32_t get_offset_of_walkWhenLookDown_7() { return static_cast<int32_t>(offsetof(Autowalk_t1504404504, ___walkWhenLookDown_7)); }
	inline bool get_walkWhenLookDown_7() const { return ___walkWhenLookDown_7; }
	inline bool* get_address_of_walkWhenLookDown_7() { return &___walkWhenLookDown_7; }
	inline void set_walkWhenLookDown_7(bool value)
	{
		___walkWhenLookDown_7 = value;
	}

	inline static int32_t get_offset_of_thresholdAngle_8() { return static_cast<int32_t>(offsetof(Autowalk_t1504404504, ___thresholdAngle_8)); }
	inline double get_thresholdAngle_8() const { return ___thresholdAngle_8; }
	inline double* get_address_of_thresholdAngle_8() { return &___thresholdAngle_8; }
	inline void set_thresholdAngle_8(double value)
	{
		___thresholdAngle_8 = value;
	}

	inline static int32_t get_offset_of_freezeYPosition_9() { return static_cast<int32_t>(offsetof(Autowalk_t1504404504, ___freezeYPosition_9)); }
	inline bool get_freezeYPosition_9() const { return ___freezeYPosition_9; }
	inline bool* get_address_of_freezeYPosition_9() { return &___freezeYPosition_9; }
	inline void set_freezeYPosition_9(bool value)
	{
		___freezeYPosition_9 = value;
	}

	inline static int32_t get_offset_of_yOffset_10() { return static_cast<int32_t>(offsetof(Autowalk_t1504404504, ___yOffset_10)); }
	inline float get_yOffset_10() const { return ___yOffset_10; }
	inline float* get_address_of_yOffset_10() { return &___yOffset_10; }
	inline void set_yOffset_10(float value)
	{
		___yOffset_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
