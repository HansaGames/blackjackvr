﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MKGlowSystem.MKGlow
struct MKGlow_t3477081437;
// UnityEngine.GUIStyle
struct GUIStyle_t2990928826;
// UnityEngine.GUISkin
struct GUISkin_t3371348110;
// UnityEngine.Shader[]
struct ShaderU5BU5D_t3604329748;
// UnityEngine.Cubemap
struct Cubemap_t761092157;
// UnityEngine.Texture[]
struct TextureU5BU5D_t606176076;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t2662109048;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MK_DemoControl
struct  MK_DemoControl_t1618197689  : public MonoBehaviour_t667441552
{
public:
	// MKGlowSystem.MKGlow MK_DemoControl::mkGlow
	MKGlow_t3477081437 * ___mkGlow_2;
	// UnityEngine.GUIStyle MK_DemoControl::bStyle
	GUIStyle_t2990928826 * ___bStyle_3;
	// UnityEngine.GUIStyle MK_DemoControl::r2bStyle
	GUIStyle_t2990928826 * ___r2bStyle_4;
	// UnityEngine.GUISkin MK_DemoControl::skin
	GUISkin_t3371348110 * ___skin_5;
	// System.Int32 MK_DemoControl::currentRoom
	int32_t ___currentRoom_6;
	// System.Int32 MK_DemoControl::currentRoom2Texture
	int32_t ___currentRoom2Texture_7;
	// System.Int32 MK_DemoControl::currentRoom1Object
	int32_t ___currentRoom1Object_8;
	// System.Int32 MK_DemoControl::currentRoom1Texture
	int32_t ___currentRoom1Texture_9;
	// System.Int32 MK_DemoControl::currentRoom1GlowColor
	int32_t ___currentRoom1GlowColor_10;
	// System.Int32 MK_DemoControl::currentRoom1GlowTexColor
	int32_t ___currentRoom1GlowTexColor_11;
	// System.Int32 MK_DemoControl::currentRoom0GlowColor
	int32_t ___currentRoom0GlowColor_12;
	// System.Int32 MK_DemoControl::currentRoom1Shader
	int32_t ___currentRoom1Shader_13;
	// UnityEngine.Shader[] MK_DemoControl::room1Shaders
	ShaderU5BU5D_t3604329748* ___room1Shaders_14;
	// UnityEngine.Cubemap MK_DemoControl::cm
	Cubemap_t761092157 * ___cm_15;
	// System.Single MK_DemoControl::room1RimP
	float ___room1RimP_16;
	// System.Int32 MK_DemoControl::currentRimColor
	int32_t ___currentRimColor_17;
	// System.Single MK_DemoControl::room1GlowIntensity
	float ___room1GlowIntensity_18;
	// System.Single MK_DemoControl::room1GlowTextureStrength
	float ___room1GlowTextureStrength_19;
	// UnityEngine.Texture[] MK_DemoControl::room2Tex
	TextureU5BU5D_t606176076* ___room2Tex_20;
	// UnityEngine.GameObject[] MK_DemoControl::room2Objects
	GameObjectU5BU5D_t2662109048* ___room2Objects_21;
	// UnityEngine.GameObject[] MK_DemoControl::room1Objects
	GameObjectU5BU5D_t2662109048* ___room1Objects_22;

public:
	inline static int32_t get_offset_of_mkGlow_2() { return static_cast<int32_t>(offsetof(MK_DemoControl_t1618197689, ___mkGlow_2)); }
	inline MKGlow_t3477081437 * get_mkGlow_2() const { return ___mkGlow_2; }
	inline MKGlow_t3477081437 ** get_address_of_mkGlow_2() { return &___mkGlow_2; }
	inline void set_mkGlow_2(MKGlow_t3477081437 * value)
	{
		___mkGlow_2 = value;
		Il2CppCodeGenWriteBarrier(&___mkGlow_2, value);
	}

	inline static int32_t get_offset_of_bStyle_3() { return static_cast<int32_t>(offsetof(MK_DemoControl_t1618197689, ___bStyle_3)); }
	inline GUIStyle_t2990928826 * get_bStyle_3() const { return ___bStyle_3; }
	inline GUIStyle_t2990928826 ** get_address_of_bStyle_3() { return &___bStyle_3; }
	inline void set_bStyle_3(GUIStyle_t2990928826 * value)
	{
		___bStyle_3 = value;
		Il2CppCodeGenWriteBarrier(&___bStyle_3, value);
	}

	inline static int32_t get_offset_of_r2bStyle_4() { return static_cast<int32_t>(offsetof(MK_DemoControl_t1618197689, ___r2bStyle_4)); }
	inline GUIStyle_t2990928826 * get_r2bStyle_4() const { return ___r2bStyle_4; }
	inline GUIStyle_t2990928826 ** get_address_of_r2bStyle_4() { return &___r2bStyle_4; }
	inline void set_r2bStyle_4(GUIStyle_t2990928826 * value)
	{
		___r2bStyle_4 = value;
		Il2CppCodeGenWriteBarrier(&___r2bStyle_4, value);
	}

	inline static int32_t get_offset_of_skin_5() { return static_cast<int32_t>(offsetof(MK_DemoControl_t1618197689, ___skin_5)); }
	inline GUISkin_t3371348110 * get_skin_5() const { return ___skin_5; }
	inline GUISkin_t3371348110 ** get_address_of_skin_5() { return &___skin_5; }
	inline void set_skin_5(GUISkin_t3371348110 * value)
	{
		___skin_5 = value;
		Il2CppCodeGenWriteBarrier(&___skin_5, value);
	}

	inline static int32_t get_offset_of_currentRoom_6() { return static_cast<int32_t>(offsetof(MK_DemoControl_t1618197689, ___currentRoom_6)); }
	inline int32_t get_currentRoom_6() const { return ___currentRoom_6; }
	inline int32_t* get_address_of_currentRoom_6() { return &___currentRoom_6; }
	inline void set_currentRoom_6(int32_t value)
	{
		___currentRoom_6 = value;
	}

	inline static int32_t get_offset_of_currentRoom2Texture_7() { return static_cast<int32_t>(offsetof(MK_DemoControl_t1618197689, ___currentRoom2Texture_7)); }
	inline int32_t get_currentRoom2Texture_7() const { return ___currentRoom2Texture_7; }
	inline int32_t* get_address_of_currentRoom2Texture_7() { return &___currentRoom2Texture_7; }
	inline void set_currentRoom2Texture_7(int32_t value)
	{
		___currentRoom2Texture_7 = value;
	}

	inline static int32_t get_offset_of_currentRoom1Object_8() { return static_cast<int32_t>(offsetof(MK_DemoControl_t1618197689, ___currentRoom1Object_8)); }
	inline int32_t get_currentRoom1Object_8() const { return ___currentRoom1Object_8; }
	inline int32_t* get_address_of_currentRoom1Object_8() { return &___currentRoom1Object_8; }
	inline void set_currentRoom1Object_8(int32_t value)
	{
		___currentRoom1Object_8 = value;
	}

	inline static int32_t get_offset_of_currentRoom1Texture_9() { return static_cast<int32_t>(offsetof(MK_DemoControl_t1618197689, ___currentRoom1Texture_9)); }
	inline int32_t get_currentRoom1Texture_9() const { return ___currentRoom1Texture_9; }
	inline int32_t* get_address_of_currentRoom1Texture_9() { return &___currentRoom1Texture_9; }
	inline void set_currentRoom1Texture_9(int32_t value)
	{
		___currentRoom1Texture_9 = value;
	}

	inline static int32_t get_offset_of_currentRoom1GlowColor_10() { return static_cast<int32_t>(offsetof(MK_DemoControl_t1618197689, ___currentRoom1GlowColor_10)); }
	inline int32_t get_currentRoom1GlowColor_10() const { return ___currentRoom1GlowColor_10; }
	inline int32_t* get_address_of_currentRoom1GlowColor_10() { return &___currentRoom1GlowColor_10; }
	inline void set_currentRoom1GlowColor_10(int32_t value)
	{
		___currentRoom1GlowColor_10 = value;
	}

	inline static int32_t get_offset_of_currentRoom1GlowTexColor_11() { return static_cast<int32_t>(offsetof(MK_DemoControl_t1618197689, ___currentRoom1GlowTexColor_11)); }
	inline int32_t get_currentRoom1GlowTexColor_11() const { return ___currentRoom1GlowTexColor_11; }
	inline int32_t* get_address_of_currentRoom1GlowTexColor_11() { return &___currentRoom1GlowTexColor_11; }
	inline void set_currentRoom1GlowTexColor_11(int32_t value)
	{
		___currentRoom1GlowTexColor_11 = value;
	}

	inline static int32_t get_offset_of_currentRoom0GlowColor_12() { return static_cast<int32_t>(offsetof(MK_DemoControl_t1618197689, ___currentRoom0GlowColor_12)); }
	inline int32_t get_currentRoom0GlowColor_12() const { return ___currentRoom0GlowColor_12; }
	inline int32_t* get_address_of_currentRoom0GlowColor_12() { return &___currentRoom0GlowColor_12; }
	inline void set_currentRoom0GlowColor_12(int32_t value)
	{
		___currentRoom0GlowColor_12 = value;
	}

	inline static int32_t get_offset_of_currentRoom1Shader_13() { return static_cast<int32_t>(offsetof(MK_DemoControl_t1618197689, ___currentRoom1Shader_13)); }
	inline int32_t get_currentRoom1Shader_13() const { return ___currentRoom1Shader_13; }
	inline int32_t* get_address_of_currentRoom1Shader_13() { return &___currentRoom1Shader_13; }
	inline void set_currentRoom1Shader_13(int32_t value)
	{
		___currentRoom1Shader_13 = value;
	}

	inline static int32_t get_offset_of_room1Shaders_14() { return static_cast<int32_t>(offsetof(MK_DemoControl_t1618197689, ___room1Shaders_14)); }
	inline ShaderU5BU5D_t3604329748* get_room1Shaders_14() const { return ___room1Shaders_14; }
	inline ShaderU5BU5D_t3604329748** get_address_of_room1Shaders_14() { return &___room1Shaders_14; }
	inline void set_room1Shaders_14(ShaderU5BU5D_t3604329748* value)
	{
		___room1Shaders_14 = value;
		Il2CppCodeGenWriteBarrier(&___room1Shaders_14, value);
	}

	inline static int32_t get_offset_of_cm_15() { return static_cast<int32_t>(offsetof(MK_DemoControl_t1618197689, ___cm_15)); }
	inline Cubemap_t761092157 * get_cm_15() const { return ___cm_15; }
	inline Cubemap_t761092157 ** get_address_of_cm_15() { return &___cm_15; }
	inline void set_cm_15(Cubemap_t761092157 * value)
	{
		___cm_15 = value;
		Il2CppCodeGenWriteBarrier(&___cm_15, value);
	}

	inline static int32_t get_offset_of_room1RimP_16() { return static_cast<int32_t>(offsetof(MK_DemoControl_t1618197689, ___room1RimP_16)); }
	inline float get_room1RimP_16() const { return ___room1RimP_16; }
	inline float* get_address_of_room1RimP_16() { return &___room1RimP_16; }
	inline void set_room1RimP_16(float value)
	{
		___room1RimP_16 = value;
	}

	inline static int32_t get_offset_of_currentRimColor_17() { return static_cast<int32_t>(offsetof(MK_DemoControl_t1618197689, ___currentRimColor_17)); }
	inline int32_t get_currentRimColor_17() const { return ___currentRimColor_17; }
	inline int32_t* get_address_of_currentRimColor_17() { return &___currentRimColor_17; }
	inline void set_currentRimColor_17(int32_t value)
	{
		___currentRimColor_17 = value;
	}

	inline static int32_t get_offset_of_room1GlowIntensity_18() { return static_cast<int32_t>(offsetof(MK_DemoControl_t1618197689, ___room1GlowIntensity_18)); }
	inline float get_room1GlowIntensity_18() const { return ___room1GlowIntensity_18; }
	inline float* get_address_of_room1GlowIntensity_18() { return &___room1GlowIntensity_18; }
	inline void set_room1GlowIntensity_18(float value)
	{
		___room1GlowIntensity_18 = value;
	}

	inline static int32_t get_offset_of_room1GlowTextureStrength_19() { return static_cast<int32_t>(offsetof(MK_DemoControl_t1618197689, ___room1GlowTextureStrength_19)); }
	inline float get_room1GlowTextureStrength_19() const { return ___room1GlowTextureStrength_19; }
	inline float* get_address_of_room1GlowTextureStrength_19() { return &___room1GlowTextureStrength_19; }
	inline void set_room1GlowTextureStrength_19(float value)
	{
		___room1GlowTextureStrength_19 = value;
	}

	inline static int32_t get_offset_of_room2Tex_20() { return static_cast<int32_t>(offsetof(MK_DemoControl_t1618197689, ___room2Tex_20)); }
	inline TextureU5BU5D_t606176076* get_room2Tex_20() const { return ___room2Tex_20; }
	inline TextureU5BU5D_t606176076** get_address_of_room2Tex_20() { return &___room2Tex_20; }
	inline void set_room2Tex_20(TextureU5BU5D_t606176076* value)
	{
		___room2Tex_20 = value;
		Il2CppCodeGenWriteBarrier(&___room2Tex_20, value);
	}

	inline static int32_t get_offset_of_room2Objects_21() { return static_cast<int32_t>(offsetof(MK_DemoControl_t1618197689, ___room2Objects_21)); }
	inline GameObjectU5BU5D_t2662109048* get_room2Objects_21() const { return ___room2Objects_21; }
	inline GameObjectU5BU5D_t2662109048** get_address_of_room2Objects_21() { return &___room2Objects_21; }
	inline void set_room2Objects_21(GameObjectU5BU5D_t2662109048* value)
	{
		___room2Objects_21 = value;
		Il2CppCodeGenWriteBarrier(&___room2Objects_21, value);
	}

	inline static int32_t get_offset_of_room1Objects_22() { return static_cast<int32_t>(offsetof(MK_DemoControl_t1618197689, ___room1Objects_22)); }
	inline GameObjectU5BU5D_t2662109048* get_room1Objects_22() const { return ___room1Objects_22; }
	inline GameObjectU5BU5D_t2662109048** get_address_of_room1Objects_22() { return &___room1Objects_22; }
	inline void set_room1Objects_22(GameObjectU5BU5D_t2662109048* value)
	{
		___room1Objects_22 = value;
		Il2CppCodeGenWriteBarrier(&___room1Objects_22, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
