﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PointsTutorial/<showCards>c__Iterator12
struct U3CshowCardsU3Ec__Iterator12_t1334761580;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PointsTutorial/<showCards>c__Iterator12::.ctor()
extern "C"  void U3CshowCardsU3Ec__Iterator12__ctor_m2150854783 (U3CshowCardsU3Ec__Iterator12_t1334761580 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PointsTutorial/<showCards>c__Iterator12::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CshowCardsU3Ec__Iterator12_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m156350771 (U3CshowCardsU3Ec__Iterator12_t1334761580 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PointsTutorial/<showCards>c__Iterator12::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CshowCardsU3Ec__Iterator12_System_Collections_IEnumerator_get_Current_m3213525703 (U3CshowCardsU3Ec__Iterator12_t1334761580 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PointsTutorial/<showCards>c__Iterator12::MoveNext()
extern "C"  bool U3CshowCardsU3Ec__Iterator12_MoveNext_m3682655509 (U3CshowCardsU3Ec__Iterator12_t1334761580 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PointsTutorial/<showCards>c__Iterator12::Dispose()
extern "C"  void U3CshowCardsU3Ec__Iterator12_Dispose_m990460156 (U3CshowCardsU3Ec__Iterator12_t1334761580 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PointsTutorial/<showCards>c__Iterator12::Reset()
extern "C"  void U3CshowCardsU3Ec__Iterator12_Reset_m4092255020 (U3CshowCardsU3Ec__Iterator12_t1334761580 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
