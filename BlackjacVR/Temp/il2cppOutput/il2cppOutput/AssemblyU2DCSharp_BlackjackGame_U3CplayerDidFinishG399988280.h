﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// BlackjackGame
struct BlackjackGame_t1439740240;

#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Blackjack_GameResult3329670025.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BlackjackGame/<playerDidFinishGame>c__Iterator10
struct  U3CplayerDidFinishGameU3Ec__Iterator10_t399988280  : public Il2CppObject
{
public:
	// Blackjack.GameResult BlackjackGame/<playerDidFinishGame>c__Iterator10::result
	GameResult_t3329670025  ___result_0;
	// System.Int32 BlackjackGame/<playerDidFinishGame>c__Iterator10::$PC
	int32_t ___U24PC_1;
	// System.Object BlackjackGame/<playerDidFinishGame>c__Iterator10::$current
	Il2CppObject * ___U24current_2;
	// Blackjack.GameResult BlackjackGame/<playerDidFinishGame>c__Iterator10::<$>result
	GameResult_t3329670025  ___U3CU24U3Eresult_3;
	// BlackjackGame BlackjackGame/<playerDidFinishGame>c__Iterator10::<>f__this
	BlackjackGame_t1439740240 * ___U3CU3Ef__this_4;

public:
	inline static int32_t get_offset_of_result_0() { return static_cast<int32_t>(offsetof(U3CplayerDidFinishGameU3Ec__Iterator10_t399988280, ___result_0)); }
	inline GameResult_t3329670025  get_result_0() const { return ___result_0; }
	inline GameResult_t3329670025 * get_address_of_result_0() { return &___result_0; }
	inline void set_result_0(GameResult_t3329670025  value)
	{
		___result_0 = value;
	}

	inline static int32_t get_offset_of_U24PC_1() { return static_cast<int32_t>(offsetof(U3CplayerDidFinishGameU3Ec__Iterator10_t399988280, ___U24PC_1)); }
	inline int32_t get_U24PC_1() const { return ___U24PC_1; }
	inline int32_t* get_address_of_U24PC_1() { return &___U24PC_1; }
	inline void set_U24PC_1(int32_t value)
	{
		___U24PC_1 = value;
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CplayerDidFinishGameU3Ec__Iterator10_t399988280, ___U24current_2)); }
	inline Il2CppObject * get_U24current_2() const { return ___U24current_2; }
	inline Il2CppObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(Il2CppObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_2, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Eresult_3() { return static_cast<int32_t>(offsetof(U3CplayerDidFinishGameU3Ec__Iterator10_t399988280, ___U3CU24U3Eresult_3)); }
	inline GameResult_t3329670025  get_U3CU24U3Eresult_3() const { return ___U3CU24U3Eresult_3; }
	inline GameResult_t3329670025 * get_address_of_U3CU24U3Eresult_3() { return &___U3CU24U3Eresult_3; }
	inline void set_U3CU24U3Eresult_3(GameResult_t3329670025  value)
	{
		___U3CU24U3Eresult_3 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_4() { return static_cast<int32_t>(offsetof(U3CplayerDidFinishGameU3Ec__Iterator10_t399988280, ___U3CU3Ef__this_4)); }
	inline BlackjackGame_t1439740240 * get_U3CU3Ef__this_4() const { return ___U3CU3Ef__this_4; }
	inline BlackjackGame_t1439740240 ** get_address_of_U3CU3Ef__this_4() { return &___U3CU3Ef__this_4; }
	inline void set_U3CU3Ef__this_4(BlackjackGame_t1439740240 * value)
	{
		___U3CU3Ef__this_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
