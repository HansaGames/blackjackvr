﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Singleton_1_gen128664468MethodDeclarations.h"

// System.Void Singleton`1<PlayerManager>::.ctor()
#define Singleton_1__ctor_m1539768777(__this, method) ((  void (*) (Singleton_1_t245044621 *, const MethodInfo*))Singleton_1__ctor_m3958676923_gshared)(__this, method)
// System.Void Singleton`1<PlayerManager>::.cctor()
#define Singleton_1__cctor_m6095620(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Singleton_1__cctor_m1977804114_gshared)(__this /* static, unused */, method)
// T Singleton`1<PlayerManager>::get_Instance()
#define Singleton_1_get_Instance_m1621096696(__this /* static, unused */, method) ((  PlayerManager_t4287196524 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Singleton_1_get_Instance_m1020946630_gshared)(__this /* static, unused */, method)
// System.Void Singleton`1<PlayerManager>::OnDestroy()
#define Singleton_1_OnDestroy_m3376466498(__this, method) ((  void (*) (Singleton_1_t245044621 *, const MethodInfo*))Singleton_1_OnDestroy_m276503860_gshared)(__this, method)
