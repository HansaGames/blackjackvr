﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Bidding
struct  Bidding_t1547197275  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GameObject Bidding::biddingText
	GameObject_t3674682005 * ___biddingText_2;
	// UnityEngine.GameObject Bidding::clearButton
	GameObject_t3674682005 * ___clearButton_3;
	// UnityEngine.GameObject Bidding::dealButton
	GameObject_t3674682005 * ___dealButton_4;
	// System.Int32 Bidding::bid
	int32_t ___bid_5;

public:
	inline static int32_t get_offset_of_biddingText_2() { return static_cast<int32_t>(offsetof(Bidding_t1547197275, ___biddingText_2)); }
	inline GameObject_t3674682005 * get_biddingText_2() const { return ___biddingText_2; }
	inline GameObject_t3674682005 ** get_address_of_biddingText_2() { return &___biddingText_2; }
	inline void set_biddingText_2(GameObject_t3674682005 * value)
	{
		___biddingText_2 = value;
		Il2CppCodeGenWriteBarrier(&___biddingText_2, value);
	}

	inline static int32_t get_offset_of_clearButton_3() { return static_cast<int32_t>(offsetof(Bidding_t1547197275, ___clearButton_3)); }
	inline GameObject_t3674682005 * get_clearButton_3() const { return ___clearButton_3; }
	inline GameObject_t3674682005 ** get_address_of_clearButton_3() { return &___clearButton_3; }
	inline void set_clearButton_3(GameObject_t3674682005 * value)
	{
		___clearButton_3 = value;
		Il2CppCodeGenWriteBarrier(&___clearButton_3, value);
	}

	inline static int32_t get_offset_of_dealButton_4() { return static_cast<int32_t>(offsetof(Bidding_t1547197275, ___dealButton_4)); }
	inline GameObject_t3674682005 * get_dealButton_4() const { return ___dealButton_4; }
	inline GameObject_t3674682005 ** get_address_of_dealButton_4() { return &___dealButton_4; }
	inline void set_dealButton_4(GameObject_t3674682005 * value)
	{
		___dealButton_4 = value;
		Il2CppCodeGenWriteBarrier(&___dealButton_4, value);
	}

	inline static int32_t get_offset_of_bid_5() { return static_cast<int32_t>(offsetof(Bidding_t1547197275, ___bid_5)); }
	inline int32_t get_bid_5() const { return ___bid_5; }
	inline int32_t* get_address_of_bid_5() { return &___bid_5; }
	inline void set_bid_5(int32_t value)
	{
		___bid_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
