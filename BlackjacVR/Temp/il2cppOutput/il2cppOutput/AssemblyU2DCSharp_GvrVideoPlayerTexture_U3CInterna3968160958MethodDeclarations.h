﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GvrVideoPlayerTexture/<InternalOnVideoEventCallback>c__AnonStorey17
struct U3CInternalOnVideoEventCallbackU3Ec__AnonStorey17_t3968160958;

#include "codegen/il2cpp-codegen.h"

// System.Void GvrVideoPlayerTexture/<InternalOnVideoEventCallback>c__AnonStorey17::.ctor()
extern "C"  void U3CInternalOnVideoEventCallbackU3Ec__AnonStorey17__ctor_m3532702829 (U3CInternalOnVideoEventCallbackU3Ec__AnonStorey17_t3968160958 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GvrVideoPlayerTexture/<InternalOnVideoEventCallback>c__AnonStorey17::<>m__A()
extern "C"  void U3CInternalOnVideoEventCallbackU3Ec__AnonStorey17_U3CU3Em__A_m2075525997 (U3CInternalOnVideoEventCallbackU3Ec__AnonStorey17_t3968160958 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
