﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Blackjack.Card[]
struct CardU5BU5D_t1346973999;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Object
struct Il2CppObject;
// BlackjackGame
struct BlackjackGame_t1439740240;

#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Blackjack_Card539529194.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BlackjackGame/<drawNewPlayerCards>c__Iterator9
struct  U3CdrawNewPlayerCardsU3Ec__Iterator9_t230613443  : public Il2CppObject
{
public:
	// System.Int32 BlackjackGame/<drawNewPlayerCards>c__Iterator9::<i>__0
	int32_t ___U3CiU3E__0_0;
	// Blackjack.Card[] BlackjackGame/<drawNewPlayerCards>c__Iterator9::cards
	CardU5BU5D_t1346973999* ___cards_1;
	// Blackjack.Card BlackjackGame/<drawNewPlayerCards>c__Iterator9::<card>__1
	Card_t539529194  ___U3CcardU3E__1_2;
	// System.Boolean BlackjackGame/<drawNewPlayerCards>c__Iterator9::<drawWithDelay>__2
	bool ___U3CdrawWithDelayU3E__2_3;
	// UnityEngine.GameObject BlackjackGame/<drawNewPlayerCards>c__Iterator9::<cardGO>__3
	GameObject_t3674682005 * ___U3CcardGOU3E__3_4;
	// System.Single BlackjackGame/<drawNewPlayerCards>c__Iterator9::<deltaX>__4
	float ___U3CdeltaXU3E__4_5;
	// System.Single BlackjackGame/<drawNewPlayerCards>c__Iterator9::<deltaY>__5
	float ___U3CdeltaYU3E__5_6;
	// System.Single BlackjackGame/<drawNewPlayerCards>c__Iterator9::<deltaZ>__6
	float ___U3CdeltaZU3E__6_7;
	// System.Int32 BlackjackGame/<drawNewPlayerCards>c__Iterator9::$PC
	int32_t ___U24PC_8;
	// System.Object BlackjackGame/<drawNewPlayerCards>c__Iterator9::$current
	Il2CppObject * ___U24current_9;
	// Blackjack.Card[] BlackjackGame/<drawNewPlayerCards>c__Iterator9::<$>cards
	CardU5BU5D_t1346973999* ___U3CU24U3Ecards_10;
	// BlackjackGame BlackjackGame/<drawNewPlayerCards>c__Iterator9::<>f__this
	BlackjackGame_t1439740240 * ___U3CU3Ef__this_11;

public:
	inline static int32_t get_offset_of_U3CiU3E__0_0() { return static_cast<int32_t>(offsetof(U3CdrawNewPlayerCardsU3Ec__Iterator9_t230613443, ___U3CiU3E__0_0)); }
	inline int32_t get_U3CiU3E__0_0() const { return ___U3CiU3E__0_0; }
	inline int32_t* get_address_of_U3CiU3E__0_0() { return &___U3CiU3E__0_0; }
	inline void set_U3CiU3E__0_0(int32_t value)
	{
		___U3CiU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_cards_1() { return static_cast<int32_t>(offsetof(U3CdrawNewPlayerCardsU3Ec__Iterator9_t230613443, ___cards_1)); }
	inline CardU5BU5D_t1346973999* get_cards_1() const { return ___cards_1; }
	inline CardU5BU5D_t1346973999** get_address_of_cards_1() { return &___cards_1; }
	inline void set_cards_1(CardU5BU5D_t1346973999* value)
	{
		___cards_1 = value;
		Il2CppCodeGenWriteBarrier(&___cards_1, value);
	}

	inline static int32_t get_offset_of_U3CcardU3E__1_2() { return static_cast<int32_t>(offsetof(U3CdrawNewPlayerCardsU3Ec__Iterator9_t230613443, ___U3CcardU3E__1_2)); }
	inline Card_t539529194  get_U3CcardU3E__1_2() const { return ___U3CcardU3E__1_2; }
	inline Card_t539529194 * get_address_of_U3CcardU3E__1_2() { return &___U3CcardU3E__1_2; }
	inline void set_U3CcardU3E__1_2(Card_t539529194  value)
	{
		___U3CcardU3E__1_2 = value;
	}

	inline static int32_t get_offset_of_U3CdrawWithDelayU3E__2_3() { return static_cast<int32_t>(offsetof(U3CdrawNewPlayerCardsU3Ec__Iterator9_t230613443, ___U3CdrawWithDelayU3E__2_3)); }
	inline bool get_U3CdrawWithDelayU3E__2_3() const { return ___U3CdrawWithDelayU3E__2_3; }
	inline bool* get_address_of_U3CdrawWithDelayU3E__2_3() { return &___U3CdrawWithDelayU3E__2_3; }
	inline void set_U3CdrawWithDelayU3E__2_3(bool value)
	{
		___U3CdrawWithDelayU3E__2_3 = value;
	}

	inline static int32_t get_offset_of_U3CcardGOU3E__3_4() { return static_cast<int32_t>(offsetof(U3CdrawNewPlayerCardsU3Ec__Iterator9_t230613443, ___U3CcardGOU3E__3_4)); }
	inline GameObject_t3674682005 * get_U3CcardGOU3E__3_4() const { return ___U3CcardGOU3E__3_4; }
	inline GameObject_t3674682005 ** get_address_of_U3CcardGOU3E__3_4() { return &___U3CcardGOU3E__3_4; }
	inline void set_U3CcardGOU3E__3_4(GameObject_t3674682005 * value)
	{
		___U3CcardGOU3E__3_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcardGOU3E__3_4, value);
	}

	inline static int32_t get_offset_of_U3CdeltaXU3E__4_5() { return static_cast<int32_t>(offsetof(U3CdrawNewPlayerCardsU3Ec__Iterator9_t230613443, ___U3CdeltaXU3E__4_5)); }
	inline float get_U3CdeltaXU3E__4_5() const { return ___U3CdeltaXU3E__4_5; }
	inline float* get_address_of_U3CdeltaXU3E__4_5() { return &___U3CdeltaXU3E__4_5; }
	inline void set_U3CdeltaXU3E__4_5(float value)
	{
		___U3CdeltaXU3E__4_5 = value;
	}

	inline static int32_t get_offset_of_U3CdeltaYU3E__5_6() { return static_cast<int32_t>(offsetof(U3CdrawNewPlayerCardsU3Ec__Iterator9_t230613443, ___U3CdeltaYU3E__5_6)); }
	inline float get_U3CdeltaYU3E__5_6() const { return ___U3CdeltaYU3E__5_6; }
	inline float* get_address_of_U3CdeltaYU3E__5_6() { return &___U3CdeltaYU3E__5_6; }
	inline void set_U3CdeltaYU3E__5_6(float value)
	{
		___U3CdeltaYU3E__5_6 = value;
	}

	inline static int32_t get_offset_of_U3CdeltaZU3E__6_7() { return static_cast<int32_t>(offsetof(U3CdrawNewPlayerCardsU3Ec__Iterator9_t230613443, ___U3CdeltaZU3E__6_7)); }
	inline float get_U3CdeltaZU3E__6_7() const { return ___U3CdeltaZU3E__6_7; }
	inline float* get_address_of_U3CdeltaZU3E__6_7() { return &___U3CdeltaZU3E__6_7; }
	inline void set_U3CdeltaZU3E__6_7(float value)
	{
		___U3CdeltaZU3E__6_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CdrawNewPlayerCardsU3Ec__Iterator9_t230613443, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}

	inline static int32_t get_offset_of_U24current_9() { return static_cast<int32_t>(offsetof(U3CdrawNewPlayerCardsU3Ec__Iterator9_t230613443, ___U24current_9)); }
	inline Il2CppObject * get_U24current_9() const { return ___U24current_9; }
	inline Il2CppObject ** get_address_of_U24current_9() { return &___U24current_9; }
	inline void set_U24current_9(Il2CppObject * value)
	{
		___U24current_9 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_9, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Ecards_10() { return static_cast<int32_t>(offsetof(U3CdrawNewPlayerCardsU3Ec__Iterator9_t230613443, ___U3CU24U3Ecards_10)); }
	inline CardU5BU5D_t1346973999* get_U3CU24U3Ecards_10() const { return ___U3CU24U3Ecards_10; }
	inline CardU5BU5D_t1346973999** get_address_of_U3CU24U3Ecards_10() { return &___U3CU24U3Ecards_10; }
	inline void set_U3CU24U3Ecards_10(CardU5BU5D_t1346973999* value)
	{
		___U3CU24U3Ecards_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Ecards_10, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_11() { return static_cast<int32_t>(offsetof(U3CdrawNewPlayerCardsU3Ec__Iterator9_t230613443, ___U3CU3Ef__this_11)); }
	inline BlackjackGame_t1439740240 * get_U3CU3Ef__this_11() const { return ___U3CU3Ef__this_11; }
	inline BlackjackGame_t1439740240 ** get_address_of_U3CU3Ef__this_11() { return &___U3CU3Ef__this_11; }
	inline void set_U3CU3Ef__this_11(BlackjackGame_t1439740240 * value)
	{
		___U3CU3Ef__this_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
