﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_VertexHelper3377436606.h"
#include "UnityEngine_UI_UnityEngine_UI_BaseVertexEffect3555037586.h"
#include "UnityEngine_UI_UnityEngine_UI_BaseMeshEffect2306480155.h"
#include "UnityEngine_UI_UnityEngine_UI_Outline3745177896.h"
#include "UnityEngine_UI_UnityEngine_UI_PositionAsUV14062429115.h"
#include "UnityEngine_UI_UnityEngine_UI_Shadow75537580.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E3053238933.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E_3379220348.h"
#include "UnityEngine_Analytics_U3CModuleU3E86524790.h"
#include "AssemblyU2DCSharp_U3CModuleU3E86524790.h"
#include "AssemblyU2DCSharp_DemoInputManager1793962534.h"
#include "AssemblyU2DCSharp_Teleport2999409483.h"
#include "AssemblyU2DCSharp_JumpToPage1437897112.h"
#include "AssemblyU2DCSharp_ChildrenPageProvider4047875423.h"
#include "AssemblyU2DCSharp_PrefabPageProvider1813801316.h"
#include "AssemblyU2DCSharp_PagedScrollBar3367069169.h"
#include "AssemblyU2DCSharp_PagedScrollRect1300409286.h"
#include "AssemblyU2DCSharp_BaseScrollEffect2301062223.h"
#include "AssemblyU2DCSharp_BaseTile2638111039.h"
#include "AssemblyU2DCSharp_FadeScrollEffect3547337594.h"
#include "AssemblyU2DCSharp_FloatTile4187585290.h"
#include "AssemblyU2DCSharp_MaskedTile543780345.h"
#include "AssemblyU2DCSharp_ScaleScrollEffect2532801800.h"
#include "AssemblyU2DCSharp_TileScrollEffect120837292.h"
#include "AssemblyU2DCSharp_TiledPage1156665093.h"
#include "AssemblyU2DCSharp_TranslateScrollEffect1438813964.h"
#include "AssemblyU2DCSharp_Tab83829.h"
#include "AssemblyU2DCSharp_TabGroup3430424586.h"
#include "AssemblyU2DCSharp_UIFadeTransition1995896997.h"
#include "AssemblyU2DCSharp_GVR_Input_AppButtonInput2269773419.h"
#include "AssemblyU2DCSharp_GVRSample_AutoPlayVideo709409430.h"
#include "AssemblyU2DCSharp_GVR_Input_Vector3Event4177984638.h"
#include "AssemblyU2DCSharp_GVR_Input_Vector2Event4149355487.h"
#include "AssemblyU2DCSharp_GVR_Input_FloatEvent2942564530.h"
#include "AssemblyU2DCSharp_GVR_Input_BoolEvent4133175974.h"
#include "AssemblyU2DCSharp_GVR_Input_ButtonEvent340822814.h"
#include "AssemblyU2DCSharp_GVR_Input_TouchPadEvent1864639100.h"
#include "AssemblyU2DCSharp_GVR_Input_TransformEvent3703450146.h"
#include "AssemblyU2DCSharp_GVR_Input_GameObjectEvent568205151.h"
#include "AssemblyU2DCSharp_MenuHandler3851978955.h"
#include "AssemblyU2DCSharp_MenuHandler_U3CDoAppearU3Ec__Ite2930392421.h"
#include "AssemblyU2DCSharp_MenuHandler_U3CDoFadeU3Ec__Itera2584778527.h"
#include "AssemblyU2DCSharp_GVR_Events_PositionSwapper1161445795.h"
#include "AssemblyU2DCSharp_ScrubberEvents1045358937.h"
#include "AssemblyU2DCSharp_SwitchVideos1482505164.h"
#include "AssemblyU2DCSharp_GVR_Events_ToggleAction844807776.h"
#include "AssemblyU2DCSharp_VideoControlsManager127564348.h"
#include "AssemblyU2DCSharp_VideoControlsManager_U3CDoAppear2411281622.h"
#include "AssemblyU2DCSharp_VideoControlsManager_U3CDoFadeU33093734992.h"
#include "AssemblyU2DCSharp_VideoPlayerReference3178980239.h"
#include "AssemblyU2DCSharp_GazeInputModule2064533489.h"
#include "AssemblyU2DCSharp_GvrHead2074018243.h"
#include "AssemblyU2DCSharp_GvrHead_HeadUpdatedDelegate4119218932.h"
#include "AssemblyU2DCSharp_GvrCameraUtils3751592777.h"
#include "AssemblyU2DCSharp_GvrEye2145111534.h"
#include "AssemblyU2DCSharp_GvrPostRender3571076089.h"
#include "AssemblyU2DCSharp_GvrPreRender2492587030.h"
#include "AssemblyU2DCSharp_GvrProfile2868291302.h"
#include "AssemblyU2DCSharp_GvrProfile_Screen3577771989.h"
#include "AssemblyU2DCSharp_GvrProfile_Lenses3379109269.h"
#include "AssemblyU2DCSharp_GvrProfile_MaxFOV3404298290.h"
#include "AssemblyU2DCSharp_GvrProfile_Distortion2316389254.h"
#include "AssemblyU2DCSharp_GvrProfile_Viewer3668830587.h"
#include "AssemblyU2DCSharp_GvrProfile_ScreenSizes4088223165.h"
#include "AssemblyU2DCSharp_GvrProfile_ViewerTypes962081726.h"
#include "AssemblyU2DCSharp_StereoController1637909972.h"
#include "AssemblyU2DCSharp_StereoController_U3CEndOfFrameU33320551397.h"
#include "AssemblyU2DCSharp_StereoRenderEffect4012593919.h"
#include "AssemblyU2DCSharp_Gvr_Internal_BaseVRDevice591083105.h"
#include "AssemblyU2DCSharp_Gvr_Internal_GvrDevice2458844523.h"
#include "AssemblyU2DCSharp_Gvr_Internal_iOSDevice141998261.h"
#include "AssemblyU2DCSharp_GvrGaze2073985384.h"
#include "AssemblyU2DCSharp_GvrReticle4275820913.h"
#include "AssemblyU2DCSharp_GvrAudio4159038547.h"
#include "AssemblyU2DCSharp_GvrAudio_Quality3830609699.h"
#include "AssemblyU2DCSharp_GvrAudio_SpatializerData3927744912.h"
#include "AssemblyU2DCSharp_GvrAudio_SpatializerType3928244512.h"
#include "AssemblyU2DCSharp_GvrAudio_RoomProperties3951668682.h"
#include "AssemblyU2DCSharp_GvrAudioListener2617838119.h"
#include "AssemblyU2DCSharp_GvrAudioRoom252475342.h"
#include "AssemblyU2DCSharp_GvrAudioRoom_SurfaceMaterial3675539603.h"
#include "AssemblyU2DCSharp_GvrAudioSoundfield1975836158.h"
#include "AssemblyU2DCSharp_GvrAudioSource2139450958.h"
#include "AssemblyU2DCSharp_GvrArmModel2565158032.h"
#include "AssemblyU2DCSharp_GvrArmModelOffsets3190771600.h"
#include "AssemblyU2DCSharp_GvrController2631983423.h"
#include "AssemblyU2DCSharp_GvrControllerVisual1817913695.h"
#include "AssemblyU2DCSharp_GvrControllerVisualManager956695886.h"
#include "AssemblyU2DCSharp_GvrPointerManager2245873363.h"
#include "AssemblyU2DCSharp_GvrTooltip2037640512.h"
#include "AssemblyU2DCSharp_Gvr_Internal_EmulatorClientSocke2137111857.h"
#include "AssemblyU2DCSharp_Gvr_Internal_EmulatorConfig3000403349.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1800 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1800[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1801 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1801[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1802 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1802[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1803 = { sizeof (VertexHelper_t3377436606), -1, sizeof(VertexHelper_t3377436606_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1803[9] = 
{
	VertexHelper_t3377436606::get_offset_of_m_Positions_0(),
	VertexHelper_t3377436606::get_offset_of_m_Colors_1(),
	VertexHelper_t3377436606::get_offset_of_m_Uv0S_2(),
	VertexHelper_t3377436606::get_offset_of_m_Uv1S_3(),
	VertexHelper_t3377436606::get_offset_of_m_Normals_4(),
	VertexHelper_t3377436606::get_offset_of_m_Tangents_5(),
	VertexHelper_t3377436606::get_offset_of_m_Indices_6(),
	VertexHelper_t3377436606_StaticFields::get_offset_of_s_DefaultTangent_7(),
	VertexHelper_t3377436606_StaticFields::get_offset_of_s_DefaultNormal_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1804 = { sizeof (BaseVertexEffect_t3555037586), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1805 = { sizeof (BaseMeshEffect_t2306480155), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1805[1] = 
{
	BaseMeshEffect_t2306480155::get_offset_of_m_Graphic_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1806 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1807 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1808 = { sizeof (Outline_t3745177896), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1809 = { sizeof (PositionAsUV1_t4062429115), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1810 = { sizeof (Shadow_t75537580), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1810[4] = 
{
	0,
	Shadow_t75537580::get_offset_of_m_EffectColor_4(),
	Shadow_t75537580::get_offset_of_m_EffectDistance_5(),
	Shadow_t75537580::get_offset_of_m_UseGraphicAlpha_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1811 = { sizeof (U3CPrivateImplementationDetailsU3E_t3053238937), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3053238937_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1811[1] = 
{
	U3CPrivateImplementationDetailsU3E_t3053238937_StaticFields::get_offset_of_U24U24fieldU2D0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1812 = { sizeof (U24ArrayTypeU2412_t3379220351)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2412_t3379220351_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1813 = { sizeof (U3CModuleU3E_t86524797), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1814 = { sizeof (U3CModuleU3E_t86524798), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1815 = { sizeof (DemoInputManager_t1793962534), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1816 = { sizeof (Teleport_t2999409483), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1816[3] = 
{
	Teleport_t2999409483::get_offset_of_startingPosition_2(),
	Teleport_t2999409483::get_offset_of_inactiveMaterial_3(),
	Teleport_t2999409483::get_offset_of_gazedAtMaterial_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1817 = { sizeof (JumpToPage_t1437897112), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1818 = { sizeof (ChildrenPageProvider_t4047875423), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1818[2] = 
{
	ChildrenPageProvider_t4047875423::get_offset_of_pages_2(),
	ChildrenPageProvider_t4047875423::get_offset_of_spacing_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1819 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1820 = { sizeof (PrefabPageProvider_t1813801316), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1820[2] = 
{
	PrefabPageProvider_t1813801316::get_offset_of_prefabs_2(),
	PrefabPageProvider_t1813801316::get_offset_of_spacing_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1821 = { sizeof (PagedScrollBar_t3367069169), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1822 = { sizeof (PagedScrollRect_t1300409286), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1823 = { sizeof (BaseScrollEffect_t2301062223), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1824 = { sizeof (BaseTile_t2638111039), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1825 = { sizeof (FadeScrollEffect_t3547337594), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1826 = { sizeof (FloatTile_t4187585290), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1827 = { sizeof (MaskedTile_t543780345), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1828 = { sizeof (ScaleScrollEffect_t2532801800), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1829 = { sizeof (TileScrollEffect_t120837292), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1830 = { sizeof (TiledPage_t1156665093), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1831 = { sizeof (TranslateScrollEffect_t1438813964), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1832 = { sizeof (Tab_t83829), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1833 = { sizeof (TabGroup_t3430424586), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1834 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1835 = { sizeof (UIFadeTransition_t1995896997), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1836 = { sizeof (AppButtonInput_t2269773419), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1837 = { sizeof (AutoPlayVideo_t709409430), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1837[5] = 
{
	AutoPlayVideo_t709409430::get_offset_of_done_2(),
	AutoPlayVideo_t709409430::get_offset_of_t_3(),
	AutoPlayVideo_t709409430::get_offset_of_player_4(),
	AutoPlayVideo_t709409430::get_offset_of_delay_5(),
	AutoPlayVideo_t709409430::get_offset_of_loop_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1838 = { sizeof (Vector3Event_t4177984638), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1839 = { sizeof (Vector2Event_t4149355487), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1840 = { sizeof (FloatEvent_t2942564530), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1841 = { sizeof (BoolEvent_t4133175974), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1842 = { sizeof (ButtonEvent_t340822814), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1843 = { sizeof (TouchPadEvent_t1864639100), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1844 = { sizeof (TransformEvent_t3703450146), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1845 = { sizeof (GameObjectEvent_t568205151), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1846 = { sizeof (MenuHandler_t3851978955), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1846[1] = 
{
	MenuHandler_t3851978955::get_offset_of_menuObjects_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1847 = { sizeof (U3CDoAppearU3Ec__Iterator0_t2930392421), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1847[4] = 
{
	U3CDoAppearU3Ec__Iterator0_t2930392421::get_offset_of_U3CcgU3E__0_0(),
	U3CDoAppearU3Ec__Iterator0_t2930392421::get_offset_of_U24PC_1(),
	U3CDoAppearU3Ec__Iterator0_t2930392421::get_offset_of_U24current_2(),
	U3CDoAppearU3Ec__Iterator0_t2930392421::get_offset_of_U3CU3Ef__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1848 = { sizeof (U3CDoFadeU3Ec__Iterator1_t2584778527), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1848[4] = 
{
	U3CDoFadeU3Ec__Iterator1_t2584778527::get_offset_of_U3CcgU3E__0_0(),
	U3CDoFadeU3Ec__Iterator1_t2584778527::get_offset_of_U24PC_1(),
	U3CDoFadeU3Ec__Iterator1_t2584778527::get_offset_of_U24current_2(),
	U3CDoFadeU3Ec__Iterator1_t2584778527::get_offset_of_U3CU3Ef__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1849 = { sizeof (PositionSwapper_t1161445795), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1849[2] = 
{
	PositionSwapper_t1161445795::get_offset_of_currentIndex_2(),
	PositionSwapper_t1161445795::get_offset_of_Positions_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1850 = { sizeof (ScrubberEvents_t1045358937), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1850[5] = 
{
	ScrubberEvents_t1045358937::get_offset_of_newPositionHandle_2(),
	ScrubberEvents_t1045358937::get_offset_of_corners_3(),
	ScrubberEvents_t1045358937::get_offset_of_slider_4(),
	ScrubberEvents_t1045358937::get_offset_of_mgr_5(),
	ScrubberEvents_t1045358937::get_offset_of_inp_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1851 = { sizeof (SwitchVideos_t1482505164), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1851[5] = 
{
	SwitchVideos_t1482505164::get_offset_of_localVideoSample_2(),
	SwitchVideos_t1482505164::get_offset_of_dashVideoSample_3(),
	SwitchVideos_t1482505164::get_offset_of_panoVideoSample_4(),
	SwitchVideos_t1482505164::get_offset_of_videoSamples_5(),
	SwitchVideos_t1482505164::get_offset_of_missingLibText_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1852 = { sizeof (ToggleAction_t844807776), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1852[7] = 
{
	ToggleAction_t844807776::get_offset_of_lastUsage_2(),
	ToggleAction_t844807776::get_offset_of_on_3(),
	ToggleAction_t844807776::get_offset_of_OnToggleOn_4(),
	ToggleAction_t844807776::get_offset_of_OnToggleOff_5(),
	ToggleAction_t844807776::get_offset_of_InitialState_6(),
	ToggleAction_t844807776::get_offset_of_RaiseEventForInitialState_7(),
	ToggleAction_t844807776::get_offset_of_Cooldown_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1853 = { sizeof (VideoControlsManager_t127564348), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1853[11] = 
{
	VideoControlsManager_t127564348::get_offset_of_pauseSprite_2(),
	VideoControlsManager_t127564348::get_offset_of_playSprite_3(),
	VideoControlsManager_t127564348::get_offset_of_videoScrubber_4(),
	VideoControlsManager_t127564348::get_offset_of_volumeSlider_5(),
	VideoControlsManager_t127564348::get_offset_of_volumeWidget_6(),
	VideoControlsManager_t127564348::get_offset_of_settingsPanel_7(),
	VideoControlsManager_t127564348::get_offset_of_bufferedBackground_8(),
	VideoControlsManager_t127564348::get_offset_of_basePosition_9(),
	VideoControlsManager_t127564348::get_offset_of_videoPosition_10(),
	VideoControlsManager_t127564348::get_offset_of_videoDuration_11(),
	VideoControlsManager_t127564348::get_offset_of_U3CPlayerU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1854 = { sizeof (U3CDoAppearU3Ec__Iterator2_t2411281622), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1854[4] = 
{
	U3CDoAppearU3Ec__Iterator2_t2411281622::get_offset_of_U3CcgU3E__0_0(),
	U3CDoAppearU3Ec__Iterator2_t2411281622::get_offset_of_U24PC_1(),
	U3CDoAppearU3Ec__Iterator2_t2411281622::get_offset_of_U24current_2(),
	U3CDoAppearU3Ec__Iterator2_t2411281622::get_offset_of_U3CU3Ef__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1855 = { sizeof (U3CDoFadeU3Ec__Iterator3_t3093734992), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1855[4] = 
{
	U3CDoFadeU3Ec__Iterator3_t3093734992::get_offset_of_U3CcgU3E__0_0(),
	U3CDoFadeU3Ec__Iterator3_t3093734992::get_offset_of_U24PC_1(),
	U3CDoFadeU3Ec__Iterator3_t3093734992::get_offset_of_U24current_2(),
	U3CDoFadeU3Ec__Iterator3_t3093734992::get_offset_of_U3CU3Ef__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1856 = { sizeof (VideoPlayerReference_t3178980239), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1856[1] = 
{
	VideoPlayerReference_t3178980239::get_offset_of_player_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1857 = { sizeof (GazeInputModule_t2064533489), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1858 = { sizeof (GvrHead_t2074018243), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1858[6] = 
{
	GvrHead_t2074018243::get_offset_of_trackRotation_2(),
	GvrHead_t2074018243::get_offset_of_trackPosition_3(),
	GvrHead_t2074018243::get_offset_of_target_4(),
	GvrHead_t2074018243::get_offset_of_updateEarly_5(),
	GvrHead_t2074018243::get_offset_of_updated_6(),
	GvrHead_t2074018243::get_offset_of_OnHeadUpdated_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1859 = { sizeof (HeadUpdatedDelegate_t4119218932), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1860 = { sizeof (GvrCameraUtils_t3751592777), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1861 = { sizeof (GvrEye_t2145111534), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1861[8] = 
{
	GvrEye_t2145111534::get_offset_of_eye_2(),
	GvrEye_t2145111534::get_offset_of_toggleCullingMask_3(),
	GvrEye_t2145111534::get_offset_of_controller_4(),
	GvrEye_t2145111534::get_offset_of_stereoEffect_5(),
	GvrEye_t2145111534::get_offset_of_monoCamera_6(),
	GvrEye_t2145111534::get_offset_of_realProj_7(),
	GvrEye_t2145111534::get_offset_of_interpPosition_8(),
	GvrEye_t2145111534::get_offset_of_U3CcamU3Ek__BackingField_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1862 = { sizeof (GvrPostRender_t3571076089), -1, sizeof(GvrPostRender_t3571076089_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1862[22] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	GvrPostRender_t3571076089::get_offset_of_distortionMesh_14(),
	GvrPostRender_t3571076089::get_offset_of_meshMaterial_15(),
	GvrPostRender_t3571076089::get_offset_of_uiMaterial_16(),
	GvrPostRender_t3571076089::get_offset_of_centerWidthPx_17(),
	GvrPostRender_t3571076089::get_offset_of_buttonWidthPx_18(),
	GvrPostRender_t3571076089::get_offset_of_xScale_19(),
	GvrPostRender_t3571076089::get_offset_of_yScale_20(),
	GvrPostRender_t3571076089::get_offset_of_xfm_21(),
	GvrPostRender_t3571076089_StaticFields::get_offset_of_Angles_22(),
	GvrPostRender_t3571076089::get_offset_of_U3CcamU3Ek__BackingField_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1863 = { sizeof (GvrPreRender_t2492587030), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1863[1] = 
{
	GvrPreRender_t2492587030::get_offset_of_U3CcamU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1864 = { sizeof (GvrProfile_t2868291302), -1, sizeof(GvrProfile_t2868291302_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1864[15] = 
{
	GvrProfile_t2868291302::get_offset_of_screen_0(),
	GvrProfile_t2868291302::get_offset_of_viewer_1(),
	GvrProfile_t2868291302_StaticFields::get_offset_of_Nexus5_2(),
	GvrProfile_t2868291302_StaticFields::get_offset_of_Nexus6_3(),
	GvrProfile_t2868291302_StaticFields::get_offset_of_GalaxyS6_4(),
	GvrProfile_t2868291302_StaticFields::get_offset_of_GalaxyNote4_5(),
	GvrProfile_t2868291302_StaticFields::get_offset_of_LGG3_6(),
	GvrProfile_t2868291302_StaticFields::get_offset_of_iPhone4_7(),
	GvrProfile_t2868291302_StaticFields::get_offset_of_iPhone5_8(),
	GvrProfile_t2868291302_StaticFields::get_offset_of_iPhone6_9(),
	GvrProfile_t2868291302_StaticFields::get_offset_of_iPhone6p_10(),
	GvrProfile_t2868291302_StaticFields::get_offset_of_CardboardJun2014_11(),
	GvrProfile_t2868291302_StaticFields::get_offset_of_CardboardMay2015_12(),
	GvrProfile_t2868291302_StaticFields::get_offset_of_GoggleTechC1Glass_13(),
	GvrProfile_t2868291302_StaticFields::get_offset_of_Default_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1865 = { sizeof (Screen_t3577771989)+ sizeof (Il2CppObject), sizeof(Screen_t3577771989_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1865[3] = 
{
	Screen_t3577771989::get_offset_of_width_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Screen_t3577771989::get_offset_of_height_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Screen_t3577771989::get_offset_of_border_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1866 = { sizeof (Lenses_t3379109269)+ sizeof (Il2CppObject), sizeof(Lenses_t3379109269_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1866[7] = 
{
	0,
	0,
	0,
	Lenses_t3379109269::get_offset_of_separation_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Lenses_t3379109269::get_offset_of_offset_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Lenses_t3379109269::get_offset_of_screenDistance_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Lenses_t3379109269::get_offset_of_alignment_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1867 = { sizeof (MaxFOV_t3404298290)+ sizeof (Il2CppObject), sizeof(MaxFOV_t3404298290_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1867[4] = 
{
	MaxFOV_t3404298290::get_offset_of_outer_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MaxFOV_t3404298290::get_offset_of_inner_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MaxFOV_t3404298290::get_offset_of_upper_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MaxFOV_t3404298290::get_offset_of_lower_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1868 = { sizeof (Distortion_t2316389254)+ sizeof (Il2CppObject), sizeof(Distortion_t2316389254_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1868[1] = 
{
	Distortion_t2316389254::get_offset_of_coef_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1869 = { sizeof (Viewer_t3668830587)+ sizeof (Il2CppObject), sizeof(Viewer_t3668830587_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1869[4] = 
{
	Viewer_t3668830587::get_offset_of_lenses_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Viewer_t3668830587::get_offset_of_maxFOV_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Viewer_t3668830587::get_offset_of_distortion_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Viewer_t3668830587::get_offset_of_inverse_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1870 = { sizeof (ScreenSizes_t4088223165)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1870[10] = 
{
	ScreenSizes_t4088223165::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1871 = { sizeof (ViewerTypes_t962081726)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1871[4] = 
{
	ViewerTypes_t962081726::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1872 = { sizeof (StereoController_t1637909972), -1, sizeof(StereoController_t1637909972_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1872[17] = 
{
	StereoController_t1637909972::get_offset_of_directRender_2(),
	StereoController_t1637909972::get_offset_of_keepStereoUpdated_3(),
	StereoController_t1637909972::get_offset_of_stereoMultiplier_4(),
	StereoController_t1637909972::get_offset_of_matchMonoFOV_5(),
	StereoController_t1637909972::get_offset_of_matchByZoom_6(),
	StereoController_t1637909972::get_offset_of_centerOfInterest_7(),
	StereoController_t1637909972::get_offset_of_radiusOfInterest_8(),
	StereoController_t1637909972::get_offset_of_checkStereoComfort_9(),
	StereoController_t1637909972::get_offset_of_stereoAdjustSmoothing_10(),
	StereoController_t1637909972::get_offset_of_screenParallax_11(),
	StereoController_t1637909972::get_offset_of_stereoPaddingX_12(),
	StereoController_t1637909972::get_offset_of_stereoPaddingY_13(),
	StereoController_t1637909972::get_offset_of_renderedStereo_14(),
	StereoController_t1637909972::get_offset_of_eyes_15(),
	StereoController_t1637909972::get_offset_of_head_16(),
	StereoController_t1637909972::get_offset_of_U3CcamU3Ek__BackingField_17(),
	StereoController_t1637909972_StaticFields::get_offset_of_U3CU3Ef__amU24cache10_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1873 = { sizeof (U3CEndOfFrameU3Ec__Iterator4_t3320551397), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1873[3] = 
{
	U3CEndOfFrameU3Ec__Iterator4_t3320551397::get_offset_of_U24PC_0(),
	U3CEndOfFrameU3Ec__Iterator4_t3320551397::get_offset_of_U24current_1(),
	U3CEndOfFrameU3Ec__Iterator4_t3320551397::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1874 = { sizeof (StereoRenderEffect_t4012593919), -1, sizeof(StereoRenderEffect_t4012593919_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1874[3] = 
{
	StereoRenderEffect_t4012593919::get_offset_of_material_2(),
	StereoRenderEffect_t4012593919::get_offset_of_cam_3(),
	StereoRenderEffect_t4012593919_StaticFields::get_offset_of_fullRect_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1875 = { sizeof (BaseVRDevice_t591083105), -1, sizeof(BaseVRDevice_t591083105_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1875[19] = 
{
	BaseVRDevice_t591083105_StaticFields::get_offset_of_device_0(),
	BaseVRDevice_t591083105::get_offset_of_headPose_1(),
	BaseVRDevice_t591083105::get_offset_of_leftEyePose_2(),
	BaseVRDevice_t591083105::get_offset_of_rightEyePose_3(),
	BaseVRDevice_t591083105::get_offset_of_leftEyeDistortedProjection_4(),
	BaseVRDevice_t591083105::get_offset_of_rightEyeDistortedProjection_5(),
	BaseVRDevice_t591083105::get_offset_of_leftEyeUndistortedProjection_6(),
	BaseVRDevice_t591083105::get_offset_of_rightEyeUndistortedProjection_7(),
	BaseVRDevice_t591083105::get_offset_of_leftEyeDistortedViewport_8(),
	BaseVRDevice_t591083105::get_offset_of_rightEyeDistortedViewport_9(),
	BaseVRDevice_t591083105::get_offset_of_leftEyeUndistortedViewport_10(),
	BaseVRDevice_t591083105::get_offset_of_rightEyeUndistortedViewport_11(),
	BaseVRDevice_t591083105::get_offset_of_recommendedTextureSize_12(),
	BaseVRDevice_t591083105::get_offset_of_leftEyeOrientation_13(),
	BaseVRDevice_t591083105::get_offset_of_rightEyeOrientation_14(),
	BaseVRDevice_t591083105::get_offset_of_tilted_15(),
	BaseVRDevice_t591083105::get_offset_of_profileChanged_16(),
	BaseVRDevice_t591083105::get_offset_of_backButtonPressed_17(),
	BaseVRDevice_t591083105::get_offset_of_U3CProfileU3Ek__BackingField_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1876 = { sizeof (GvrDevice_t2458844523), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1876[13] = 
{
	0,
	0,
	0,
	0,
	0,
	GvrDevice_t2458844523::get_offset_of_headData_24(),
	GvrDevice_t2458844523::get_offset_of_viewData_25(),
	GvrDevice_t2458844523::get_offset_of_profileData_26(),
	GvrDevice_t2458844523::get_offset_of_headView_27(),
	GvrDevice_t2458844523::get_offset_of_leftEyeView_28(),
	GvrDevice_t2458844523::get_offset_of_rightEyeView_29(),
	GvrDevice_t2458844523::get_offset_of_debugDisableNativeProjections_30(),
	GvrDevice_t2458844523::get_offset_of_debugDisableNativeUILayer_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1877 = { sizeof (iOSDevice_t141998261), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1877[1] = 
{
	iOSDevice_t141998261::get_offset_of_isOpenGL_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1878 = { sizeof (GvrGaze_t2073985384), -1, sizeof(GvrGaze_t2073985384_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1878[11] = 
{
	GvrGaze_t2073985384::get_offset_of_pointerObject_2(),
	GvrGaze_t2073985384::get_offset_of_pointer_3(),
	GvrGaze_t2073985384::get_offset_of_mask_4(),
	GvrGaze_t2073985384::get_offset_of_currentTarget_5(),
	GvrGaze_t2073985384::get_offset_of_currentGazeObject_6(),
	GvrGaze_t2073985384::get_offset_of_lastIntersectPosition_7(),
	GvrGaze_t2073985384::get_offset_of_lastIntersectionRay_8(),
	GvrGaze_t2073985384::get_offset_of_isTriggered_9(),
	GvrGaze_t2073985384::get_offset_of_U3CcamU3Ek__BackingField_10(),
	GvrGaze_t2073985384_StaticFields::get_offset_of_U3CU3Ef__amU24cache9_11(),
	GvrGaze_t2073985384_StaticFields::get_offset_of_U3CU3Ef__amU24cacheA_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1879 = { sizeof (GvrReticle_t4275820913), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1880 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1881 = { sizeof (GvrAudio_t4159038547), -1, sizeof(GvrAudio_t4159038547_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1881[23] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	GvrAudio_t4159038547_StaticFields::get_offset_of_sampleRate_12(),
	GvrAudio_t4159038547_StaticFields::get_offset_of_numChannels_13(),
	GvrAudio_t4159038547_StaticFields::get_offset_of_framesPerBuffer_14(),
	GvrAudio_t4159038547_StaticFields::get_offset_of_listenerDirectivityColor_15(),
	GvrAudio_t4159038547_StaticFields::get_offset_of_sourceDirectivityColor_16(),
	GvrAudio_t4159038547_StaticFields::get_offset_of_bounds_17(),
	GvrAudio_t4159038547_StaticFields::get_offset_of_enabledRooms_18(),
	GvrAudio_t4159038547_StaticFields::get_offset_of_initialized_19(),
	GvrAudio_t4159038547_StaticFields::get_offset_of_listenerTransform_20(),
	GvrAudio_t4159038547_StaticFields::get_offset_of_occlusionMaskValue_21(),
	GvrAudio_t4159038547_StaticFields::get_offset_of_pose_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1882 = { sizeof (Quality_t3830609699)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1882[4] = 
{
	Quality_t3830609699::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1883 = { sizeof (SpatializerData_t3927744912)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1883[9] = 
{
	SpatializerData_t3927744912::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1884 = { sizeof (SpatializerType_t3928244512)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1884[3] = 
{
	SpatializerType_t3928244512::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1885 = { sizeof (RoomProperties_t3951668682)+ sizeof (Il2CppObject), sizeof(RoomProperties_t3951668682_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1885[20] = 
{
	RoomProperties_t3951668682::get_offset_of_positionX_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RoomProperties_t3951668682::get_offset_of_positionY_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RoomProperties_t3951668682::get_offset_of_positionZ_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RoomProperties_t3951668682::get_offset_of_rotationX_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RoomProperties_t3951668682::get_offset_of_rotationY_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RoomProperties_t3951668682::get_offset_of_rotationZ_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RoomProperties_t3951668682::get_offset_of_rotationW_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RoomProperties_t3951668682::get_offset_of_dimensionsX_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RoomProperties_t3951668682::get_offset_of_dimensionsY_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RoomProperties_t3951668682::get_offset_of_dimensionsZ_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RoomProperties_t3951668682::get_offset_of_materialLeft_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RoomProperties_t3951668682::get_offset_of_materialRight_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RoomProperties_t3951668682::get_offset_of_materialBottom_12() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RoomProperties_t3951668682::get_offset_of_materialTop_13() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RoomProperties_t3951668682::get_offset_of_materialFront_14() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RoomProperties_t3951668682::get_offset_of_materialBack_15() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RoomProperties_t3951668682::get_offset_of_reflectionScalar_16() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RoomProperties_t3951668682::get_offset_of_reverbGain_17() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RoomProperties_t3951668682::get_offset_of_reverbTime_18() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RoomProperties_t3951668682::get_offset_of_reverbBrightness_19() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1886 = { sizeof (GvrAudioListener_t2617838119), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1886[3] = 
{
	GvrAudioListener_t2617838119::get_offset_of_globalGainDb_2(),
	GvrAudioListener_t2617838119::get_offset_of_occlusionMask_3(),
	GvrAudioListener_t2617838119::get_offset_of_quality_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1887 = { sizeof (GvrAudioRoom_t252475342), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1887[11] = 
{
	GvrAudioRoom_t252475342::get_offset_of_leftWall_2(),
	GvrAudioRoom_t252475342::get_offset_of_rightWall_3(),
	GvrAudioRoom_t252475342::get_offset_of_floor_4(),
	GvrAudioRoom_t252475342::get_offset_of_ceiling_5(),
	GvrAudioRoom_t252475342::get_offset_of_backWall_6(),
	GvrAudioRoom_t252475342::get_offset_of_frontWall_7(),
	GvrAudioRoom_t252475342::get_offset_of_reflectivity_8(),
	GvrAudioRoom_t252475342::get_offset_of_reverbGainDb_9(),
	GvrAudioRoom_t252475342::get_offset_of_reverbBrightness_10(),
	GvrAudioRoom_t252475342::get_offset_of_reverbTime_11(),
	GvrAudioRoom_t252475342::get_offset_of_size_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1888 = { sizeof (SurfaceMaterial_t3675539603)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1888[24] = 
{
	SurfaceMaterial_t3675539603::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1889 = { sizeof (GvrAudioSoundfield_t1975836158), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1889[12] = 
{
	GvrAudioSoundfield_t1975836158::get_offset_of_gainDb_2(),
	GvrAudioSoundfield_t1975836158::get_offset_of_playOnAwake_3(),
	GvrAudioSoundfield_t1975836158::get_offset_of_soundfieldClip0102_4(),
	GvrAudioSoundfield_t1975836158::get_offset_of_soundfieldClip0304_5(),
	GvrAudioSoundfield_t1975836158::get_offset_of_soundfieldLoop_6(),
	GvrAudioSoundfield_t1975836158::get_offset_of_soundfieldMute_7(),
	GvrAudioSoundfield_t1975836158::get_offset_of_soundfieldPitch_8(),
	GvrAudioSoundfield_t1975836158::get_offset_of_soundfieldPriority_9(),
	GvrAudioSoundfield_t1975836158::get_offset_of_soundfieldVolume_10(),
	GvrAudioSoundfield_t1975836158::get_offset_of_id_11(),
	GvrAudioSoundfield_t1975836158::get_offset_of_audioSources_12(),
	GvrAudioSoundfield_t1975836158::get_offset_of_isPaused_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1890 = { sizeof (GvrAudioSource_t2139450958), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1890[25] = 
{
	GvrAudioSource_t2139450958::get_offset_of_bypassRoomEffects_2(),
	GvrAudioSource_t2139450958::get_offset_of_directivityAlpha_3(),
	GvrAudioSource_t2139450958::get_offset_of_directivitySharpness_4(),
	GvrAudioSource_t2139450958::get_offset_of_listenerDirectivityAlpha_5(),
	GvrAudioSource_t2139450958::get_offset_of_listenerDirectivitySharpness_6(),
	GvrAudioSource_t2139450958::get_offset_of_gainDb_7(),
	GvrAudioSource_t2139450958::get_offset_of_occlusionEnabled_8(),
	GvrAudioSource_t2139450958::get_offset_of_playOnAwake_9(),
	GvrAudioSource_t2139450958::get_offset_of_sourceClip_10(),
	GvrAudioSource_t2139450958::get_offset_of_sourceLoop_11(),
	GvrAudioSource_t2139450958::get_offset_of_sourceMute_12(),
	GvrAudioSource_t2139450958::get_offset_of_sourcePitch_13(),
	GvrAudioSource_t2139450958::get_offset_of_sourcePriority_14(),
	GvrAudioSource_t2139450958::get_offset_of_sourceDopplerLevel_15(),
	GvrAudioSource_t2139450958::get_offset_of_sourceSpread_16(),
	GvrAudioSource_t2139450958::get_offset_of_sourceVolume_17(),
	GvrAudioSource_t2139450958::get_offset_of_sourceRolloffMode_18(),
	GvrAudioSource_t2139450958::get_offset_of_sourceMaxDistance_19(),
	GvrAudioSource_t2139450958::get_offset_of_sourceMinDistance_20(),
	GvrAudioSource_t2139450958::get_offset_of_hrtfEnabled_21(),
	GvrAudioSource_t2139450958::get_offset_of_audioSource_22(),
	GvrAudioSource_t2139450958::get_offset_of_id_23(),
	GvrAudioSource_t2139450958::get_offset_of_currentOcclusion_24(),
	GvrAudioSource_t2139450958::get_offset_of_nextOcclusionUpdate_25(),
	GvrAudioSource_t2139450958::get_offset_of_isPaused_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1891 = { sizeof (GvrArmModel_t2565158032), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1892 = { sizeof (GvrArmModelOffsets_t3190771600), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1893 = { sizeof (GvrController_t2631983423), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1894 = { sizeof (GvrControllerVisual_t1817913695), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1895 = { sizeof (GvrControllerVisualManager_t956695886), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1896 = { sizeof (GvrPointerManager_t2245873363), -1, sizeof(GvrPointerManager_t2245873363_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1896[2] = 
{
	GvrPointerManager_t2245873363_StaticFields::get_offset_of_instance_2(),
	GvrPointerManager_t2245873363::get_offset_of_pointer_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1897 = { sizeof (GvrTooltip_t2037640512), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1898 = { sizeof (EmulatorClientSocket_t2137111857), -1, sizeof(EmulatorClientSocket_t2137111857_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1898[9] = 
{
	0,
	0,
	EmulatorClientSocket_t2137111857_StaticFields::get_offset_of_kPhoneEventPort_4(),
	EmulatorClientSocket_t2137111857::get_offset_of_phoneMirroringSocket_5(),
	EmulatorClientSocket_t2137111857::get_offset_of_phoneEventThread_6(),
	EmulatorClientSocket_t2137111857::get_offset_of_shouldStop_7(),
	EmulatorClientSocket_t2137111857::get_offset_of_lastConnectionAttemptWasSuccessful_8(),
	EmulatorClientSocket_t2137111857::get_offset_of_phoneRemote_9(),
	EmulatorClientSocket_t2137111857::get_offset_of_U3CconnectedU3Ek__BackingField_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1899 = { sizeof (EmulatorConfig_t3000403349), -1, sizeof(EmulatorConfig_t3000403349_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1899[4] = 
{
	EmulatorConfig_t3000403349_StaticFields::get_offset_of_instance_2(),
	EmulatorConfig_t3000403349::get_offset_of_PHONE_EVENT_MODE_3(),
	EmulatorConfig_t3000403349_StaticFields::get_offset_of_USB_SERVER_IP_4(),
	EmulatorConfig_t3000403349_StaticFields::get_offset_of_WIFI_SERVER_IP_5(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
