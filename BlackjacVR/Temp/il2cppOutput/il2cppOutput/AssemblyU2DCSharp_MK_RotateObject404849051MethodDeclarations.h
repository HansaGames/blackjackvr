﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MK_RotateObject
struct MK_RotateObject_t404849051;

#include "codegen/il2cpp-codegen.h"

// System.Void MK_RotateObject::.ctor()
extern "C"  void MK_RotateObject__ctor_m966714416 (MK_RotateObject_t404849051 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MK_RotateObject::Update()
extern "C"  void MK_RotateObject_Update_m1630237917 (MK_RotateObject_t404849051 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
