﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// VideoPlayerReference
struct VideoPlayerReference_t3178980239;
// VideoControlsManager
struct VideoControlsManager_t127564348;
// System.Object
struct Il2CppObject;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_VideoPlayerReference3178980239.h"
#include "AssemblyU2DCSharp_VideoPlayerReference3178980239MethodDeclarations.h"
#include "mscorlib_System_Void2863195528.h"
#include "UnityEngine_UnityEngine_MonoBehaviour667441552MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3501516275MethodDeclarations.h"
#include "AssemblyU2DCSharp_VideoControlsManager127564348MethodDeclarations.h"
#include "AssemblyU2DCSharp_VideoControlsManager127564348.h"
#include "mscorlib_System_Boolean476798718.h"
#include "UnityEngine_UnityEngine_Component3501516275.h"
#include "AssemblyU2DCSharp_GvrVideoPlayerTexture4035356898.h"

// !!0 UnityEngine.Component::GetComponentInChildren<System.Object>(System.Boolean)
extern "C"  Il2CppObject * Component_GetComponentInChildren_TisIl2CppObject_m1157584366_gshared (Component_t3501516275 * __this, bool p0, const MethodInfo* method);
#define Component_GetComponentInChildren_TisIl2CppObject_m1157584366(__this, p0, method) ((  Il2CppObject * (*) (Component_t3501516275 *, bool, const MethodInfo*))Component_GetComponentInChildren_TisIl2CppObject_m1157584366_gshared)(__this, p0, method)
// !!0 UnityEngine.Component::GetComponentInChildren<VideoControlsManager>(System.Boolean)
#define Component_GetComponentInChildren_TisVideoControlsManager_t127564348_m281478244(__this, p0, method) ((  VideoControlsManager_t127564348 * (*) (Component_t3501516275 *, bool, const MethodInfo*))Component_GetComponentInChildren_TisIl2CppObject_m1157584366_gshared)(__this, p0, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void VideoPlayerReference::.ctor()
extern "C"  void VideoPlayerReference__ctor_m802326508 (VideoPlayerReference_t3178980239 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VideoPlayerReference::Awake()
extern const MethodInfo* Component_GetComponentInChildren_TisVideoControlsManager_t127564348_m281478244_MethodInfo_var;
extern const uint32_t VideoPlayerReference_Awake_m1039931727_MetadataUsageId;
extern "C"  void VideoPlayerReference_Awake_m1039931727 (VideoPlayerReference_t3178980239 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (VideoPlayerReference_Awake_m1039931727_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		VideoControlsManager_t127564348 * L_0 = Component_GetComponentInChildren_TisVideoControlsManager_t127564348_m281478244(__this, (bool)1, /*hidden argument*/Component_GetComponentInChildren_TisVideoControlsManager_t127564348_m281478244_MethodInfo_var);
		GvrVideoPlayerTexture_t4035356898 * L_1 = __this->get_player_2();
		NullCheck(L_0);
		VideoControlsManager_set_Player_m4158448525(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
