﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameButtonsSelection
struct GameButtonsSelection_t1942508349;

#include "codegen/il2cpp-codegen.h"

// System.Void GameButtonsSelection::.ctor()
extern "C"  void GameButtonsSelection__ctor_m1770871806 (GameButtonsSelection_t1942508349 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameButtonsSelection::Start()
extern "C"  void GameButtonsSelection_Start_m718009598 (GameButtonsSelection_t1942508349 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameButtonsSelection::Update()
extern "C"  void GameButtonsSelection_Update_m789313231 (GameButtonsSelection_t1942508349 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameButtonsSelection::HandleTimedInput()
extern "C"  void GameButtonsSelection_HandleTimedInput_m3634618977 (GameButtonsSelection_t1942508349 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
