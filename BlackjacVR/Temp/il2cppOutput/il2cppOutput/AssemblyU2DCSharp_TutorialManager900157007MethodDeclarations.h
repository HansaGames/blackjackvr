﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TutorialManager
struct TutorialManager_t900157007;

#include "codegen/il2cpp-codegen.h"

// System.Void TutorialManager::.ctor()
extern "C"  void TutorialManager__ctor_m3414303228 (TutorialManager_t900157007 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialManager::Start()
extern "C"  void TutorialManager_Start_m2361441020 (TutorialManager_t900157007 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialManager::HandleTimedInput()
extern "C"  void TutorialManager_HandleTimedInput_m3434353059 (TutorialManager_t900157007 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialManager::Update()
extern "C"  void TutorialManager_Update_m196079761 (TutorialManager_t900157007 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialManager::nextTutorial()
extern "C"  void TutorialManager_nextTutorial_m1638479833 (TutorialManager_t900157007 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialManager::prevTutorial()
extern "C"  void TutorialManager_prevTutorial_m3651551001 (TutorialManager_t900157007 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialManager::updateTutorialStateButtons()
extern "C"  void TutorialManager_updateTutorialStateButtons_m2290330943 (TutorialManager_t900157007 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialManager::showTutorial()
extern "C"  void TutorialManager_showTutorial_m267813731 (TutorialManager_t900157007 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialManager::enableTutorialButtons(System.Boolean)
extern "C"  void TutorialManager_enableTutorialButtons_m1044817777 (TutorialManager_t900157007 * __this, bool ___enabled0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialManager::startTutorial()
extern "C"  void TutorialManager_startTutorial_m2138800154 (TutorialManager_t900157007 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialManager::endTutorial()
extern "C"  void TutorialManager_endTutorial_m3055467411 (TutorialManager_t900157007 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
