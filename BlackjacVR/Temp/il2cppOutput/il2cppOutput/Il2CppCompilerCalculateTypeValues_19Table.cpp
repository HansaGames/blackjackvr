﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Gvr_Internal_EmulatorConfig_Mode884064821.h"
#include "AssemblyU2DCSharp_Gvr_Internal_EmulatorGyroEvent3657941864.h"
#include "AssemblyU2DCSharp_Gvr_Internal_EmulatorAccelEvent2349426533.h"
#include "AssemblyU2DCSharp_Gvr_Internal_EmulatorTouchEvent2112311150.h"
#include "AssemblyU2DCSharp_Gvr_Internal_EmulatorTouchEvent_A926435919.h"
#include "AssemblyU2DCSharp_Gvr_Internal_EmulatorTouchEvent_3710907716.h"
#include "AssemblyU2DCSharp_Gvr_Internal_EmulatorOrientation3989687101.h"
#include "AssemblyU2DCSharp_Gvr_Internal_EmulatorButtonEvent200175493.h"
#include "AssemblyU2DCSharp_Gvr_Internal_EmulatorButtonEvent3065328481.h"
#include "AssemblyU2DCSharp_Gvr_Internal_EmulatorManager622010154.h"
#include "AssemblyU2DCSharp_Gvr_Internal_EmulatorManager_OnG1752883023.h"
#include "AssemblyU2DCSharp_Gvr_Internal_EmulatorManager_OnA1207501038.h"
#include "AssemblyU2DCSharp_Gvr_Internal_EmulatorManager_OnTo970385655.h"
#include "AssemblyU2DCSharp_Gvr_Internal_EmulatorManager_OnO2004619270.h"
#include "AssemblyU2DCSharp_Gvr_Internal_EmulatorManager_OnB1240547244.h"
#include "AssemblyU2DCSharp_Gvr_Internal_EmulatorManager_U3C2571948936.h"
#include "AssemblyU2DCSharp_proto_Proto_PhoneEvent4294543246.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent1948414762.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Types1856670140.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Types_Type923828173.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Types_MotionEve2659645585.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Types_MotionEve1994437979.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Types_MotionEven190725481.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Types_MotionEve1865844949.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Types_MotionEve2183502845.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Types_Gyroscope2921203048.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Types_Gyroscope3312050644.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Types_Accelerome148610782.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Types_Accelerom1359586634.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Types_DepthMapE3758135118.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Types_DepthMapE3987156410.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Types_Orientati3825228477.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Types_Orientati3974107689.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Types_KeyEvent2095740558.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Types_KeyEvent_1063111930.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Builder2932595742.h"
#include "AssemblyU2DCSharp_GvrBasePointer3602508009.h"
#include "AssemblyU2DCSharp_GvrBasePointerRaycaster407851501.h"
#include "AssemblyU2DCSharp_GvrBasePointerRaycaster_RaycastMo525023850.h"
#include "AssemblyU2DCSharp_GvrExecuteEventsExtension2547249172.h"
#include "AssemblyU2DCSharp_GvrPointerGraphicRaycaster1071039080.h"
#include "AssemblyU2DCSharp_GvrPointerGraphicRaycaster_Block2223317336.h"
#include "AssemblyU2DCSharp_GvrPointerInputModule2267916028.h"
#include "AssemblyU2DCSharp_GvrPointerPhysicsRaycaster1923475945.h"
#include "AssemblyU2DCSharp_GvrUnitySdkVersion3137951952.h"
#include "AssemblyU2DCSharp_GvrViewer671349045.h"
#include "AssemblyU2DCSharp_GvrViewer_DistortionCorrectionMe2227033558.h"
#include "AssemblyU2DCSharp_GvrViewer_Eye643876407.h"
#include "AssemblyU2DCSharp_GvrViewer_Distortion2578366935.h"
#include "AssemblyU2DCSharp_GvrViewer_StereoScreenChangeDele2614477363.h"
#include "AssemblyU2DCSharp_Pose3D2396367586.h"
#include "AssemblyU2DCSharp_MutablePose3D1273683304.h"
#include "AssemblyU2DCSharp_GvrLaserPointer3144584661.h"
#include "AssemblyU2DCSharp_GvrReticlePointer2844437388.h"
#include "AssemblyU2DCSharp_GvrFPS2145111206.h"
#include "AssemblyU2DCSharp_GvrIntent304217535.h"
#include "AssemblyU2DCSharp_GvrUIHelpers4187146094.h"
#include "AssemblyU2DCSharp_GvrVideoPlayerTexture4035356898.h"
#include "AssemblyU2DCSharp_GvrVideoPlayerTexture_VideoType2828778824.h"
#include "AssemblyU2DCSharp_GvrVideoPlayerTexture_VideoResol3968033850.h"
#include "AssemblyU2DCSharp_GvrVideoPlayerTexture_VideoPlaye1357341794.h"
#include "AssemblyU2DCSharp_GvrVideoPlayerTexture_VideoEvent3604595623.h"
#include "AssemblyU2DCSharp_GvrVideoPlayerTexture_RenderComm2320763464.h"
#include "AssemblyU2DCSharp_GvrVideoPlayerTexture_OnVideoEve3220588464.h"
#include "AssemblyU2DCSharp_GvrVideoPlayerTexture_OnExceptio1067313352.h"
#include "AssemblyU2DCSharp_GvrVideoPlayerTexture_U3CStartU31863601168.h"
#include "AssemblyU2DCSharp_GvrVideoPlayerTexture_U3CCallPlu3164122751.h"
#include "AssemblyU2DCSharp_GvrVideoPlayerTexture_U3CInterna3968160958.h"
#include "AssemblyU2DCSharp_GvrVideoPlayerTexture_U3CInterna2058796985.h"
#include "AssemblyU2DCSharp_MK_DemoControl1618197689.h"
#include "AssemblyU2DCSharp_MK_RotateObject404849051.h"
#include "AssemblyU2DCSharp_MKGlowSystem_MKGlow3477081437.h"
#include "AssemblyU2DCSharp_MKGlowSystem_MKGlowType2044315959.h"
#include "AssemblyU2DCSharp_Autowalk1504404504.h"
#include "AssemblyU2DCSharp_Bidding1547197275.h"
#include "AssemblyU2DCSharp_BlackjackGame1439740240.h"
#include "AssemblyU2DCSharp_BlackjackGame_U3CinitCardActionU1966516297.h"
#include "AssemblyU2DCSharp_BlackjackGame_U3CdrawNewPlayerCar230613443.h"
#include "AssemblyU2DCSharp_BlackjackGame_U3CshowDealerCards1840808414.h"
#include "AssemblyU2DCSharp_BlackjackGame_U3CdrawNewDealerCa2458683140.h"
#include "AssemblyU2DCSharp_BlackjackGame_U3CplayerDidWonGam2191380681.h"
#include "AssemblyU2DCSharp_BlackjackGame_U3CplayerDidLostGa2335290970.h"
#include "AssemblyU2DCSharp_BlackjackGame_U3CplayerPlayDrawGa223069034.h"
#include "AssemblyU2DCSharp_BlackjackGame_U3CendGameU3Ec__It2718692611.h"
#include "AssemblyU2DCSharp_BlackjackGame_U3CplayerDidFinishG399988280.h"
#include "AssemblyU2DCSharp_Blackjack_Suit540024807.h"
#include "AssemblyU2DCSharp_Blackjack_CardType365306692.h"
#include "AssemblyU2DCSharp_Blackjack_Card539529194.h"
#include "AssemblyU2DCSharp_Blackjack_Deck539562371.h"
#include "AssemblyU2DCSharp_Blackjack_User540082341.h"
#include "AssemblyU2DCSharp_Blackjack_WinType832601200.h"
#include "AssemblyU2DCSharp_Blackjack_Decision939252918.h"
#include "AssemblyU2DCSharp_Blackjack_GameResult3329670025.h"
#include "AssemblyU2DCSharp_Blackjack_Game539648204.h"
#include "AssemblyU2DCSharp_GameButtonsSelection1942508349.h"
#include "AssemblyU2DCSharp_GameSelectionManager3465822387.h"
#include "AssemblyU2DCSharp_PlayerState2213905936.h"
#include "AssemblyU2DCSharp_PlayerManager4287196524.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1900 = { sizeof (Mode_t884064821)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1900[4] = 
{
	Mode_t884064821::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1901 = { sizeof (EmulatorGyroEvent_t3657941864)+ sizeof (Il2CppObject), sizeof(EmulatorGyroEvent_t3657941864_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1901[2] = 
{
	EmulatorGyroEvent_t3657941864::get_offset_of_timestamp_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	EmulatorGyroEvent_t3657941864::get_offset_of_value_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1902 = { sizeof (EmulatorAccelEvent_t2349426533)+ sizeof (Il2CppObject), sizeof(EmulatorAccelEvent_t2349426533_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1902[2] = 
{
	EmulatorAccelEvent_t2349426533::get_offset_of_timestamp_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	EmulatorAccelEvent_t2349426533::get_offset_of_value_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1903 = { sizeof (EmulatorTouchEvent_t2112311150)+ sizeof (Il2CppObject), -1, sizeof(EmulatorTouchEvent_t2112311150_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1903[6] = 
{
	EmulatorTouchEvent_t2112311150::get_offset_of_action_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	EmulatorTouchEvent_t2112311150::get_offset_of_relativeTimestamp_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	EmulatorTouchEvent_t2112311150::get_offset_of_pointers_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	EmulatorTouchEvent_t2112311150_StaticFields::get_offset_of_ACTION_POINTER_INDEX_SHIFT_3(),
	EmulatorTouchEvent_t2112311150_StaticFields::get_offset_of_ACTION_POINTER_INDEX_MASK_4(),
	EmulatorTouchEvent_t2112311150_StaticFields::get_offset_of_ACTION_MASK_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1904 = { sizeof (Action_t926435919)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1904[10] = 
{
	Action_t926435919::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1905 = { sizeof (Pointer_t3710907716)+ sizeof (Il2CppObject), sizeof(Pointer_t3710907716_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1905[3] = 
{
	Pointer_t3710907716::get_offset_of_fingerId_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Pointer_t3710907716::get_offset_of_normalizedX_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Pointer_t3710907716::get_offset_of_normalizedY_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1906 = { sizeof (EmulatorOrientationEvent_t3989687101)+ sizeof (Il2CppObject), sizeof(EmulatorOrientationEvent_t3989687101_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1906[2] = 
{
	EmulatorOrientationEvent_t3989687101::get_offset_of_timestamp_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	EmulatorOrientationEvent_t3989687101::get_offset_of_orientation_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1907 = { sizeof (EmulatorButtonEvent_t200175493)+ sizeof (Il2CppObject), sizeof(EmulatorButtonEvent_t200175493_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1907[2] = 
{
	EmulatorButtonEvent_t200175493::get_offset_of_code_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	EmulatorButtonEvent_t200175493::get_offset_of_down_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1908 = { sizeof (ButtonCode_t3065328481)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1908[7] = 
{
	ButtonCode_t3065328481::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1909 = { sizeof (EmulatorManager_t622010154), -1, sizeof(EmulatorManager_t622010154_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1909[16] = 
{
	EmulatorManager_t622010154::get_offset_of_emulatorUpdate_2(),
	EmulatorManager_t622010154::get_offset_of_waitForEndOfFrame_3(),
	EmulatorManager_t622010154_StaticFields::get_offset_of_instance_4(),
	EmulatorManager_t622010154::get_offset_of_currentGyroEvent_5(),
	EmulatorManager_t622010154::get_offset_of_currentAccelEvent_6(),
	EmulatorManager_t622010154::get_offset_of_currentTouchEvent_7(),
	EmulatorManager_t622010154::get_offset_of_currentOrientationEvent_8(),
	EmulatorManager_t622010154::get_offset_of_currentButtonEvent_9(),
	EmulatorManager_t622010154::get_offset_of_pendingEvents_10(),
	EmulatorManager_t622010154::get_offset_of_socket_11(),
	EmulatorManager_t622010154::get_offset_of_lastDownTimeMs_12(),
	EmulatorManager_t622010154::get_offset_of_gyroEventListenersInternal_13(),
	EmulatorManager_t622010154::get_offset_of_accelEventListenersInternal_14(),
	EmulatorManager_t622010154::get_offset_of_touchEventListenersInternal_15(),
	EmulatorManager_t622010154::get_offset_of_orientationEventListenersInternal_16(),
	EmulatorManager_t622010154::get_offset_of_buttonEventListenersInternal_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1910 = { sizeof (OnGyroEvent_t1752883023), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1911 = { sizeof (OnAccelEvent_t1207501038), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1912 = { sizeof (OnTouchEvent_t970385655), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1913 = { sizeof (OnOrientationEvent_t2004619270), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1914 = { sizeof (OnButtonEvent_t1240547244), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1915 = { sizeof (U3CEndOfFrameU3Ec__Iterator5_t2571948936), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1915[5] = 
{
	U3CEndOfFrameU3Ec__Iterator5_t2571948936::get_offset_of_U3CphoneEventU3E__0_0(),
	U3CEndOfFrameU3Ec__Iterator5_t2571948936::get_offset_of_U3CU24s_25U3E__1_1(),
	U3CEndOfFrameU3Ec__Iterator5_t2571948936::get_offset_of_U24PC_2(),
	U3CEndOfFrameU3Ec__Iterator5_t2571948936::get_offset_of_U24current_3(),
	U3CEndOfFrameU3Ec__Iterator5_t2571948936::get_offset_of_U3CU3Ef__this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1916 = { sizeof (PhoneEvent_t4294543246), -1, sizeof(PhoneEvent_t4294543246_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1916[1] = 
{
	PhoneEvent_t4294543246_StaticFields::get_offset_of_Descriptor_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1917 = { sizeof (PhoneEvent_t1948414762), -1, sizeof(PhoneEvent_t1948414762_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1917[25] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	PhoneEvent_t1948414762_StaticFields::get_offset_of_defaultInstance_7(),
	PhoneEvent_t1948414762_StaticFields::get_offset_of__phoneEventFieldNames_8(),
	PhoneEvent_t1948414762_StaticFields::get_offset_of__phoneEventFieldTags_9(),
	PhoneEvent_t1948414762::get_offset_of_hasType_10(),
	PhoneEvent_t1948414762::get_offset_of_type__11(),
	PhoneEvent_t1948414762::get_offset_of_hasMotionEvent_12(),
	PhoneEvent_t1948414762::get_offset_of_motionEvent__13(),
	PhoneEvent_t1948414762::get_offset_of_hasGyroscopeEvent_14(),
	PhoneEvent_t1948414762::get_offset_of_gyroscopeEvent__15(),
	PhoneEvent_t1948414762::get_offset_of_hasAccelerometerEvent_16(),
	PhoneEvent_t1948414762::get_offset_of_accelerometerEvent__17(),
	PhoneEvent_t1948414762::get_offset_of_hasDepthMapEvent_18(),
	PhoneEvent_t1948414762::get_offset_of_depthMapEvent__19(),
	PhoneEvent_t1948414762::get_offset_of_hasOrientationEvent_20(),
	PhoneEvent_t1948414762::get_offset_of_orientationEvent__21(),
	PhoneEvent_t1948414762::get_offset_of_hasKeyEvent_22(),
	PhoneEvent_t1948414762::get_offset_of_keyEvent__23(),
	PhoneEvent_t1948414762::get_offset_of_memoizedSerializedSize_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1918 = { sizeof (Types_t1856670140), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1919 = { sizeof (Type_t923828173)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1919[7] = 
{
	Type_t923828173::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1920 = { sizeof (MotionEvent_t2659645585), -1, sizeof(MotionEvent_t2659645585_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1920[12] = 
{
	0,
	0,
	0,
	MotionEvent_t2659645585_StaticFields::get_offset_of_defaultInstance_3(),
	MotionEvent_t2659645585_StaticFields::get_offset_of__motionEventFieldNames_4(),
	MotionEvent_t2659645585_StaticFields::get_offset_of__motionEventFieldTags_5(),
	MotionEvent_t2659645585::get_offset_of_hasTimestamp_6(),
	MotionEvent_t2659645585::get_offset_of_timestamp__7(),
	MotionEvent_t2659645585::get_offset_of_hasAction_8(),
	MotionEvent_t2659645585::get_offset_of_action__9(),
	MotionEvent_t2659645585::get_offset_of_pointers__10(),
	MotionEvent_t2659645585::get_offset_of_memoizedSerializedSize_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1921 = { sizeof (Types_t1994437979), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1922 = { sizeof (Pointer_t190725481), -1, sizeof(Pointer_t190725481_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1922[13] = 
{
	0,
	0,
	0,
	Pointer_t190725481_StaticFields::get_offset_of_defaultInstance_3(),
	Pointer_t190725481_StaticFields::get_offset_of__pointerFieldNames_4(),
	Pointer_t190725481_StaticFields::get_offset_of__pointerFieldTags_5(),
	Pointer_t190725481::get_offset_of_hasId_6(),
	Pointer_t190725481::get_offset_of_id__7(),
	Pointer_t190725481::get_offset_of_hasNormalizedX_8(),
	Pointer_t190725481::get_offset_of_normalizedX__9(),
	Pointer_t190725481::get_offset_of_hasNormalizedY_10(),
	Pointer_t190725481::get_offset_of_normalizedY__11(),
	Pointer_t190725481::get_offset_of_memoizedSerializedSize_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1923 = { sizeof (Builder_t1865844949), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1923[2] = 
{
	Builder_t1865844949::get_offset_of_resultIsReadOnly_0(),
	Builder_t1865844949::get_offset_of_result_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1924 = { sizeof (Builder_t2183502845), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1924[2] = 
{
	Builder_t2183502845::get_offset_of_resultIsReadOnly_0(),
	Builder_t2183502845::get_offset_of_result_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1925 = { sizeof (GyroscopeEvent_t2921203048), -1, sizeof(GyroscopeEvent_t2921203048_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1925[16] = 
{
	0,
	0,
	0,
	0,
	GyroscopeEvent_t2921203048_StaticFields::get_offset_of_defaultInstance_4(),
	GyroscopeEvent_t2921203048_StaticFields::get_offset_of__gyroscopeEventFieldNames_5(),
	GyroscopeEvent_t2921203048_StaticFields::get_offset_of__gyroscopeEventFieldTags_6(),
	GyroscopeEvent_t2921203048::get_offset_of_hasTimestamp_7(),
	GyroscopeEvent_t2921203048::get_offset_of_timestamp__8(),
	GyroscopeEvent_t2921203048::get_offset_of_hasX_9(),
	GyroscopeEvent_t2921203048::get_offset_of_x__10(),
	GyroscopeEvent_t2921203048::get_offset_of_hasY_11(),
	GyroscopeEvent_t2921203048::get_offset_of_y__12(),
	GyroscopeEvent_t2921203048::get_offset_of_hasZ_13(),
	GyroscopeEvent_t2921203048::get_offset_of_z__14(),
	GyroscopeEvent_t2921203048::get_offset_of_memoizedSerializedSize_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1926 = { sizeof (Builder_t3312050644), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1926[2] = 
{
	Builder_t3312050644::get_offset_of_resultIsReadOnly_0(),
	Builder_t3312050644::get_offset_of_result_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1927 = { sizeof (AccelerometerEvent_t148610782), -1, sizeof(AccelerometerEvent_t148610782_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1927[16] = 
{
	0,
	0,
	0,
	0,
	AccelerometerEvent_t148610782_StaticFields::get_offset_of_defaultInstance_4(),
	AccelerometerEvent_t148610782_StaticFields::get_offset_of__accelerometerEventFieldNames_5(),
	AccelerometerEvent_t148610782_StaticFields::get_offset_of__accelerometerEventFieldTags_6(),
	AccelerometerEvent_t148610782::get_offset_of_hasTimestamp_7(),
	AccelerometerEvent_t148610782::get_offset_of_timestamp__8(),
	AccelerometerEvent_t148610782::get_offset_of_hasX_9(),
	AccelerometerEvent_t148610782::get_offset_of_x__10(),
	AccelerometerEvent_t148610782::get_offset_of_hasY_11(),
	AccelerometerEvent_t148610782::get_offset_of_y__12(),
	AccelerometerEvent_t148610782::get_offset_of_hasZ_13(),
	AccelerometerEvent_t148610782::get_offset_of_z__14(),
	AccelerometerEvent_t148610782::get_offset_of_memoizedSerializedSize_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1928 = { sizeof (Builder_t1359586634), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1928[2] = 
{
	Builder_t1359586634::get_offset_of_resultIsReadOnly_0(),
	Builder_t1359586634::get_offset_of_result_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1929 = { sizeof (DepthMapEvent_t3758135118), -1, sizeof(DepthMapEvent_t3758135118_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1929[16] = 
{
	0,
	0,
	0,
	0,
	DepthMapEvent_t3758135118_StaticFields::get_offset_of_defaultInstance_4(),
	DepthMapEvent_t3758135118_StaticFields::get_offset_of__depthMapEventFieldNames_5(),
	DepthMapEvent_t3758135118_StaticFields::get_offset_of__depthMapEventFieldTags_6(),
	DepthMapEvent_t3758135118::get_offset_of_hasTimestamp_7(),
	DepthMapEvent_t3758135118::get_offset_of_timestamp__8(),
	DepthMapEvent_t3758135118::get_offset_of_hasWidth_9(),
	DepthMapEvent_t3758135118::get_offset_of_width__10(),
	DepthMapEvent_t3758135118::get_offset_of_hasHeight_11(),
	DepthMapEvent_t3758135118::get_offset_of_height__12(),
	DepthMapEvent_t3758135118::get_offset_of_zDistancesMemoizedSerializedSize_13(),
	DepthMapEvent_t3758135118::get_offset_of_zDistances__14(),
	DepthMapEvent_t3758135118::get_offset_of_memoizedSerializedSize_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1930 = { sizeof (Builder_t3987156410), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1930[2] = 
{
	Builder_t3987156410::get_offset_of_resultIsReadOnly_0(),
	Builder_t3987156410::get_offset_of_result_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1931 = { sizeof (OrientationEvent_t3825228477), -1, sizeof(OrientationEvent_t3825228477_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1931[19] = 
{
	0,
	0,
	0,
	0,
	0,
	OrientationEvent_t3825228477_StaticFields::get_offset_of_defaultInstance_5(),
	OrientationEvent_t3825228477_StaticFields::get_offset_of__orientationEventFieldNames_6(),
	OrientationEvent_t3825228477_StaticFields::get_offset_of__orientationEventFieldTags_7(),
	OrientationEvent_t3825228477::get_offset_of_hasTimestamp_8(),
	OrientationEvent_t3825228477::get_offset_of_timestamp__9(),
	OrientationEvent_t3825228477::get_offset_of_hasX_10(),
	OrientationEvent_t3825228477::get_offset_of_x__11(),
	OrientationEvent_t3825228477::get_offset_of_hasY_12(),
	OrientationEvent_t3825228477::get_offset_of_y__13(),
	OrientationEvent_t3825228477::get_offset_of_hasZ_14(),
	OrientationEvent_t3825228477::get_offset_of_z__15(),
	OrientationEvent_t3825228477::get_offset_of_hasW_16(),
	OrientationEvent_t3825228477::get_offset_of_w__17(),
	OrientationEvent_t3825228477::get_offset_of_memoizedSerializedSize_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1932 = { sizeof (Builder_t3974107689), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1932[2] = 
{
	Builder_t3974107689::get_offset_of_resultIsReadOnly_0(),
	Builder_t3974107689::get_offset_of_result_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1933 = { sizeof (KeyEvent_t2095740558), -1, sizeof(KeyEvent_t2095740558_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1933[10] = 
{
	0,
	0,
	KeyEvent_t2095740558_StaticFields::get_offset_of_defaultInstance_2(),
	KeyEvent_t2095740558_StaticFields::get_offset_of__keyEventFieldNames_3(),
	KeyEvent_t2095740558_StaticFields::get_offset_of__keyEventFieldTags_4(),
	KeyEvent_t2095740558::get_offset_of_hasAction_5(),
	KeyEvent_t2095740558::get_offset_of_action__6(),
	KeyEvent_t2095740558::get_offset_of_hasCode_7(),
	KeyEvent_t2095740558::get_offset_of_code__8(),
	KeyEvent_t2095740558::get_offset_of_memoizedSerializedSize_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1934 = { sizeof (Builder_t1063111930), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1934[2] = 
{
	Builder_t1063111930::get_offset_of_resultIsReadOnly_0(),
	Builder_t1063111930::get_offset_of_result_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1935 = { sizeof (Builder_t2932595742), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1935[2] = 
{
	Builder_t2932595742::get_offset_of_resultIsReadOnly_0(),
	Builder_t2932595742::get_offset_of_result_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1936 = { sizeof (GvrBasePointer_t3602508009), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1936[1] = 
{
	GvrBasePointer_t3602508009::get_offset_of_U3CShouldUseExitRadiusForRaycastU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1937 = { sizeof (GvrBasePointerRaycaster_t407851501), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1937[2] = 
{
	GvrBasePointerRaycaster_t407851501::get_offset_of_raycastMode_2(),
	GvrBasePointerRaycaster_t407851501::get_offset_of_lastRay_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1938 = { sizeof (RaycastMode_t525023850)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1938[3] = 
{
	RaycastMode_t525023850::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1939 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1940 = { sizeof (GvrExecuteEventsExtension_t2547249172), -1, sizeof(GvrExecuteEventsExtension_t2547249172_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1940[1] = 
{
	GvrExecuteEventsExtension_t2547249172_StaticFields::get_offset_of_s_HoverHandler_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1941 = { sizeof (GvrPointerGraphicRaycaster_t1071039080), -1, sizeof(GvrPointerGraphicRaycaster_t1071039080_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1941[9] = 
{
	0,
	GvrPointerGraphicRaycaster_t1071039080::get_offset_of_ignoreReversedGraphics_5(),
	GvrPointerGraphicRaycaster_t1071039080::get_offset_of_blockingObjects_6(),
	GvrPointerGraphicRaycaster_t1071039080::get_offset_of_blockingMask_7(),
	GvrPointerGraphicRaycaster_t1071039080::get_offset_of_targetCanvas_8(),
	GvrPointerGraphicRaycaster_t1071039080::get_offset_of_raycastResults_9(),
	GvrPointerGraphicRaycaster_t1071039080::get_offset_of_cachedPointerEventCamera_10(),
	GvrPointerGraphicRaycaster_t1071039080_StaticFields::get_offset_of_sortedGraphics_11(),
	GvrPointerGraphicRaycaster_t1071039080_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1942 = { sizeof (BlockingObjects_t2223317336)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1942[5] = 
{
	BlockingObjects_t2223317336::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1943 = { sizeof (GvrPointerInputModule_t2267916028), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1943[8] = 
{
	0,
	0,
	GvrPointerInputModule_t2267916028::get_offset_of_vrModeOnly_8(),
	GvrPointerInputModule_t2267916028::get_offset_of_pointerData_9(),
	GvrPointerInputModule_t2267916028::get_offset_of_lastPose_10(),
	GvrPointerInputModule_t2267916028::get_offset_of_lastScroll_11(),
	GvrPointerInputModule_t2267916028::get_offset_of_eligibleForScroll_12(),
	GvrPointerInputModule_t2267916028::get_offset_of_isActive_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1944 = { sizeof (GvrPointerPhysicsRaycaster_t1923475945), -1, sizeof(GvrPointerPhysicsRaycaster_t1923475945_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1944[4] = 
{
	0,
	GvrPointerPhysicsRaycaster_t1923475945::get_offset_of_raycasterEventMask_5(),
	GvrPointerPhysicsRaycaster_t1923475945::get_offset_of_cachedEventCamera_6(),
	GvrPointerPhysicsRaycaster_t1923475945_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1945 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1946 = { sizeof (GvrUnitySdkVersion_t3137951952), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1946[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1947 = { sizeof (GvrViewer_t671349045), -1, sizeof(GvrViewer_t671349045_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1947[20] = 
{
	0,
	GvrViewer_t671349045_StaticFields::get_offset_of_instance_3(),
	GvrViewer_t671349045_StaticFields::get_offset_of_currentController_4(),
	GvrViewer_t671349045_StaticFields::get_offset_of_currentMainCamera_5(),
	GvrViewer_t671349045::get_offset_of_vrModeEnabled_6(),
	GvrViewer_t671349045::get_offset_of_distortionCorrection_7(),
	GvrViewer_t671349045::get_offset_of_neckModelScale_8(),
	GvrViewer_t671349045_StaticFields::get_offset_of_device_9(),
	GvrViewer_t671349045::get_offset_of_stereoScreenScale_10(),
	GvrViewer_t671349045_StaticFields::get_offset_of_stereoScreen_11(),
	GvrViewer_t671349045::get_offset_of_defaultComfortableViewingRange_12(),
	GvrViewer_t671349045::get_offset_of_DefaultDeviceProfile_13(),
	GvrViewer_t671349045::get_offset_of_updatedToFrame_14(),
	GvrViewer_t671349045::get_offset_of_OnStereoScreenChanged_15(),
	GvrViewer_t671349045::get_offset_of_U3CNativeDistortionCorrectionSupportedU3Ek__BackingField_16(),
	GvrViewer_t671349045::get_offset_of_U3CNativeUILayerSupportedU3Ek__BackingField_17(),
	GvrViewer_t671349045::get_offset_of_U3CTriggeredU3Ek__BackingField_18(),
	GvrViewer_t671349045::get_offset_of_U3CTiltedU3Ek__BackingField_19(),
	GvrViewer_t671349045::get_offset_of_U3CProfileChangedU3Ek__BackingField_20(),
	GvrViewer_t671349045::get_offset_of_U3CBackButtonPressedU3Ek__BackingField_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1948 = { sizeof (DistortionCorrectionMethod_t2227033558)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1948[4] = 
{
	DistortionCorrectionMethod_t2227033558::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1949 = { sizeof (Eye_t643876407)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1949[4] = 
{
	Eye_t643876407::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1950 = { sizeof (Distortion_t2578366935)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1950[3] = 
{
	Distortion_t2578366935::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1951 = { sizeof (StereoScreenChangeDelegate_t2614477363), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1952 = { sizeof (Pose3D_t2396367586), -1, sizeof(Pose3D_t2396367586_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1952[4] = 
{
	Pose3D_t2396367586_StaticFields::get_offset_of_flipZ_0(),
	Pose3D_t2396367586::get_offset_of_U3CPositionU3Ek__BackingField_1(),
	Pose3D_t2396367586::get_offset_of_U3COrientationU3Ek__BackingField_2(),
	Pose3D_t2396367586::get_offset_of_U3CMatrixU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1953 = { sizeof (MutablePose3D_t1273683304), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1954 = { sizeof (GvrLaserPointer_t3144584661), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1955 = { sizeof (GvrReticlePointer_t2844437388), -1, sizeof(GvrReticlePointer_t2844437388_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1955[16] = 
{
	0,
	0,
	0,
	0,
	0,
	GvrReticlePointer_t2844437388::get_offset_of_reticleSegments_8(),
	GvrReticlePointer_t2844437388::get_offset_of_reticleGrowthSpeed_9(),
	GvrReticlePointer_t2844437388::get_offset_of_materialComp_10(),
	GvrReticlePointer_t2844437388::get_offset_of_reticleInnerAngle_11(),
	GvrReticlePointer_t2844437388::get_offset_of_reticleOuterAngle_12(),
	GvrReticlePointer_t2844437388::get_offset_of_reticleDistanceInMeters_13(),
	GvrReticlePointer_t2844437388::get_offset_of_reticleInnerDiameter_14(),
	GvrReticlePointer_t2844437388::get_offset_of_reticleOuterDiameter_15(),
	GvrReticlePointer_t2844437388::get_offset_of_gazeStartTime_16(),
	GvrReticlePointer_t2844437388::get_offset_of_gazedAt_17(),
	GvrReticlePointer_t2844437388_StaticFields::get_offset_of_U3CU3Ef__amU24cacheA_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1956 = { sizeof (GvrFPS_t2145111206), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1956[6] = 
{
	0,
	0,
	0,
	GvrFPS_t2145111206::get_offset_of_textField_5(),
	GvrFPS_t2145111206::get_offset_of_fps_6(),
	GvrFPS_t2145111206::get_offset_of_cam_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1957 = { sizeof (GvrIntent_t304217535), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1957[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1958 = { sizeof (GvrUIHelpers_t4187146094), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1959 = { sizeof (GvrVideoPlayerTexture_t4035356898), -1, sizeof(GvrVideoPlayerTexture_t4035356898_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1959[30] = 
{
	0,
	0,
	0,
	GvrVideoPlayerTexture_t4035356898::get_offset_of_videoTextures_5(),
	GvrVideoPlayerTexture_t4035356898::get_offset_of_currentTexture_6(),
	GvrVideoPlayerTexture_t4035356898::get_offset_of_videoPlayerPtr_7(),
	GvrVideoPlayerTexture_t4035356898::get_offset_of_videoPlayerEventBase_8(),
	GvrVideoPlayerTexture_t4035356898::get_offset_of_initialTexture_9(),
	GvrVideoPlayerTexture_t4035356898::get_offset_of_initialized_10(),
	GvrVideoPlayerTexture_t4035356898::get_offset_of_texWidth_11(),
	GvrVideoPlayerTexture_t4035356898::get_offset_of_texHeight_12(),
	GvrVideoPlayerTexture_t4035356898::get_offset_of_lastBufferedPosition_13(),
	GvrVideoPlayerTexture_t4035356898::get_offset_of_framecount_14(),
	GvrVideoPlayerTexture_t4035356898::get_offset_of_graphicComponent_15(),
	GvrVideoPlayerTexture_t4035356898::get_offset_of_rendererComponent_16(),
	GvrVideoPlayerTexture_t4035356898::get_offset_of_renderEventFunction_17(),
	GvrVideoPlayerTexture_t4035356898::get_offset_of_processingRunning_18(),
	GvrVideoPlayerTexture_t4035356898::get_offset_of_onEventCallbacks_19(),
	GvrVideoPlayerTexture_t4035356898::get_offset_of_onExceptionCallbacks_20(),
	GvrVideoPlayerTexture_t4035356898_StaticFields::get_offset_of_ExecuteOnMainThread_21(),
	GvrVideoPlayerTexture_t4035356898::get_offset_of_statusText_22(),
	GvrVideoPlayerTexture_t4035356898::get_offset_of_bufferSize_23(),
	GvrVideoPlayerTexture_t4035356898::get_offset_of_videoType_24(),
	GvrVideoPlayerTexture_t4035356898::get_offset_of_videoURL_25(),
	GvrVideoPlayerTexture_t4035356898::get_offset_of_videoContentID_26(),
	GvrVideoPlayerTexture_t4035356898::get_offset_of_videoProviderId_27(),
	GvrVideoPlayerTexture_t4035356898::get_offset_of_initialResolution_28(),
	GvrVideoPlayerTexture_t4035356898::get_offset_of_adjustAspectRatio_29(),
	GvrVideoPlayerTexture_t4035356898::get_offset_of_useSecurePath_30(),
	GvrVideoPlayerTexture_t4035356898_StaticFields::get_offset_of_U3CU3Ef__amU24cache1A_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1960 = { sizeof (VideoType_t2828778824)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1960[4] = 
{
	VideoType_t2828778824::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1961 = { sizeof (VideoResolution_t3968033850)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1961[6] = 
{
	VideoResolution_t3968033850::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1962 = { sizeof (VideoPlayerState_t1357341794)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1962[6] = 
{
	VideoPlayerState_t1357341794::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1963 = { sizeof (VideoEvents_t3604595623)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1963[6] = 
{
	VideoEvents_t3604595623::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1964 = { sizeof (RenderCommand_t2320763464)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1964[8] = 
{
	RenderCommand_t2320763464::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1965 = { sizeof (OnVideoEventCallback_t3220588464), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1966 = { sizeof (OnExceptionCallback_t1067313352), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1967 = { sizeof (U3CStartU3Ec__Iterator6_t1863601168), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1967[3] = 
{
	U3CStartU3Ec__Iterator6_t1863601168::get_offset_of_U24PC_0(),
	U3CStartU3Ec__Iterator6_t1863601168::get_offset_of_U24current_1(),
	U3CStartU3Ec__Iterator6_t1863601168::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1968 = { sizeof (U3CCallPluginAtEndOfFramesU3Ec__Iterator7_t3164122751), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1968[9] = 
{
	U3CCallPluginAtEndOfFramesU3Ec__Iterator7_t3164122751::get_offset_of_U3CrunningU3E__0_0(),
	U3CCallPluginAtEndOfFramesU3Ec__Iterator7_t3164122751::get_offset_of_U3CtexU3E__1_1(),
	U3CCallPluginAtEndOfFramesU3Ec__Iterator7_t3164122751::get_offset_of_U3CiU3E__2_2(),
	U3CCallPluginAtEndOfFramesU3Ec__Iterator7_t3164122751::get_offset_of_U3CwU3E__3_3(),
	U3CCallPluginAtEndOfFramesU3Ec__Iterator7_t3164122751::get_offset_of_U3ChU3E__4_4(),
	U3CCallPluginAtEndOfFramesU3Ec__Iterator7_t3164122751::get_offset_of_U3CbpU3E__5_5(),
	U3CCallPluginAtEndOfFramesU3Ec__Iterator7_t3164122751::get_offset_of_U24PC_6(),
	U3CCallPluginAtEndOfFramesU3Ec__Iterator7_t3164122751::get_offset_of_U24current_7(),
	U3CCallPluginAtEndOfFramesU3Ec__Iterator7_t3164122751::get_offset_of_U3CU3Ef__this_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1969 = { sizeof (U3CInternalOnVideoEventCallbackU3Ec__AnonStorey17_t3968160958), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1969[2] = 
{
	U3CInternalOnVideoEventCallbackU3Ec__AnonStorey17_t3968160958::get_offset_of_player_0(),
	U3CInternalOnVideoEventCallbackU3Ec__AnonStorey17_t3968160958::get_offset_of_eventId_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1970 = { sizeof (U3CInternalOnExceptionCallbackU3Ec__AnonStorey18_t2058796985), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1970[3] = 
{
	U3CInternalOnExceptionCallbackU3Ec__AnonStorey18_t2058796985::get_offset_of_player_0(),
	U3CInternalOnExceptionCallbackU3Ec__AnonStorey18_t2058796985::get_offset_of_type_1(),
	U3CInternalOnExceptionCallbackU3Ec__AnonStorey18_t2058796985::get_offset_of_msg_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1971 = { sizeof (MK_DemoControl_t1618197689), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1971[21] = 
{
	MK_DemoControl_t1618197689::get_offset_of_mkGlow_2(),
	MK_DemoControl_t1618197689::get_offset_of_bStyle_3(),
	MK_DemoControl_t1618197689::get_offset_of_r2bStyle_4(),
	MK_DemoControl_t1618197689::get_offset_of_skin_5(),
	MK_DemoControl_t1618197689::get_offset_of_currentRoom_6(),
	MK_DemoControl_t1618197689::get_offset_of_currentRoom2Texture_7(),
	MK_DemoControl_t1618197689::get_offset_of_currentRoom1Object_8(),
	MK_DemoControl_t1618197689::get_offset_of_currentRoom1Texture_9(),
	MK_DemoControl_t1618197689::get_offset_of_currentRoom1GlowColor_10(),
	MK_DemoControl_t1618197689::get_offset_of_currentRoom1GlowTexColor_11(),
	MK_DemoControl_t1618197689::get_offset_of_currentRoom0GlowColor_12(),
	MK_DemoControl_t1618197689::get_offset_of_currentRoom1Shader_13(),
	MK_DemoControl_t1618197689::get_offset_of_room1Shaders_14(),
	MK_DemoControl_t1618197689::get_offset_of_cm_15(),
	MK_DemoControl_t1618197689::get_offset_of_room1RimP_16(),
	MK_DemoControl_t1618197689::get_offset_of_currentRimColor_17(),
	MK_DemoControl_t1618197689::get_offset_of_room1GlowIntensity_18(),
	MK_DemoControl_t1618197689::get_offset_of_room1GlowTextureStrength_19(),
	MK_DemoControl_t1618197689::get_offset_of_room2Tex_20(),
	MK_DemoControl_t1618197689::get_offset_of_room2Objects_21(),
	MK_DemoControl_t1618197689::get_offset_of_room1Objects_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1972 = { sizeof (MK_RotateObject_t404849051), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1972[1] = 
{
	MK_RotateObject_t404849051::get_offset_of_speed_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1973 = { sizeof (MKGlow_t3477081437), -1, sizeof(MKGlow_t3477081437_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1973[19] = 
{
	MKGlow_t3477081437_StaticFields::get_offset_of_gaussFilter_2(),
	MKGlow_t3477081437::get_offset_of_blurShader_3(),
	MKGlow_t3477081437::get_offset_of_compositeShader_4(),
	MKGlow_t3477081437::get_offset_of_glowRenderShader_5(),
	MKGlow_t3477081437::get_offset_of_compositeMaterial_6(),
	MKGlow_t3477081437::get_offset_of_blurMaterial_7(),
	MKGlow_t3477081437::get_offset_of_glowCamera_8(),
	MKGlow_t3477081437::get_offset_of_glowCameraObject_9(),
	MKGlow_t3477081437::get_offset_of_glowTexture_10(),
	MKGlow_t3477081437::get_offset_of_glowLayer_11(),
	MKGlow_t3477081437::get_offset_of_renderCamera_12(),
	MKGlow_t3477081437::get_offset_of_showCutoutGlow_13(),
	MKGlow_t3477081437::get_offset_of_showTransparentGlow_14(),
	MKGlow_t3477081437::get_offset_of_glowType_15(),
	MKGlow_t3477081437::get_offset_of_fullScreenGlowTint_16(),
	MKGlow_t3477081437::get_offset_of_blurSpread_17(),
	MKGlow_t3477081437::get_offset_of_blurIterations_18(),
	MKGlow_t3477081437::get_offset_of_glowIntensity_19(),
	MKGlow_t3477081437::get_offset_of_samples_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1974 = { sizeof (MKGlowType_t2044315959)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1974[3] = 
{
	MKGlowType_t2044315959::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1975 = { sizeof (Autowalk_t1504404504), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1975[9] = 
{
	0,
	Autowalk_t1504404504::get_offset_of_isWalking_3(),
	Autowalk_t1504404504::get_offset_of_mainCamera_4(),
	Autowalk_t1504404504::get_offset_of_speed_5(),
	Autowalk_t1504404504::get_offset_of_walkWhenTriggered_6(),
	Autowalk_t1504404504::get_offset_of_walkWhenLookDown_7(),
	Autowalk_t1504404504::get_offset_of_thresholdAngle_8(),
	Autowalk_t1504404504::get_offset_of_freezeYPosition_9(),
	Autowalk_t1504404504::get_offset_of_yOffset_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1976 = { sizeof (Bidding_t1547197275), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1976[4] = 
{
	Bidding_t1547197275::get_offset_of_biddingText_2(),
	Bidding_t1547197275::get_offset_of_clearButton_3(),
	Bidding_t1547197275::get_offset_of_dealButton_4(),
	Bidding_t1547197275::get_offset_of_bid_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1977 = { sizeof (BlackjackGame_t1439740240), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1977[6] = 
{
	BlackjackGame_t1439740240::get_offset_of_dealerScore_2(),
	BlackjackGame_t1439740240::get_offset_of_playerScore_3(),
	BlackjackGame_t1439740240::get_offset_of_playerBid_4(),
	BlackjackGame_t1439740240::get_offset_of_dealerCardsGO_5(),
	BlackjackGame_t1439740240::get_offset_of_playerCardsGO_6(),
	BlackjackGame_t1439740240::get_offset_of_game_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1978 = { sizeof (U3CinitCardActionUpdateU3Ec__Iterator8_t1966516297), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1978[4] = 
{
	U3CinitCardActionUpdateU3Ec__Iterator8_t1966516297::get_offset_of_U3CdealerCardU3E__0_0(),
	U3CinitCardActionUpdateU3Ec__Iterator8_t1966516297::get_offset_of_U24PC_1(),
	U3CinitCardActionUpdateU3Ec__Iterator8_t1966516297::get_offset_of_U24current_2(),
	U3CinitCardActionUpdateU3Ec__Iterator8_t1966516297::get_offset_of_U3CU3Ef__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1979 = { sizeof (U3CdrawNewPlayerCardsU3Ec__Iterator9_t230613443), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1979[12] = 
{
	U3CdrawNewPlayerCardsU3Ec__Iterator9_t230613443::get_offset_of_U3CiU3E__0_0(),
	U3CdrawNewPlayerCardsU3Ec__Iterator9_t230613443::get_offset_of_cards_1(),
	U3CdrawNewPlayerCardsU3Ec__Iterator9_t230613443::get_offset_of_U3CcardU3E__1_2(),
	U3CdrawNewPlayerCardsU3Ec__Iterator9_t230613443::get_offset_of_U3CdrawWithDelayU3E__2_3(),
	U3CdrawNewPlayerCardsU3Ec__Iterator9_t230613443::get_offset_of_U3CcardGOU3E__3_4(),
	U3CdrawNewPlayerCardsU3Ec__Iterator9_t230613443::get_offset_of_U3CdeltaXU3E__4_5(),
	U3CdrawNewPlayerCardsU3Ec__Iterator9_t230613443::get_offset_of_U3CdeltaYU3E__5_6(),
	U3CdrawNewPlayerCardsU3Ec__Iterator9_t230613443::get_offset_of_U3CdeltaZU3E__6_7(),
	U3CdrawNewPlayerCardsU3Ec__Iterator9_t230613443::get_offset_of_U24PC_8(),
	U3CdrawNewPlayerCardsU3Ec__Iterator9_t230613443::get_offset_of_U24current_9(),
	U3CdrawNewPlayerCardsU3Ec__Iterator9_t230613443::get_offset_of_U3CU24U3Ecards_10(),
	U3CdrawNewPlayerCardsU3Ec__Iterator9_t230613443::get_offset_of_U3CU3Ef__this_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1980 = { sizeof (U3CshowDealerCardsU3Ec__IteratorA_t1840808414), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1980[4] = 
{
	U3CshowDealerCardsU3Ec__IteratorA_t1840808414::get_offset_of_U3CcardGOU3E__0_0(),
	U3CshowDealerCardsU3Ec__IteratorA_t1840808414::get_offset_of_U24PC_1(),
	U3CshowDealerCardsU3Ec__IteratorA_t1840808414::get_offset_of_U24current_2(),
	U3CshowDealerCardsU3Ec__IteratorA_t1840808414::get_offset_of_U3CU3Ef__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1981 = { sizeof (U3CdrawNewDealerCardsU3Ec__IteratorB_t2458683140), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1981[14] = 
{
	U3CdrawNewDealerCardsU3Ec__IteratorB_t2458683140::get_offset_of_U3CiU3E__0_0(),
	U3CdrawNewDealerCardsU3Ec__IteratorB_t2458683140::get_offset_of_cards_1(),
	U3CdrawNewDealerCardsU3Ec__IteratorB_t2458683140::get_offset_of_U3CcardU3E__1_2(),
	U3CdrawNewDealerCardsU3Ec__IteratorB_t2458683140::get_offset_of_U3CdrawWithDelayU3E__2_3(),
	U3CdrawNewDealerCardsU3Ec__IteratorB_t2458683140::get_offset_of_U3CcardGOU3E__3_4(),
	U3CdrawNewDealerCardsU3Ec__IteratorB_t2458683140::get_offset_of_U3CdeltaXU3E__4_5(),
	U3CdrawNewDealerCardsU3Ec__IteratorB_t2458683140::get_offset_of_U3CdeltaYU3E__5_6(),
	U3CdrawNewDealerCardsU3Ec__IteratorB_t2458683140::get_offset_of_U3CdeltaZU3E__6_7(),
	U3CdrawNewDealerCardsU3Ec__IteratorB_t2458683140::get_offset_of_showResult_8(),
	U3CdrawNewDealerCardsU3Ec__IteratorB_t2458683140::get_offset_of_U24PC_9(),
	U3CdrawNewDealerCardsU3Ec__IteratorB_t2458683140::get_offset_of_U24current_10(),
	U3CdrawNewDealerCardsU3Ec__IteratorB_t2458683140::get_offset_of_U3CU24U3Ecards_11(),
	U3CdrawNewDealerCardsU3Ec__IteratorB_t2458683140::get_offset_of_U3CU24U3EshowResult_12(),
	U3CdrawNewDealerCardsU3Ec__IteratorB_t2458683140::get_offset_of_U3CU3Ef__this_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1982 = { sizeof (U3CplayerDidWonGameU3Ec__IteratorC_t2191380681), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1982[5] = 
{
	U3CplayerDidWonGameU3Ec__IteratorC_t2191380681::get_offset_of_U3CplayerBackgroundU3E__0_0(),
	U3CplayerDidWonGameU3Ec__IteratorC_t2191380681::get_offset_of_U3CnewMatU3E__1_1(),
	U3CplayerDidWonGameU3Ec__IteratorC_t2191380681::get_offset_of_U24PC_2(),
	U3CplayerDidWonGameU3Ec__IteratorC_t2191380681::get_offset_of_U24current_3(),
	U3CplayerDidWonGameU3Ec__IteratorC_t2191380681::get_offset_of_U3CU3Ef__this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1983 = { sizeof (U3CplayerDidLostGameU3Ec__IteratorD_t2335290970), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1983[5] = 
{
	U3CplayerDidLostGameU3Ec__IteratorD_t2335290970::get_offset_of_U3CplayerBackgroundU3E__0_0(),
	U3CplayerDidLostGameU3Ec__IteratorD_t2335290970::get_offset_of_U3CnewMatU3E__1_1(),
	U3CplayerDidLostGameU3Ec__IteratorD_t2335290970::get_offset_of_U24PC_2(),
	U3CplayerDidLostGameU3Ec__IteratorD_t2335290970::get_offset_of_U24current_3(),
	U3CplayerDidLostGameU3Ec__IteratorD_t2335290970::get_offset_of_U3CU3Ef__this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1984 = { sizeof (U3CplayerPlayDrawGameU3Ec__IteratorE_t223069034), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1984[5] = 
{
	U3CplayerPlayDrawGameU3Ec__IteratorE_t223069034::get_offset_of_U3CplayerBackgroundU3E__0_0(),
	U3CplayerPlayDrawGameU3Ec__IteratorE_t223069034::get_offset_of_U3CnewMatU3E__1_1(),
	U3CplayerPlayDrawGameU3Ec__IteratorE_t223069034::get_offset_of_U24PC_2(),
	U3CplayerPlayDrawGameU3Ec__IteratorE_t223069034::get_offset_of_U24current_3(),
	U3CplayerPlayDrawGameU3Ec__IteratorE_t223069034::get_offset_of_U3CU3Ef__this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1985 = { sizeof (U3CendGameU3Ec__IteratorF_t2718692611), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1985[3] = 
{
	U3CendGameU3Ec__IteratorF_t2718692611::get_offset_of_U24PC_0(),
	U3CendGameU3Ec__IteratorF_t2718692611::get_offset_of_U24current_1(),
	U3CendGameU3Ec__IteratorF_t2718692611::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1986 = { sizeof (U3CplayerDidFinishGameU3Ec__Iterator10_t399988280), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1986[5] = 
{
	U3CplayerDidFinishGameU3Ec__Iterator10_t399988280::get_offset_of_result_0(),
	U3CplayerDidFinishGameU3Ec__Iterator10_t399988280::get_offset_of_U24PC_1(),
	U3CplayerDidFinishGameU3Ec__Iterator10_t399988280::get_offset_of_U24current_2(),
	U3CplayerDidFinishGameU3Ec__Iterator10_t399988280::get_offset_of_U3CU24U3Eresult_3(),
	U3CplayerDidFinishGameU3Ec__Iterator10_t399988280::get_offset_of_U3CU3Ef__this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1987 = { sizeof (Suit_t540024807)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1987[5] = 
{
	Suit_t540024807::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1988 = { sizeof (CardType_t365306692)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1988[14] = 
{
	CardType_t365306692::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1989 = { sizeof (Card_t539529194)+ sizeof (Il2CppObject), sizeof(Card_t539529194_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1989[3] = 
{
	Card_t539529194::get_offset_of_suit_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Card_t539529194::get_offset_of_type_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Card_t539529194::get_offset_of_values_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1990 = { sizeof (Deck_t539562371), -1, sizeof(Deck_t539562371_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1990[3] = 
{
	Deck_t539562371::get_offset_of_cardDeck_0(),
	Deck_t539562371::get_offset_of_gen_1(),
	Deck_t539562371_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1991 = { sizeof (User_t540082341)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1991[4] = 
{
	User_t540082341::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1992 = { sizeof (WinType_t832601200)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1992[4] = 
{
	WinType_t832601200::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1993 = { sizeof (Decision_t939252918)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1993[5] = 
{
	Decision_t939252918::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1994 = { sizeof (GameResult_t3329670025)+ sizeof (Il2CppObject), sizeof(GameResult_t3329670025_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1994[5] = 
{
	GameResult_t3329670025::get_offset_of_type_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GameResult_t3329670025::get_offset_of_winner_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GameResult_t3329670025::get_offset_of_delaerPoints_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GameResult_t3329670025::get_offset_of_playerPoints_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GameResult_t3329670025::get_offset_of_winAmount_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1995 = { sizeof (Game_t539648204), -1, sizeof(Game_t539648204_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1995[10] = 
{
	Game_t539648204::get_offset_of_deck_0(),
	Game_t539648204::get_offset_of_dealerCards_1(),
	Game_t539648204::get_offset_of_playerCards_2(),
	Game_t539648204::get_offset_of_chips_3(),
	Game_t539648204::get_offset_of_canDouble_4(),
	Game_t539648204::get_offset_of_playerOver_5(),
	Game_t539648204::get_offset_of_playerPoints_6(),
	Game_t539648204::get_offset_of_dealerPoints_7(),
	Game_t539648204_StaticFields::get_offset_of_U3CU3Ef__amU24cache8_8(),
	Game_t539648204_StaticFields::get_offset_of_U3CU3Ef__amU24cache9_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1996 = { sizeof (GameButtonsSelection_t1942508349), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1997 = { sizeof (GameSelectionManager_t3465822387), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1997[2] = 
{
	GameSelectionManager_t3465822387::get_offset_of_allButtons_2(),
	GameSelectionManager_t3465822387::get_offset_of_currentGame_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1998 = { sizeof (PlayerState_t2213905936)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1998[6] = 
{
	PlayerState_t2213905936::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1999 = { sizeof (PlayerManager_t4287196524), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1999[7] = 
{
	PlayerManager_t4287196524::get_offset_of_playerMoney_5(),
	PlayerManager_t4287196524::get_offset_of_deck_6(),
	PlayerManager_t4287196524::get_offset_of_state_7(),
	PlayerManager_t4287196524::get_offset_of_blackJackManager_8(),
	PlayerManager_t4287196524::get_offset_of_player_9(),
	PlayerManager_t4287196524::get_offset_of_mainDealer_10(),
	PlayerManager_t4287196524::get_offset_of_playerScoreText_11(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
