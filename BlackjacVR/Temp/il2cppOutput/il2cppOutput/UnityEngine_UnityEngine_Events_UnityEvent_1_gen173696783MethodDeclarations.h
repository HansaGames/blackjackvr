﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Events.UnityEvent`1<UnityEngine.Vector3>
struct UnityEvent_1_t173696783;
// UnityEngine.Events.UnityAction`1<UnityEngine.Vector3>
struct UnityAction_1_t4176246569;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// UnityEngine.Events.BaseInvokableCall
struct BaseInvokableCall_t1559630662;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Reflection_MethodInfo318736065.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Vector3>::.ctor()
extern "C"  void UnityEvent_1__ctor_m3935009870_gshared (UnityEvent_1_t173696783 * __this, const MethodInfo* method);
#define UnityEvent_1__ctor_m3935009870(__this, method) ((  void (*) (UnityEvent_1_t173696783 *, const MethodInfo*))UnityEvent_1__ctor_m3935009870_gshared)(__this, method)
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Vector3>::AddListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  void UnityEvent_1_AddListener_m2644241798_gshared (UnityEvent_1_t173696783 * __this, UnityAction_1_t4176246569 * ___call0, const MethodInfo* method);
#define UnityEvent_1_AddListener_m2644241798(__this, ___call0, method) ((  void (*) (UnityEvent_1_t173696783 *, UnityAction_1_t4176246569 *, const MethodInfo*))UnityEvent_1_AddListener_m2644241798_gshared)(__this, ___call0, method)
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Vector3>::RemoveListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  void UnityEvent_1_RemoveListener_m2746342017_gshared (UnityEvent_1_t173696783 * __this, UnityAction_1_t4176246569 * ___call0, const MethodInfo* method);
#define UnityEvent_1_RemoveListener_m2746342017(__this, ___call0, method) ((  void (*) (UnityEvent_1_t173696783 *, UnityAction_1_t4176246569 *, const MethodInfo*))UnityEvent_1_RemoveListener_m2746342017_gshared)(__this, ___call0, method)
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`1<UnityEngine.Vector3>::FindMethod_Impl(System.String,System.Object)
extern "C"  MethodInfo_t * UnityEvent_1_FindMethod_Impl_m606274546_gshared (UnityEvent_1_t173696783 * __this, String_t* ___name0, Il2CppObject * ___targetObj1, const MethodInfo* method);
#define UnityEvent_1_FindMethod_Impl_m606274546(__this, ___name0, ___targetObj1, method) ((  MethodInfo_t * (*) (UnityEvent_1_t173696783 *, String_t*, Il2CppObject *, const MethodInfo*))UnityEvent_1_FindMethod_Impl_m606274546_gshared)(__this, ___name0, ___targetObj1, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<UnityEngine.Vector3>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C"  BaseInvokableCall_t1559630662 * UnityEvent_1_GetDelegate_m1327259510_gshared (UnityEvent_1_t173696783 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method);
#define UnityEvent_1_GetDelegate_m1327259510(__this, ___target0, ___theFunction1, method) ((  BaseInvokableCall_t1559630662 * (*) (UnityEvent_1_t173696783 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))UnityEvent_1_GetDelegate_m1327259510_gshared)(__this, ___target0, ___theFunction1, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<UnityEngine.Vector3>::GetDelegate(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  BaseInvokableCall_t1559630662 * UnityEvent_1_GetDelegate_m437362323_gshared (Il2CppObject * __this /* static, unused */, UnityAction_1_t4176246569 * ___action0, const MethodInfo* method);
#define UnityEvent_1_GetDelegate_m437362323(__this /* static, unused */, ___action0, method) ((  BaseInvokableCall_t1559630662 * (*) (Il2CppObject * /* static, unused */, UnityAction_1_t4176246569 *, const MethodInfo*))UnityEvent_1_GetDelegate_m437362323_gshared)(__this /* static, unused */, ___action0, method)
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Vector3>::Invoke(T0)
extern "C"  void UnityEvent_1_Invoke_m2575287433_gshared (UnityEvent_1_t173696783 * __this, Vector3_t4282066566  ___arg00, const MethodInfo* method);
#define UnityEvent_1_Invoke_m2575287433(__this, ___arg00, method) ((  void (*) (UnityEvent_1_t173696783 *, Vector3_t4282066566 , const MethodInfo*))UnityEvent_1_Invoke_m2575287433_gshared)(__this, ___arg00, method)
