﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GvrAudio/RoomProperties
struct RoomProperties_t3951668682;
struct RoomProperties_t3951668682_marshaled_pinvoke;
struct RoomProperties_t3951668682_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct RoomProperties_t3951668682;
struct RoomProperties_t3951668682_marshaled_pinvoke;

extern "C" void RoomProperties_t3951668682_marshal_pinvoke(const RoomProperties_t3951668682& unmarshaled, RoomProperties_t3951668682_marshaled_pinvoke& marshaled);
extern "C" void RoomProperties_t3951668682_marshal_pinvoke_back(const RoomProperties_t3951668682_marshaled_pinvoke& marshaled, RoomProperties_t3951668682& unmarshaled);
extern "C" void RoomProperties_t3951668682_marshal_pinvoke_cleanup(RoomProperties_t3951668682_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct RoomProperties_t3951668682;
struct RoomProperties_t3951668682_marshaled_com;

extern "C" void RoomProperties_t3951668682_marshal_com(const RoomProperties_t3951668682& unmarshaled, RoomProperties_t3951668682_marshaled_com& marshaled);
extern "C" void RoomProperties_t3951668682_marshal_com_back(const RoomProperties_t3951668682_marshaled_com& marshaled, RoomProperties_t3951668682& unmarshaled);
extern "C" void RoomProperties_t3951668682_marshal_com_cleanup(RoomProperties_t3951668682_marshaled_com& marshaled);
