﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BlackjackGame/<drawNewDealerCards>c__IteratorB
struct U3CdrawNewDealerCardsU3Ec__IteratorB_t2458683140;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void BlackjackGame/<drawNewDealerCards>c__IteratorB::.ctor()
extern "C"  void U3CdrawNewDealerCardsU3Ec__IteratorB__ctor_m721455639 (U3CdrawNewDealerCardsU3Ec__IteratorB_t2458683140 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object BlackjackGame/<drawNewDealerCards>c__IteratorB::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CdrawNewDealerCardsU3Ec__IteratorB_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1780293477 (U3CdrawNewDealerCardsU3Ec__IteratorB_t2458683140 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object BlackjackGame/<drawNewDealerCards>c__IteratorB::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CdrawNewDealerCardsU3Ec__IteratorB_System_Collections_IEnumerator_get_Current_m3766258937 (U3CdrawNewDealerCardsU3Ec__IteratorB_t2458683140 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BlackjackGame/<drawNewDealerCards>c__IteratorB::MoveNext()
extern "C"  bool U3CdrawNewDealerCardsU3Ec__IteratorB_MoveNext_m1633076581 (U3CdrawNewDealerCardsU3Ec__IteratorB_t2458683140 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BlackjackGame/<drawNewDealerCards>c__IteratorB::Dispose()
extern "C"  void U3CdrawNewDealerCardsU3Ec__IteratorB_Dispose_m1727417492 (U3CdrawNewDealerCardsU3Ec__IteratorB_t2458683140 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BlackjackGame/<drawNewDealerCards>c__IteratorB::Reset()
extern "C"  void U3CdrawNewDealerCardsU3Ec__IteratorB_Reset_m2662855876 (U3CdrawNewDealerCardsU3Ec__IteratorB_t2458683140 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
