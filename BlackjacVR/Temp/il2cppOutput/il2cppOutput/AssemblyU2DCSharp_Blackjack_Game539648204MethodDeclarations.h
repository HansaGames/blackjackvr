﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Blackjack.Game
struct Game_t539648204;
// System.Collections.Generic.List`1<Blackjack.Card>
struct List_1_t1907714746;
// System.Collections.Generic.List`1<Blackjack.Decision>
struct List_1_t2307438470;
// Blackjack.Card[]
struct CardU5BU5D_t1346973999;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Blackjack_GameResult3329670025.h"
#include "AssemblyU2DCSharp_Blackjack_Card539529194.h"

// System.Void Blackjack.Game::.ctor(System.Collections.Generic.List`1<Blackjack.Card>)
extern "C"  void Game__ctor_m1393738713 (Game_t539648204 * __this, List_1_t1907714746 * ___deck0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Blackjack.Decision> Blackjack.Game::getDecisions()
extern "C"  List_1_t2307438470 * Game_getDecisions_m2719775499 (Game_t539648204 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Blackjack.Game::isGameOver()
extern "C"  bool Game_isGameOver_m3580498767 (Game_t539648204 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Blackjack.Card[] Blackjack.Game::getDealerCards()
extern "C"  CardU5BU5D_t1346973999* Game_getDealerCards_m987404986 (Game_t539648204 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Blackjack.Card[] Blackjack.Game::getPlayerCards()
extern "C"  CardU5BU5D_t1346973999* Game_getPlayerCards_m110439666 (Game_t539648204 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Blackjack.GameResult Blackjack.Game::getGameResult()
extern "C"  GameResult_t3329670025  Game_getGameResult_m1529646430 (Game_t539648204 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Blackjack.Game::getPlayerPoints()
extern "C"  int32_t Game_getPlayerPoints_m1368404899 (Game_t539648204 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Blackjack.Game::getDealerPoints()
extern "C"  int32_t Game_getDealerPoints_m2784526043 (Game_t539648204 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Blackjack.Card Blackjack.Game::playerHit()
extern "C"  Card_t539529194  Game_playerHit_m724001164 (Game_t539648204 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Blackjack.Game::playerStands()
extern "C"  void Game_playerStands_m3193314057 (Game_t539648204 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Blackjack.Game::playerDouble()
extern "C"  void Game_playerDouble_m3240137341 (Game_t539648204 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Blackjack.Game::addChips(System.Double)
extern "C"  void Game_addChips_m2758782193 (Game_t539648204 * __this, double ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Blackjack.Game::removeChips(System.Double)
extern "C"  void Game_removeChips_m2897374952 (Game_t539648204 * __this, double ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double Blackjack.Game::getCurrentChips()
extern "C"  double Game_getCurrentChips_m3395500024 (Game_t539648204 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Blackjack.Card Blackjack.Game::takeNextCard()
extern "C"  Card_t539529194  Game_takeNextCard_m3295386130 (Game_t539648204 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Blackjack.Game::countPoints(System.Collections.Generic.List`1<Blackjack.Card>)
extern "C"  int32_t Game_countPoints_m3263376471 (Game_t539648204 * __this, List_1_t1907714746 * ___cards0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Blackjack.Game::dealerTurn()
extern "C"  void Game_dealerTurn_m3610268705 (Game_t539648204 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Blackjack.Card Blackjack.Game::<getDealerCards>m__D(Blackjack.Card)
extern "C"  Card_t539529194  Game_U3CgetDealerCardsU3Em__D_m4228568429 (Il2CppObject * __this /* static, unused */, Card_t539529194  ___card0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Blackjack.Card Blackjack.Game::<getPlayerCards>m__E(Blackjack.Card)
extern "C"  Card_t539529194  Game_U3CgetPlayerCardsU3Em__E_m1516488246 (Il2CppObject * __this /* static, unused */, Card_t539529194  ___card0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
