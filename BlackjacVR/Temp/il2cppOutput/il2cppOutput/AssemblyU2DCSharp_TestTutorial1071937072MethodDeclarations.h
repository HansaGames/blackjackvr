﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TestTutorial
struct TestTutorial_t1071937072;
// Blackjack.Game
struct Game_t539648204;
// UnityEngine.AudioClip
struct AudioClip_t794140988;

#include "codegen/il2cpp-codegen.h"

// System.Void TestTutorial::.ctor()
extern "C"  void TestTutorial__ctor_m2420994667 (TestTutorial_t1071937072 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestTutorial::Start()
extern "C"  void TestTutorial_Start_m1368132459 (TestTutorial_t1071937072 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestTutorial::Update()
extern "C"  void TestTutorial_Update_m3763252738 (TestTutorial_t1071937072 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestTutorial::startTutorial()
extern "C"  void TestTutorial_startTutorial_m4121096585 (TestTutorial_t1071937072 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Blackjack.Game TestTutorial::getGame()
extern "C"  Game_t539648204 * TestTutorial_getGame_m1355249718 (TestTutorial_t1071937072 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioClip TestTutorial::getTutorialClip()
extern "C"  AudioClip_t794140988 * TestTutorial_getTutorialClip_m2102739881 (TestTutorial_t1071937072 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
