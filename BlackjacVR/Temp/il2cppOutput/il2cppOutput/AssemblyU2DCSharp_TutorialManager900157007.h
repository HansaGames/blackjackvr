﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// BaseTutorial
struct BaseTutorial_t2983299439;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TutorialManager
struct  TutorialManager_t900157007  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GameObject TutorialManager::tutorialButtons
	GameObject_t3674682005 * ___tutorialButtons_2;
	// System.Int32 TutorialManager::currentTutorial
	int32_t ___currentTutorial_3;
	// BaseTutorial TutorialManager::currentTutorialGO
	BaseTutorial_t2983299439 * ___currentTutorialGO_4;

public:
	inline static int32_t get_offset_of_tutorialButtons_2() { return static_cast<int32_t>(offsetof(TutorialManager_t900157007, ___tutorialButtons_2)); }
	inline GameObject_t3674682005 * get_tutorialButtons_2() const { return ___tutorialButtons_2; }
	inline GameObject_t3674682005 ** get_address_of_tutorialButtons_2() { return &___tutorialButtons_2; }
	inline void set_tutorialButtons_2(GameObject_t3674682005 * value)
	{
		___tutorialButtons_2 = value;
		Il2CppCodeGenWriteBarrier(&___tutorialButtons_2, value);
	}

	inline static int32_t get_offset_of_currentTutorial_3() { return static_cast<int32_t>(offsetof(TutorialManager_t900157007, ___currentTutorial_3)); }
	inline int32_t get_currentTutorial_3() const { return ___currentTutorial_3; }
	inline int32_t* get_address_of_currentTutorial_3() { return &___currentTutorial_3; }
	inline void set_currentTutorial_3(int32_t value)
	{
		___currentTutorial_3 = value;
	}

	inline static int32_t get_offset_of_currentTutorialGO_4() { return static_cast<int32_t>(offsetof(TutorialManager_t900157007, ___currentTutorialGO_4)); }
	inline BaseTutorial_t2983299439 * get_currentTutorialGO_4() const { return ___currentTutorialGO_4; }
	inline BaseTutorial_t2983299439 ** get_address_of_currentTutorialGO_4() { return &___currentTutorialGO_4; }
	inline void set_currentTutorialGO_4(BaseTutorial_t2983299439 * value)
	{
		___currentTutorialGO_4 = value;
		Il2CppCodeGenWriteBarrier(&___currentTutorialGO_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
