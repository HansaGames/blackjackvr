﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BlackjackGame/<playerDidLostGame>c__IteratorD
struct U3CplayerDidLostGameU3Ec__IteratorD_t2335290970;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void BlackjackGame/<playerDidLostGame>c__IteratorD::.ctor()
extern "C"  void U3CplayerDidLostGameU3Ec__IteratorD__ctor_m2433323089 (U3CplayerDidLostGameU3Ec__IteratorD_t2335290970 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object BlackjackGame/<playerDidLostGame>c__IteratorD::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CplayerDidLostGameU3Ec__IteratorD_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4233337953 (U3CplayerDidLostGameU3Ec__IteratorD_t2335290970 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object BlackjackGame/<playerDidLostGame>c__IteratorD::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CplayerDidLostGameU3Ec__IteratorD_System_Collections_IEnumerator_get_Current_m3521413621 (U3CplayerDidLostGameU3Ec__IteratorD_t2335290970 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BlackjackGame/<playerDidLostGame>c__IteratorD::MoveNext()
extern "C"  bool U3CplayerDidLostGameU3Ec__IteratorD_MoveNext_m2290354947 (U3CplayerDidLostGameU3Ec__IteratorD_t2335290970 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BlackjackGame/<playerDidLostGame>c__IteratorD::Dispose()
extern "C"  void U3CplayerDidLostGameU3Ec__IteratorD_Dispose_m1859562574 (U3CplayerDidLostGameU3Ec__IteratorD_t2335290970 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BlackjackGame/<playerDidLostGame>c__IteratorD::Reset()
extern "C"  void U3CplayerDidLostGameU3Ec__IteratorD_Reset_m79756030 (U3CplayerDidLostGameU3Ec__IteratorD_t2335290970 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
