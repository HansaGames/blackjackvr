﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameSelectionManager
struct GameSelectionManager_t3465822387;

#include "codegen/il2cpp-codegen.h"

// System.Void GameSelectionManager::.ctor()
extern "C"  void GameSelectionManager__ctor_m2595631944 (GameSelectionManager_t3465822387 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameSelectionManager::Start()
extern "C"  void GameSelectionManager_Start_m1542769736 (GameSelectionManager_t3465822387 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameSelectionManager::Update()
extern "C"  void GameSelectionManager_Update_m587073733 (GameSelectionManager_t3465822387 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameSelectionManager::HandleTimedInput()
extern "C"  void GameSelectionManager_HandleTimedInput_m1236826327 (GameSelectionManager_t3465822387 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameSelectionManager::startTutorial()
extern "C"  void GameSelectionManager_startTutorial_m1784644966 (GameSelectionManager_t3465822387 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameSelectionManager::startGame()
extern "C"  void GameSelectionManager_startGame_m1433021562 (GameSelectionManager_t3465822387 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameSelectionManager::enterBidsState()
extern "C"  void GameSelectionManager_enterBidsState_m578710911 (GameSelectionManager_t3465822387 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameSelectionManager::enterGameState(System.Int32)
extern "C"  void GameSelectionManager_enterGameState_m2384588276 (GameSelectionManager_t3465822387 * __this, int32_t ___bid0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameSelectionManager::enterTable()
extern "C"  void GameSelectionManager_enterTable_m3893994226 (GameSelectionManager_t3465822387 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameSelectionManager::dismissFromTable()
extern "C"  void GameSelectionManager_dismissFromTable_m1045290166 (GameSelectionManager_t3465822387 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameSelectionManager::SetAllCollidersStatus(System.Boolean)
extern "C"  void GameSelectionManager_SetAllCollidersStatus_m393529679 (GameSelectionManager_t3465822387 * __this, bool ___active0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
