﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StrategyTutorial/<stiffAndPatStrategy>c__Iterator15
struct U3CstiffAndPatStrategyU3Ec__Iterator15_t642946848;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void StrategyTutorial/<stiffAndPatStrategy>c__Iterator15::.ctor()
extern "C"  void U3CstiffAndPatStrategyU3Ec__Iterator15__ctor_m500031563 (U3CstiffAndPatStrategyU3Ec__Iterator15_t642946848 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object StrategyTutorial/<stiffAndPatStrategy>c__Iterator15::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CstiffAndPatStrategyU3Ec__Iterator15_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m486492263 (U3CstiffAndPatStrategyU3Ec__Iterator15_t642946848 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object StrategyTutorial/<stiffAndPatStrategy>c__Iterator15::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CstiffAndPatStrategyU3Ec__Iterator15_System_Collections_IEnumerator_get_Current_m2927950331 (U3CstiffAndPatStrategyU3Ec__Iterator15_t642946848 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean StrategyTutorial/<stiffAndPatStrategy>c__Iterator15::MoveNext()
extern "C"  bool U3CstiffAndPatStrategyU3Ec__Iterator15_MoveNext_m905443273 (U3CstiffAndPatStrategyU3Ec__Iterator15_t642946848 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StrategyTutorial/<stiffAndPatStrategy>c__Iterator15::Dispose()
extern "C"  void U3CstiffAndPatStrategyU3Ec__Iterator15_Dispose_m3687245256 (U3CstiffAndPatStrategyU3Ec__Iterator15_t642946848 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StrategyTutorial/<stiffAndPatStrategy>c__Iterator15::Reset()
extern "C"  void U3CstiffAndPatStrategyU3Ec__Iterator15_Reset_m2441431800 (U3CstiffAndPatStrategyU3Ec__Iterator15_t642946848 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
