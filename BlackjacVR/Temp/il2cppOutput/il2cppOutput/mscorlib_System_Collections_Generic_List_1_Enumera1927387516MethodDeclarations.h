﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<Blackjack.Card>
struct List_1_t1907714746;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1927387516.h"
#include "AssemblyU2DCSharp_Blackjack_Card539529194.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Blackjack.Card>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m1216643315_gshared (Enumerator_t1927387516 * __this, List_1_t1907714746 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m1216643315(__this, ___l0, method) ((  void (*) (Enumerator_t1927387516 *, List_1_t1907714746 *, const MethodInfo*))Enumerator__ctor_m1216643315_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Blackjack.Card>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1554286463_gshared (Enumerator_t1927387516 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1554286463(__this, method) ((  void (*) (Enumerator_t1927387516 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1554286463_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Blackjack.Card>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2936139893_gshared (Enumerator_t1927387516 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2936139893(__this, method) ((  Il2CppObject * (*) (Enumerator_t1927387516 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2936139893_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Blackjack.Card>::Dispose()
extern "C"  void Enumerator_Dispose_m1717451672_gshared (Enumerator_t1927387516 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1717451672(__this, method) ((  void (*) (Enumerator_t1927387516 *, const MethodInfo*))Enumerator_Dispose_m1717451672_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Blackjack.Card>::VerifyState()
extern "C"  void Enumerator_VerifyState_m3092934353_gshared (Enumerator_t1927387516 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m3092934353(__this, method) ((  void (*) (Enumerator_t1927387516 *, const MethodInfo*))Enumerator_VerifyState_m3092934353_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Blackjack.Card>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m4059042136_gshared (Enumerator_t1927387516 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m4059042136(__this, method) ((  bool (*) (Enumerator_t1927387516 *, const MethodInfo*))Enumerator_MoveNext_m4059042136_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Blackjack.Card>::get_Current()
extern "C"  Card_t539529194  Enumerator_get_Current_m3557413636_gshared (Enumerator_t1927387516 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3557413636(__this, method) ((  Card_t539529194  (*) (Enumerator_t1927387516 *, const MethodInfo*))Enumerator_get_Current_m3557413636_gshared)(__this, method)
