﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen4064996374MethodDeclarations.h"

// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Transform>::.ctor(System.Object,System.IntPtr)
#define UnityAction_1__ctor_m900524391(__this, ___object0, ___method1, method) ((  void (*) (UnityAction_1_t1553302789 *, Il2CppObject *, IntPtr_t, const MethodInfo*))UnityAction_1__ctor_m2698834494_gshared)(__this, ___object0, ___method1, method)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Transform>::Invoke(T0)
#define UnityAction_1_Invoke_m1099404197(__this, ___arg00, method) ((  void (*) (UnityAction_1_t1553302789 *, Transform_t1659122786 *, const MethodInfo*))UnityAction_1_Invoke_m774762876_gshared)(__this, ___arg00, method)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.Transform>::BeginInvoke(T0,System.AsyncCallback,System.Object)
#define UnityAction_1_BeginInvoke_m396131024(__this, ___arg00, ___callback1, ___object2, method) ((  Il2CppObject * (*) (UnityAction_1_t1553302789 *, Transform_t1659122786 *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))UnityAction_1_BeginInvoke_m1225830823_gshared)(__this, ___arg00, ___callback1, ___object2, method)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Transform>::EndInvoke(System.IAsyncResult)
#define UnityAction_1_EndInvoke_m3091408631(__this, ___result0, method) ((  void (*) (UnityAction_1_t1553302789 *, Il2CppObject *, const MethodInfo*))UnityAction_1_EndInvoke_m4220465998_gshared)(__this, ___result0, method)
