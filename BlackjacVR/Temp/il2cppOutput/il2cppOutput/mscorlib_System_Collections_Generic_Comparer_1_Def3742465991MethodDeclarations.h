﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<Blackjack.Decision>
struct DefaultComparer_t3742465991;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Blackjack_Decision939252918.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<Blackjack.Decision>::.ctor()
extern "C"  void DefaultComparer__ctor_m4008501042_gshared (DefaultComparer_t3742465991 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m4008501042(__this, method) ((  void (*) (DefaultComparer_t3742465991 *, const MethodInfo*))DefaultComparer__ctor_m4008501042_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<Blackjack.Decision>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m2200368005_gshared (DefaultComparer_t3742465991 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Compare_m2200368005(__this, ___x0, ___y1, method) ((  int32_t (*) (DefaultComparer_t3742465991 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Compare_m2200368005_gshared)(__this, ___x0, ___y1, method)
