﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BlackjackGame/<showDealerCards>c__IteratorA
struct U3CshowDealerCardsU3Ec__IteratorA_t1840808414;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void BlackjackGame/<showDealerCards>c__IteratorA::.ctor()
extern "C"  void U3CshowDealerCardsU3Ec__IteratorA__ctor_m2841547597 (U3CshowDealerCardsU3Ec__IteratorA_t1840808414 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object BlackjackGame/<showDealerCards>c__IteratorA::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CshowDealerCardsU3Ec__IteratorA_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3847720357 (U3CshowDealerCardsU3Ec__IteratorA_t1840808414 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object BlackjackGame/<showDealerCards>c__IteratorA::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CshowDealerCardsU3Ec__IteratorA_System_Collections_IEnumerator_get_Current_m3235609401 (U3CshowDealerCardsU3Ec__IteratorA_t1840808414 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BlackjackGame/<showDealerCards>c__IteratorA::MoveNext()
extern "C"  bool U3CshowDealerCardsU3Ec__IteratorA_MoveNext_m2117392775 (U3CshowDealerCardsU3Ec__IteratorA_t1840808414 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BlackjackGame/<showDealerCards>c__IteratorA::Dispose()
extern "C"  void U3CshowDealerCardsU3Ec__IteratorA_Dispose_m3321290826 (U3CshowDealerCardsU3Ec__IteratorA_t1840808414 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BlackjackGame/<showDealerCards>c__IteratorA::Reset()
extern "C"  void U3CshowDealerCardsU3Ec__IteratorA_Reset_m487980538 (U3CshowDealerCardsU3Ec__IteratorA_t1840808414 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
