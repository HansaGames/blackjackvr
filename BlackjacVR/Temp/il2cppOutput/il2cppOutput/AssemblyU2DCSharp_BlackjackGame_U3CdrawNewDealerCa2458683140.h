﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Blackjack.Card[]
struct CardU5BU5D_t1346973999;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Object
struct Il2CppObject;
// BlackjackGame
struct BlackjackGame_t1439740240;

#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Blackjack_Card539529194.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BlackjackGame/<drawNewDealerCards>c__IteratorB
struct  U3CdrawNewDealerCardsU3Ec__IteratorB_t2458683140  : public Il2CppObject
{
public:
	// System.Int32 BlackjackGame/<drawNewDealerCards>c__IteratorB::<i>__0
	int32_t ___U3CiU3E__0_0;
	// Blackjack.Card[] BlackjackGame/<drawNewDealerCards>c__IteratorB::cards
	CardU5BU5D_t1346973999* ___cards_1;
	// Blackjack.Card BlackjackGame/<drawNewDealerCards>c__IteratorB::<card>__1
	Card_t539529194  ___U3CcardU3E__1_2;
	// System.Boolean BlackjackGame/<drawNewDealerCards>c__IteratorB::<drawWithDelay>__2
	bool ___U3CdrawWithDelayU3E__2_3;
	// UnityEngine.GameObject BlackjackGame/<drawNewDealerCards>c__IteratorB::<cardGO>__3
	GameObject_t3674682005 * ___U3CcardGOU3E__3_4;
	// System.Single BlackjackGame/<drawNewDealerCards>c__IteratorB::<deltaX>__4
	float ___U3CdeltaXU3E__4_5;
	// System.Single BlackjackGame/<drawNewDealerCards>c__IteratorB::<deltaY>__5
	float ___U3CdeltaYU3E__5_6;
	// System.Single BlackjackGame/<drawNewDealerCards>c__IteratorB::<deltaZ>__6
	float ___U3CdeltaZU3E__6_7;
	// System.Boolean BlackjackGame/<drawNewDealerCards>c__IteratorB::showResult
	bool ___showResult_8;
	// System.Int32 BlackjackGame/<drawNewDealerCards>c__IteratorB::$PC
	int32_t ___U24PC_9;
	// System.Object BlackjackGame/<drawNewDealerCards>c__IteratorB::$current
	Il2CppObject * ___U24current_10;
	// Blackjack.Card[] BlackjackGame/<drawNewDealerCards>c__IteratorB::<$>cards
	CardU5BU5D_t1346973999* ___U3CU24U3Ecards_11;
	// System.Boolean BlackjackGame/<drawNewDealerCards>c__IteratorB::<$>showResult
	bool ___U3CU24U3EshowResult_12;
	// BlackjackGame BlackjackGame/<drawNewDealerCards>c__IteratorB::<>f__this
	BlackjackGame_t1439740240 * ___U3CU3Ef__this_13;

public:
	inline static int32_t get_offset_of_U3CiU3E__0_0() { return static_cast<int32_t>(offsetof(U3CdrawNewDealerCardsU3Ec__IteratorB_t2458683140, ___U3CiU3E__0_0)); }
	inline int32_t get_U3CiU3E__0_0() const { return ___U3CiU3E__0_0; }
	inline int32_t* get_address_of_U3CiU3E__0_0() { return &___U3CiU3E__0_0; }
	inline void set_U3CiU3E__0_0(int32_t value)
	{
		___U3CiU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_cards_1() { return static_cast<int32_t>(offsetof(U3CdrawNewDealerCardsU3Ec__IteratorB_t2458683140, ___cards_1)); }
	inline CardU5BU5D_t1346973999* get_cards_1() const { return ___cards_1; }
	inline CardU5BU5D_t1346973999** get_address_of_cards_1() { return &___cards_1; }
	inline void set_cards_1(CardU5BU5D_t1346973999* value)
	{
		___cards_1 = value;
		Il2CppCodeGenWriteBarrier(&___cards_1, value);
	}

	inline static int32_t get_offset_of_U3CcardU3E__1_2() { return static_cast<int32_t>(offsetof(U3CdrawNewDealerCardsU3Ec__IteratorB_t2458683140, ___U3CcardU3E__1_2)); }
	inline Card_t539529194  get_U3CcardU3E__1_2() const { return ___U3CcardU3E__1_2; }
	inline Card_t539529194 * get_address_of_U3CcardU3E__1_2() { return &___U3CcardU3E__1_2; }
	inline void set_U3CcardU3E__1_2(Card_t539529194  value)
	{
		___U3CcardU3E__1_2 = value;
	}

	inline static int32_t get_offset_of_U3CdrawWithDelayU3E__2_3() { return static_cast<int32_t>(offsetof(U3CdrawNewDealerCardsU3Ec__IteratorB_t2458683140, ___U3CdrawWithDelayU3E__2_3)); }
	inline bool get_U3CdrawWithDelayU3E__2_3() const { return ___U3CdrawWithDelayU3E__2_3; }
	inline bool* get_address_of_U3CdrawWithDelayU3E__2_3() { return &___U3CdrawWithDelayU3E__2_3; }
	inline void set_U3CdrawWithDelayU3E__2_3(bool value)
	{
		___U3CdrawWithDelayU3E__2_3 = value;
	}

	inline static int32_t get_offset_of_U3CcardGOU3E__3_4() { return static_cast<int32_t>(offsetof(U3CdrawNewDealerCardsU3Ec__IteratorB_t2458683140, ___U3CcardGOU3E__3_4)); }
	inline GameObject_t3674682005 * get_U3CcardGOU3E__3_4() const { return ___U3CcardGOU3E__3_4; }
	inline GameObject_t3674682005 ** get_address_of_U3CcardGOU3E__3_4() { return &___U3CcardGOU3E__3_4; }
	inline void set_U3CcardGOU3E__3_4(GameObject_t3674682005 * value)
	{
		___U3CcardGOU3E__3_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcardGOU3E__3_4, value);
	}

	inline static int32_t get_offset_of_U3CdeltaXU3E__4_5() { return static_cast<int32_t>(offsetof(U3CdrawNewDealerCardsU3Ec__IteratorB_t2458683140, ___U3CdeltaXU3E__4_5)); }
	inline float get_U3CdeltaXU3E__4_5() const { return ___U3CdeltaXU3E__4_5; }
	inline float* get_address_of_U3CdeltaXU3E__4_5() { return &___U3CdeltaXU3E__4_5; }
	inline void set_U3CdeltaXU3E__4_5(float value)
	{
		___U3CdeltaXU3E__4_5 = value;
	}

	inline static int32_t get_offset_of_U3CdeltaYU3E__5_6() { return static_cast<int32_t>(offsetof(U3CdrawNewDealerCardsU3Ec__IteratorB_t2458683140, ___U3CdeltaYU3E__5_6)); }
	inline float get_U3CdeltaYU3E__5_6() const { return ___U3CdeltaYU3E__5_6; }
	inline float* get_address_of_U3CdeltaYU3E__5_6() { return &___U3CdeltaYU3E__5_6; }
	inline void set_U3CdeltaYU3E__5_6(float value)
	{
		___U3CdeltaYU3E__5_6 = value;
	}

	inline static int32_t get_offset_of_U3CdeltaZU3E__6_7() { return static_cast<int32_t>(offsetof(U3CdrawNewDealerCardsU3Ec__IteratorB_t2458683140, ___U3CdeltaZU3E__6_7)); }
	inline float get_U3CdeltaZU3E__6_7() const { return ___U3CdeltaZU3E__6_7; }
	inline float* get_address_of_U3CdeltaZU3E__6_7() { return &___U3CdeltaZU3E__6_7; }
	inline void set_U3CdeltaZU3E__6_7(float value)
	{
		___U3CdeltaZU3E__6_7 = value;
	}

	inline static int32_t get_offset_of_showResult_8() { return static_cast<int32_t>(offsetof(U3CdrawNewDealerCardsU3Ec__IteratorB_t2458683140, ___showResult_8)); }
	inline bool get_showResult_8() const { return ___showResult_8; }
	inline bool* get_address_of_showResult_8() { return &___showResult_8; }
	inline void set_showResult_8(bool value)
	{
		___showResult_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CdrawNewDealerCardsU3Ec__IteratorB_t2458683140, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}

	inline static int32_t get_offset_of_U24current_10() { return static_cast<int32_t>(offsetof(U3CdrawNewDealerCardsU3Ec__IteratorB_t2458683140, ___U24current_10)); }
	inline Il2CppObject * get_U24current_10() const { return ___U24current_10; }
	inline Il2CppObject ** get_address_of_U24current_10() { return &___U24current_10; }
	inline void set_U24current_10(Il2CppObject * value)
	{
		___U24current_10 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_10, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Ecards_11() { return static_cast<int32_t>(offsetof(U3CdrawNewDealerCardsU3Ec__IteratorB_t2458683140, ___U3CU24U3Ecards_11)); }
	inline CardU5BU5D_t1346973999* get_U3CU24U3Ecards_11() const { return ___U3CU24U3Ecards_11; }
	inline CardU5BU5D_t1346973999** get_address_of_U3CU24U3Ecards_11() { return &___U3CU24U3Ecards_11; }
	inline void set_U3CU24U3Ecards_11(CardU5BU5D_t1346973999* value)
	{
		___U3CU24U3Ecards_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Ecards_11, value);
	}

	inline static int32_t get_offset_of_U3CU24U3EshowResult_12() { return static_cast<int32_t>(offsetof(U3CdrawNewDealerCardsU3Ec__IteratorB_t2458683140, ___U3CU24U3EshowResult_12)); }
	inline bool get_U3CU24U3EshowResult_12() const { return ___U3CU24U3EshowResult_12; }
	inline bool* get_address_of_U3CU24U3EshowResult_12() { return &___U3CU24U3EshowResult_12; }
	inline void set_U3CU24U3EshowResult_12(bool value)
	{
		___U3CU24U3EshowResult_12 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_13() { return static_cast<int32_t>(offsetof(U3CdrawNewDealerCardsU3Ec__IteratorB_t2458683140, ___U3CU3Ef__this_13)); }
	inline BlackjackGame_t1439740240 * get_U3CU3Ef__this_13() const { return ___U3CU3Ef__this_13; }
	inline BlackjackGame_t1439740240 ** get_address_of_U3CU3Ef__this_13() { return &___U3CU3Ef__this_13; }
	inline void set_U3CU3Ef__this_13(BlackjackGame_t1439740240 * value)
	{
		___U3CU3Ef__this_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
