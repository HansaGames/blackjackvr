﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Blackjack.GameResult
struct GameResult_t3329670025;
struct GameResult_t3329670025_marshaled_pinvoke;
struct GameResult_t3329670025_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Blackjack_GameResult3329670025.h"
#include "AssemblyU2DCSharp_Blackjack_User540082341.h"
#include "AssemblyU2DCSharp_Blackjack_WinType832601200.h"

// System.Void Blackjack.GameResult::.ctor(Blackjack.User,Blackjack.WinType,System.Int32,System.Int32,System.Double)
extern "C"  void GameResult__ctor_m1358122211 (GameResult_t3329670025 * __this, int32_t ___winner0, int32_t ___type1, int32_t ___delaerPoints2, int32_t ___playerPoints3, double ___winAmount4, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct GameResult_t3329670025;
struct GameResult_t3329670025_marshaled_pinvoke;

extern "C" void GameResult_t3329670025_marshal_pinvoke(const GameResult_t3329670025& unmarshaled, GameResult_t3329670025_marshaled_pinvoke& marshaled);
extern "C" void GameResult_t3329670025_marshal_pinvoke_back(const GameResult_t3329670025_marshaled_pinvoke& marshaled, GameResult_t3329670025& unmarshaled);
extern "C" void GameResult_t3329670025_marshal_pinvoke_cleanup(GameResult_t3329670025_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct GameResult_t3329670025;
struct GameResult_t3329670025_marshaled_com;

extern "C" void GameResult_t3329670025_marshal_com(const GameResult_t3329670025& unmarshaled, GameResult_t3329670025_marshaled_com& marshaled);
extern "C" void GameResult_t3329670025_marshal_com_back(const GameResult_t3329670025_marshaled_com& marshaled, GameResult_t3329670025& unmarshaled);
extern "C" void GameResult_t3329670025_marshal_com_cleanup(GameResult_t3329670025_marshaled_com& marshaled);
