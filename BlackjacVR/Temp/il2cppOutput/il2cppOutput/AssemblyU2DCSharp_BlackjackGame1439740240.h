﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// Blackjack.Game
struct Game_t539648204;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BlackjackGame
struct  BlackjackGame_t1439740240  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GameObject BlackjackGame::dealerScore
	GameObject_t3674682005 * ___dealerScore_2;
	// UnityEngine.GameObject BlackjackGame::playerScore
	GameObject_t3674682005 * ___playerScore_3;
	// UnityEngine.GameObject BlackjackGame::playerBid
	GameObject_t3674682005 * ___playerBid_4;
	// UnityEngine.GameObject BlackjackGame::dealerCardsGO
	GameObject_t3674682005 * ___dealerCardsGO_5;
	// UnityEngine.GameObject BlackjackGame::playerCardsGO
	GameObject_t3674682005 * ___playerCardsGO_6;
	// Blackjack.Game BlackjackGame::game
	Game_t539648204 * ___game_7;

public:
	inline static int32_t get_offset_of_dealerScore_2() { return static_cast<int32_t>(offsetof(BlackjackGame_t1439740240, ___dealerScore_2)); }
	inline GameObject_t3674682005 * get_dealerScore_2() const { return ___dealerScore_2; }
	inline GameObject_t3674682005 ** get_address_of_dealerScore_2() { return &___dealerScore_2; }
	inline void set_dealerScore_2(GameObject_t3674682005 * value)
	{
		___dealerScore_2 = value;
		Il2CppCodeGenWriteBarrier(&___dealerScore_2, value);
	}

	inline static int32_t get_offset_of_playerScore_3() { return static_cast<int32_t>(offsetof(BlackjackGame_t1439740240, ___playerScore_3)); }
	inline GameObject_t3674682005 * get_playerScore_3() const { return ___playerScore_3; }
	inline GameObject_t3674682005 ** get_address_of_playerScore_3() { return &___playerScore_3; }
	inline void set_playerScore_3(GameObject_t3674682005 * value)
	{
		___playerScore_3 = value;
		Il2CppCodeGenWriteBarrier(&___playerScore_3, value);
	}

	inline static int32_t get_offset_of_playerBid_4() { return static_cast<int32_t>(offsetof(BlackjackGame_t1439740240, ___playerBid_4)); }
	inline GameObject_t3674682005 * get_playerBid_4() const { return ___playerBid_4; }
	inline GameObject_t3674682005 ** get_address_of_playerBid_4() { return &___playerBid_4; }
	inline void set_playerBid_4(GameObject_t3674682005 * value)
	{
		___playerBid_4 = value;
		Il2CppCodeGenWriteBarrier(&___playerBid_4, value);
	}

	inline static int32_t get_offset_of_dealerCardsGO_5() { return static_cast<int32_t>(offsetof(BlackjackGame_t1439740240, ___dealerCardsGO_5)); }
	inline GameObject_t3674682005 * get_dealerCardsGO_5() const { return ___dealerCardsGO_5; }
	inline GameObject_t3674682005 ** get_address_of_dealerCardsGO_5() { return &___dealerCardsGO_5; }
	inline void set_dealerCardsGO_5(GameObject_t3674682005 * value)
	{
		___dealerCardsGO_5 = value;
		Il2CppCodeGenWriteBarrier(&___dealerCardsGO_5, value);
	}

	inline static int32_t get_offset_of_playerCardsGO_6() { return static_cast<int32_t>(offsetof(BlackjackGame_t1439740240, ___playerCardsGO_6)); }
	inline GameObject_t3674682005 * get_playerCardsGO_6() const { return ___playerCardsGO_6; }
	inline GameObject_t3674682005 ** get_address_of_playerCardsGO_6() { return &___playerCardsGO_6; }
	inline void set_playerCardsGO_6(GameObject_t3674682005 * value)
	{
		___playerCardsGO_6 = value;
		Il2CppCodeGenWriteBarrier(&___playerCardsGO_6, value);
	}

	inline static int32_t get_offset_of_game_7() { return static_cast<int32_t>(offsetof(BlackjackGame_t1439740240, ___game_7)); }
	inline Game_t539648204 * get_game_7() const { return ___game_7; }
	inline Game_t539648204 ** get_address_of_game_7() { return &___game_7; }
	inline void set_game_7(Game_t539648204 * value)
	{
		___game_7 = value;
		Il2CppCodeGenWriteBarrier(&___game_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
