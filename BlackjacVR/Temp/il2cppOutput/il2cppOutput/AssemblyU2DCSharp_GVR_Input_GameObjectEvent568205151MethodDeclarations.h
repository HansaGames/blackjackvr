﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GVR.Input.GameObjectEvent
struct GameObjectEvent_t568205151;

#include "codegen/il2cpp-codegen.h"

// System.Void GVR.Input.GameObjectEvent::.ctor()
extern "C"  void GameObjectEvent__ctor_m4110101553 (GameObjectEvent_t568205151 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
