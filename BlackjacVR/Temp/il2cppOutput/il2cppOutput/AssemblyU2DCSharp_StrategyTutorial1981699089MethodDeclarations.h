﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StrategyTutorial
struct StrategyTutorial_t1981699089;
// UnityEngine.AudioClip
struct AudioClip_t794140988;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"

// System.Void StrategyTutorial::.ctor()
extern "C"  void StrategyTutorial__ctor_m3321222570 (StrategyTutorial_t1981699089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StrategyTutorial::Start()
extern "C"  void StrategyTutorial_Start_m2268360362 (StrategyTutorial_t1981699089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioClip StrategyTutorial::getTutorialClip()
extern "C"  AudioClip_t794140988 * StrategyTutorial_getTutorialClip_m3890161192 (StrategyTutorial_t1981699089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator StrategyTutorial::showCardCombinations()
extern "C"  Il2CppObject * StrategyTutorial_showCardCombinations_m1164131859 (StrategyTutorial_t1981699089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator StrategyTutorial::bothStiffStrategy()
extern "C"  Il2CppObject * StrategyTutorial_bothStiffStrategy_m923895578 (StrategyTutorial_t1981699089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator StrategyTutorial::stiffAndPatStrategy()
extern "C"  Il2CppObject * StrategyTutorial_stiffAndPatStrategy_m2319266695 (StrategyTutorial_t1981699089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator StrategyTutorial::patStrategy()
extern "C"  Il2CppObject * StrategyTutorial_patStrategy_m2883977750 (StrategyTutorial_t1981699089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StrategyTutorial::hideAll()
extern "C"  void StrategyTutorial_hideAll_m3298330215 (StrategyTutorial_t1981699089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StrategyTutorial::Update()
extern "C"  void StrategyTutorial_Update_m1605546659 (StrategyTutorial_t1981699089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StrategyTutorial::OnDestroy()
extern "C"  void StrategyTutorial_OnDestroy_m372295075 (StrategyTutorial_t1981699089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
