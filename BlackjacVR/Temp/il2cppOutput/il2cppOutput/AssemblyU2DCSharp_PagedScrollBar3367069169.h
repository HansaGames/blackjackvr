﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UI_UnityEngine_UI_Scrollbar2601556940.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PagedScrollBar
struct  PagedScrollBar_t3367069169  : public Scrollbar_t2601556940
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
