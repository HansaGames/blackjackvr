﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3616839166.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_Blackjack_Card539529194.h"

// System.Void System.Array/InternalEnumerator`1<Blackjack.Card>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3772209521_gshared (InternalEnumerator_1_t3616839166 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m3772209521(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3616839166 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3772209521_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<Blackjack.Card>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3624787087_gshared (InternalEnumerator_1_t3616839166 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3624787087(__this, method) ((  void (*) (InternalEnumerator_1_t3616839166 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3624787087_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Blackjack.Card>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3415056443_gshared (InternalEnumerator_1_t3616839166 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3415056443(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3616839166 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3415056443_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Blackjack.Card>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1560267912_gshared (InternalEnumerator_1_t3616839166 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1560267912(__this, method) ((  void (*) (InternalEnumerator_1_t3616839166 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1560267912_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Blackjack.Card>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3626090107_gshared (InternalEnumerator_1_t3616839166 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m3626090107(__this, method) ((  bool (*) (InternalEnumerator_1_t3616839166 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3626090107_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Blackjack.Card>::get_Current()
extern "C"  Card_t539529194  InternalEnumerator_1_get_Current_m2983445112_gshared (InternalEnumerator_1_t3616839166 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m2983445112(__this, method) ((  Card_t539529194  (*) (InternalEnumerator_1_t3616839166 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2983445112_gshared)(__this, method)
