﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<Blackjack.Card,Blackjack.Card>
struct U3CCreateSelectIteratorU3Ec__Iterator10_2_t977859401;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<Blackjack.Card>
struct IEnumerator_1_t2451394243;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Blackjack_Card539529194.h"

// System.Void System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<Blackjack.Card,Blackjack.Card>::.ctor()
extern "C"  void U3CCreateSelectIteratorU3Ec__Iterator10_2__ctor_m1913237499_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t977859401 * __this, const MethodInfo* method);
#define U3CCreateSelectIteratorU3Ec__Iterator10_2__ctor_m1913237499(__this, method) ((  void (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t977859401 *, const MethodInfo*))U3CCreateSelectIteratorU3Ec__Iterator10_2__ctor_m1913237499_gshared)(__this, method)
// TResult System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<Blackjack.Card,Blackjack.Card>::System.Collections.Generic.IEnumerator<TResult>.get_Current()
extern "C"  Card_t539529194  U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m999019592_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t977859401 * __this, const MethodInfo* method);
#define U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m999019592(__this, method) ((  Card_t539529194  (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t977859401 *, const MethodInfo*))U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m999019592_gshared)(__this, method)
// System.Object System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<Blackjack.Card,Blackjack.Card>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerator_get_Current_m3228775755_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t977859401 * __this, const MethodInfo* method);
#define U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerator_get_Current_m3228775755(__this, method) ((  Il2CppObject * (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t977859401 *, const MethodInfo*))U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerator_get_Current_m3228775755_gshared)(__this, method)
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<Blackjack.Card,Blackjack.Card>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerable_GetEnumerator_m3811846950_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t977859401 * __this, const MethodInfo* method);
#define U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerable_GetEnumerator_m3811846950(__this, method) ((  Il2CppObject * (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t977859401 *, const MethodInfo*))U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerable_GetEnumerator_m3811846950_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<Blackjack.Card,Blackjack.Card>::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m1078154021_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t977859401 * __this, const MethodInfo* method);
#define U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m1078154021(__this, method) ((  Il2CppObject* (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t977859401 *, const MethodInfo*))U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m1078154021_gshared)(__this, method)
// System.Boolean System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<Blackjack.Card,Blackjack.Card>::MoveNext()
extern "C"  bool U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m1850435129_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t977859401 * __this, const MethodInfo* method);
#define U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m1850435129(__this, method) ((  bool (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t977859401 *, const MethodInfo*))U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m1850435129_gshared)(__this, method)
// System.Void System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<Blackjack.Card,Blackjack.Card>::Dispose()
extern "C"  void U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m273516920_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t977859401 * __this, const MethodInfo* method);
#define U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m273516920(__this, method) ((  void (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t977859401 *, const MethodInfo*))U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m273516920_gshared)(__this, method)
// System.Void System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<Blackjack.Card,Blackjack.Card>::Reset()
extern "C"  void U3CCreateSelectIteratorU3Ec__Iterator10_2_Reset_m3854637736_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t977859401 * __this, const MethodInfo* method);
#define U3CCreateSelectIteratorU3Ec__Iterator10_2_Reset_m3854637736(__this, method) ((  void (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t977859401 *, const MethodInfo*))U3CCreateSelectIteratorU3Ec__Iterator10_2_Reset_m3854637736_gshared)(__this, method)
