﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<Blackjack.Decision>
struct List_1_t2307438470;
// System.Collections.Generic.IEnumerable`1<Blackjack.Decision>
struct IEnumerable_1_t4240165875;
// Blackjack.Decision[]
struct DecisionU5BU5D_t3084712883;
// System.Collections.Generic.IEnumerator`1<Blackjack.Decision>
struct IEnumerator_1_t2851117967;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<Blackjack.Decision>
struct ICollection_1_t1833842905;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Blackjack.Decision>
struct ReadOnlyCollection_1_t2496330454;
// System.Predicate`1<Blackjack.Decision>
struct Predicate_1_t550309801;
// System.Comparison`1<Blackjack.Decision>
struct Comparison_1_t3950581401;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Blackjack_Decision939252918.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2327111240.h"

// System.Void System.Collections.Generic.List`1<Blackjack.Decision>::.ctor()
extern "C"  void List_1__ctor_m722248919_gshared (List_1_t2307438470 * __this, const MethodInfo* method);
#define List_1__ctor_m722248919(__this, method) ((  void (*) (List_1_t2307438470 *, const MethodInfo*))List_1__ctor_m722248919_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Blackjack.Decision>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m3813925343_gshared (List_1_t2307438470 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m3813925343(__this, ___collection0, method) ((  void (*) (List_1_t2307438470 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m3813925343_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Blackjack.Decision>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m2193681393_gshared (List_1_t2307438470 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m2193681393(__this, ___capacity0, method) ((  void (*) (List_1_t2307438470 *, int32_t, const MethodInfo*))List_1__ctor_m2193681393_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<Blackjack.Decision>::.ctor(T[],System.Int32)
extern "C"  void List_1__ctor_m2553745403_gshared (List_1_t2307438470 * __this, DecisionU5BU5D_t3084712883* ___data0, int32_t ___size1, const MethodInfo* method);
#define List_1__ctor_m2553745403(__this, ___data0, ___size1, method) ((  void (*) (List_1_t2307438470 *, DecisionU5BU5D_t3084712883*, int32_t, const MethodInfo*))List_1__ctor_m2553745403_gshared)(__this, ___data0, ___size1, method)
// System.Void System.Collections.Generic.List`1<Blackjack.Decision>::.cctor()
extern "C"  void List_1__cctor_m586184973_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m586184973(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m586184973_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Blackjack.Decision>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2751260586_gshared (List_1_t2307438470 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2751260586(__this, method) ((  Il2CppObject* (*) (List_1_t2307438470 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2751260586_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Blackjack.Decision>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m949103268_gshared (List_1_t2307438470 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m949103268(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t2307438470 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m949103268_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Blackjack.Decision>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m610327859_gshared (List_1_t2307438470 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m610327859(__this, method) ((  Il2CppObject * (*) (List_1_t2307438470 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m610327859_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Blackjack.Decision>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m697487466_gshared (List_1_t2307438470 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m697487466(__this, ___item0, method) ((  int32_t (*) (List_1_t2307438470 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m697487466_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Blackjack.Decision>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m3675104918_gshared (List_1_t2307438470 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m3675104918(__this, ___item0, method) ((  bool (*) (List_1_t2307438470 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m3675104918_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Blackjack.Decision>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m952156610_gshared (List_1_t2307438470 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m952156610(__this, ___item0, method) ((  int32_t (*) (List_1_t2307438470 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m952156610_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Blackjack.Decision>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m341164853_gshared (List_1_t2307438470 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m341164853(__this, ___index0, ___item1, method) ((  void (*) (List_1_t2307438470 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m341164853_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Blackjack.Decision>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m926405587_gshared (List_1_t2307438470 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m926405587(__this, ___item0, method) ((  void (*) (List_1_t2307438470 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m926405587_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Blackjack.Decision>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1482871575_gshared (List_1_t2307438470 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1482871575(__this, method) ((  bool (*) (List_1_t2307438470 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1482871575_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Blackjack.Decision>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m2034245894_gshared (List_1_t2307438470 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m2034245894(__this, method) ((  bool (*) (List_1_t2307438470 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m2034245894_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Blackjack.Decision>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m2238927672_gshared (List_1_t2307438470 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m2238927672(__this, method) ((  Il2CppObject * (*) (List_1_t2307438470 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m2238927672_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Blackjack.Decision>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m2208530053_gshared (List_1_t2307438470 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m2208530053(__this, method) ((  bool (*) (List_1_t2307438470 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m2208530053_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Blackjack.Decision>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m2464914388_gshared (List_1_t2307438470 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m2464914388(__this, method) ((  bool (*) (List_1_t2307438470 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m2464914388_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Blackjack.Decision>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m896489151_gshared (List_1_t2307438470 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m896489151(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t2307438470 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m896489151_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Blackjack.Decision>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m2055861708_gshared (List_1_t2307438470 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m2055861708(__this, ___index0, ___value1, method) ((  void (*) (List_1_t2307438470 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m2055861708_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<Blackjack.Decision>::Add(T)
extern "C"  void List_1_Add_m416609735_gshared (List_1_t2307438470 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Add_m416609735(__this, ___item0, method) ((  void (*) (List_1_t2307438470 *, int32_t, const MethodInfo*))List_1_Add_m416609735_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Blackjack.Decision>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m2680950426_gshared (List_1_t2307438470 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m2680950426(__this, ___newCount0, method) ((  void (*) (List_1_t2307438470 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m2680950426_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<Blackjack.Decision>::CheckRange(System.Int32,System.Int32)
extern "C"  void List_1_CheckRange_m4043755789_gshared (List_1_t2307438470 * __this, int32_t ___idx0, int32_t ___count1, const MethodInfo* method);
#define List_1_CheckRange_m4043755789(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t2307438470 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m4043755789_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<Blackjack.Decision>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m483098008_gshared (List_1_t2307438470 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m483098008(__this, ___collection0, method) ((  void (*) (List_1_t2307438470 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m483098008_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Blackjack.Decision>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m4039037528_gshared (List_1_t2307438470 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m4039037528(__this, ___enumerable0, method) ((  void (*) (List_1_t2307438470 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m4039037528_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<Blackjack.Decision>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m1340998975_gshared (List_1_t2307438470 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m1340998975(__this, ___collection0, method) ((  void (*) (List_1_t2307438470 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m1340998975_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Blackjack.Decision>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t2496330454 * List_1_AsReadOnly_m3358850406_gshared (List_1_t2307438470 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m3358850406(__this, method) ((  ReadOnlyCollection_1_t2496330454 * (*) (List_1_t2307438470 *, const MethodInfo*))List_1_AsReadOnly_m3358850406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Blackjack.Decision>::Clear()
extern "C"  void List_1_Clear_m2428297931_gshared (List_1_t2307438470 * __this, const MethodInfo* method);
#define List_1_Clear_m2428297931(__this, method) ((  void (*) (List_1_t2307438470 *, const MethodInfo*))List_1_Clear_m2428297931_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Blackjack.Decision>::Contains(T)
extern "C"  bool List_1_Contains_m3973642429_gshared (List_1_t2307438470 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Contains_m3973642429(__this, ___item0, method) ((  bool (*) (List_1_t2307438470 *, int32_t, const MethodInfo*))List_1_Contains_m3973642429_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Blackjack.Decision>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m2221495311_gshared (List_1_t2307438470 * __this, DecisionU5BU5D_t3084712883* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m2221495311(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t2307438470 *, DecisionU5BU5D_t3084712883*, int32_t, const MethodInfo*))List_1_CopyTo_m2221495311_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<Blackjack.Decision>::Find(System.Predicate`1<T>)
extern "C"  int32_t List_1_Find_m3346180823_gshared (List_1_t2307438470 * __this, Predicate_1_t550309801 * ___match0, const MethodInfo* method);
#define List_1_Find_m3346180823(__this, ___match0, method) ((  int32_t (*) (List_1_t2307438470 *, Predicate_1_t550309801 *, const MethodInfo*))List_1_Find_m3346180823_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Blackjack.Decision>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m962915252_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t550309801 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m962915252(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t550309801 *, const MethodInfo*))List_1_CheckMatch_m962915252_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<Blackjack.Decision>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m1730570385_gshared (List_1_t2307438470 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t550309801 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m1730570385(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t2307438470 *, int32_t, int32_t, Predicate_1_t550309801 *, const MethodInfo*))List_1_GetIndex_m1730570385_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Blackjack.Decision>::GetEnumerator()
extern "C"  Enumerator_t2327111240  List_1_GetEnumerator_m1063650724_gshared (List_1_t2307438470 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m1063650724(__this, method) ((  Enumerator_t2327111240  (*) (List_1_t2307438470 *, const MethodInfo*))List_1_GetEnumerator_m1063650724_gshared)(__this, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<Blackjack.Decision>::GetRange(System.Int32,System.Int32)
extern "C"  List_1_t2307438470 * List_1_GetRange_m2535211262_gshared (List_1_t2307438470 * __this, int32_t ___index0, int32_t ___count1, const MethodInfo* method);
#define List_1_GetRange_m2535211262(__this, ___index0, ___count1, method) ((  List_1_t2307438470 * (*) (List_1_t2307438470 *, int32_t, int32_t, const MethodInfo*))List_1_GetRange_m2535211262_gshared)(__this, ___index0, ___count1, method)
// System.Int32 System.Collections.Generic.List`1<Blackjack.Decision>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m3007198491_gshared (List_1_t2307438470 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_IndexOf_m3007198491(__this, ___item0, method) ((  int32_t (*) (List_1_t2307438470 *, int32_t, const MethodInfo*))List_1_IndexOf_m3007198491_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Blackjack.Decision>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m2620369766_gshared (List_1_t2307438470 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m2620369766(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t2307438470 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m2620369766_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<Blackjack.Decision>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m1967862495_gshared (List_1_t2307438470 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m1967862495(__this, ___index0, method) ((  void (*) (List_1_t2307438470 *, int32_t, const MethodInfo*))List_1_CheckIndex_m1967862495_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Blackjack.Decision>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m1159406150_gshared (List_1_t2307438470 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define List_1_Insert_m1159406150(__this, ___index0, ___item1, method) ((  void (*) (List_1_t2307438470 *, int32_t, int32_t, const MethodInfo*))List_1_Insert_m1159406150_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Blackjack.Decision>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m927386171_gshared (List_1_t2307438470 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m927386171(__this, ___collection0, method) ((  void (*) (List_1_t2307438470 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m927386171_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<Blackjack.Decision>::Remove(T)
extern "C"  bool List_1_Remove_m4034839544_gshared (List_1_t2307438470 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Remove_m4034839544(__this, ___item0, method) ((  bool (*) (List_1_t2307438470 *, int32_t, const MethodInfo*))List_1_Remove_m4034839544_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Blackjack.Decision>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m3855509150_gshared (List_1_t2307438470 * __this, Predicate_1_t550309801 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m3855509150(__this, ___match0, method) ((  int32_t (*) (List_1_t2307438470 *, Predicate_1_t550309801 *, const MethodInfo*))List_1_RemoveAll_m3855509150_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Blackjack.Decision>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m3328226316_gshared (List_1_t2307438470 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m3328226316(__this, ___index0, method) ((  void (*) (List_1_t2307438470 *, int32_t, const MethodInfo*))List_1_RemoveAt_m3328226316_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Blackjack.Decision>::Reverse()
extern "C"  void List_1_Reverse_m3089756896_gshared (List_1_t2307438470 * __this, const MethodInfo* method);
#define List_1_Reverse_m3089756896(__this, method) ((  void (*) (List_1_t2307438470 *, const MethodInfo*))List_1_Reverse_m3089756896_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Blackjack.Decision>::Sort()
extern "C"  void List_1_Sort_m539572418_gshared (List_1_t2307438470 * __this, const MethodInfo* method);
#define List_1_Sort_m539572418(__this, method) ((  void (*) (List_1_t2307438470 *, const MethodInfo*))List_1_Sort_m539572418_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Blackjack.Decision>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m1745577877_gshared (List_1_t2307438470 * __this, Comparison_1_t3950581401 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m1745577877(__this, ___comparison0, method) ((  void (*) (List_1_t2307438470 *, Comparison_1_t3950581401 *, const MethodInfo*))List_1_Sort_m1745577877_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<Blackjack.Decision>::ToArray()
extern "C"  DecisionU5BU5D_t3084712883* List_1_ToArray_m3083317561_gshared (List_1_t2307438470 * __this, const MethodInfo* method);
#define List_1_ToArray_m3083317561(__this, method) ((  DecisionU5BU5D_t3084712883* (*) (List_1_t2307438470 *, const MethodInfo*))List_1_ToArray_m3083317561_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Blackjack.Decision>::TrimExcess()
extern "C"  void List_1_TrimExcess_m4178001627_gshared (List_1_t2307438470 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m4178001627(__this, method) ((  void (*) (List_1_t2307438470 *, const MethodInfo*))List_1_TrimExcess_m4178001627_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Blackjack.Decision>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m2964858827_gshared (List_1_t2307438470 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m2964858827(__this, method) ((  int32_t (*) (List_1_t2307438470 *, const MethodInfo*))List_1_get_Capacity_m2964858827_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Blackjack.Decision>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m4186106540_gshared (List_1_t2307438470 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m4186106540(__this, ___value0, method) ((  void (*) (List_1_t2307438470 *, int32_t, const MethodInfo*))List_1_set_Capacity_m4186106540_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<Blackjack.Decision>::get_Count()
extern "C"  int32_t List_1_get_Count_m2846147520_gshared (List_1_t2307438470 * __this, const MethodInfo* method);
#define List_1_get_Count_m2846147520(__this, method) ((  int32_t (*) (List_1_t2307438470 *, const MethodInfo*))List_1_get_Count_m2846147520_gshared)(__this, method)
// T System.Collections.Generic.List`1<Blackjack.Decision>::get_Item(System.Int32)
extern "C"  int32_t List_1_get_Item_m599795058_gshared (List_1_t2307438470 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m599795058(__this, ___index0, method) ((  int32_t (*) (List_1_t2307438470 *, int32_t, const MethodInfo*))List_1_get_Item_m599795058_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Blackjack.Decision>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m3602377117_gshared (List_1_t2307438470 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define List_1_set_Item_m3602377117(__this, ___index0, ___value1, method) ((  void (*) (List_1_t2307438470 *, int32_t, int32_t, const MethodInfo*))List_1_set_Item_m3602377117_gshared)(__this, ___index0, ___value1, method)
