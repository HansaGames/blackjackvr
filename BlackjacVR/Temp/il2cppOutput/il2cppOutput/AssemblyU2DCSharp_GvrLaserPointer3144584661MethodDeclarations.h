﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GvrLaserPointer
struct GvrLaserPointer_t3144584661;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Ray3134616544.h"

// System.Void GvrLaserPointer::.ctor()
extern "C"  void GvrLaserPointer__ctor_m3336763702 (GvrLaserPointer_t3144584661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GvrLaserPointer::Start()
extern "C"  void GvrLaserPointer_Start_m2283901494 (GvrLaserPointer_t3144584661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GvrLaserPointer::OnInputModuleEnabled()
extern "C"  void GvrLaserPointer_OnInputModuleEnabled_m4074790040 (GvrLaserPointer_t3144584661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GvrLaserPointer::OnInputModuleDisabled()
extern "C"  void GvrLaserPointer_OnInputModuleDisabled_m2546862759 (GvrLaserPointer_t3144584661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GvrLaserPointer::OnPointerEnter(UnityEngine.GameObject,UnityEngine.Vector3,UnityEngine.Ray,System.Boolean)
extern "C"  void GvrLaserPointer_OnPointerEnter_m799226705 (GvrLaserPointer_t3144584661 * __this, GameObject_t3674682005 * ___targetObject0, Vector3_t4282066566  ___intersectionPosition1, Ray_t3134616544  ___intersectionRay2, bool ___isInteractive3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GvrLaserPointer::OnPointerHover(UnityEngine.GameObject,UnityEngine.Vector3,UnityEngine.Ray,System.Boolean)
extern "C"  void GvrLaserPointer_OnPointerHover_m44626541 (GvrLaserPointer_t3144584661 * __this, GameObject_t3674682005 * ___targetObject0, Vector3_t4282066566  ___intersectionPosition1, Ray_t3134616544  ___intersectionRay2, bool ___isInteractive3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GvrLaserPointer::OnPointerExit(UnityEngine.GameObject)
extern "C"  void GvrLaserPointer_OnPointerExit_m3845606760 (GvrLaserPointer_t3144584661 * __this, GameObject_t3674682005 * ___targetObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GvrLaserPointer::OnPointerClickDown()
extern "C"  void GvrLaserPointer_OnPointerClickDown_m1411865626 (GvrLaserPointer_t3144584661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GvrLaserPointer::OnPointerClickUp()
extern "C"  void GvrLaserPointer_OnPointerClickUp_m136052115 (GvrLaserPointer_t3144584661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single GvrLaserPointer::GetMaxPointerDistance()
extern "C"  float GvrLaserPointer_GetMaxPointerDistance_m2315688292 (GvrLaserPointer_t3144584661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GvrLaserPointer::GetPointerRadius(System.Single&,System.Single&)
extern "C"  void GvrLaserPointer_GetPointerRadius_m1748253759 (GvrLaserPointer_t3144584661 * __this, float* ___enterRadius0, float* ___exitRadius1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
