﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RulesTutorial
struct RulesTutorial_t3317375317;
// UnityEngine.AudioClip
struct AudioClip_t794140988;

#include "codegen/il2cpp-codegen.h"

// System.Void RulesTutorial::.ctor()
extern "C"  void RulesTutorial__ctor_m4149669814 (RulesTutorial_t3317375317 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RulesTutorial::Start()
extern "C"  void RulesTutorial_Start_m3096807606 (RulesTutorial_t3317375317 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioClip RulesTutorial::getTutorialClip()
extern "C"  AudioClip_t794140988 * RulesTutorial_getTutorialClip_m739472700 (RulesTutorial_t3317375317 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RulesTutorial::OnDestroy()
extern "C"  void RulesTutorial_OnDestroy_m505280943 (RulesTutorial_t3317375317 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
