﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4016562890.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_Blackjack_Decision939252918.h"

// System.Void System.Array/InternalEnumerator`1<Blackjack.Decision>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m457359933_gshared (InternalEnumerator_1_t4016562890 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m457359933(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t4016562890 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m457359933_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<Blackjack.Decision>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1271941187_gshared (InternalEnumerator_1_t4016562890 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1271941187(__this, method) ((  void (*) (InternalEnumerator_1_t4016562890 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1271941187_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Blackjack.Decision>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1771571055_gshared (InternalEnumerator_1_t4016562890 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1771571055(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t4016562890 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1771571055_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Blackjack.Decision>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3215295572_gshared (InternalEnumerator_1_t4016562890 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m3215295572(__this, method) ((  void (*) (InternalEnumerator_1_t4016562890 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3215295572_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Blackjack.Decision>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1613541679_gshared (InternalEnumerator_1_t4016562890 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1613541679(__this, method) ((  bool (*) (InternalEnumerator_1_t4016562890 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1613541679_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Blackjack.Decision>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m1543978180_gshared (InternalEnumerator_1_t4016562890 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1543978180(__this, method) ((  int32_t (*) (InternalEnumerator_1_t4016562890 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1543978180_gshared)(__this, method)
