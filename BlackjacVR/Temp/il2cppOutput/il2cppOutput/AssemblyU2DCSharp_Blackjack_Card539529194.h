﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Int32[]
struct Int32U5BU5D_t3230847821;

#include "mscorlib_System_ValueType1744280289.h"
#include "AssemblyU2DCSharp_Blackjack_Suit540024807.h"
#include "AssemblyU2DCSharp_Blackjack_CardType365306692.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Blackjack.Card
struct  Card_t539529194 
{
public:
	// Blackjack.Suit Blackjack.Card::suit
	int32_t ___suit_0;
	// Blackjack.CardType Blackjack.Card::type
	int32_t ___type_1;
	// System.Int32[] Blackjack.Card::values
	Int32U5BU5D_t3230847821* ___values_2;

public:
	inline static int32_t get_offset_of_suit_0() { return static_cast<int32_t>(offsetof(Card_t539529194, ___suit_0)); }
	inline int32_t get_suit_0() const { return ___suit_0; }
	inline int32_t* get_address_of_suit_0() { return &___suit_0; }
	inline void set_suit_0(int32_t value)
	{
		___suit_0 = value;
	}

	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(Card_t539529194, ___type_1)); }
	inline int32_t get_type_1() const { return ___type_1; }
	inline int32_t* get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(int32_t value)
	{
		___type_1 = value;
	}

	inline static int32_t get_offset_of_values_2() { return static_cast<int32_t>(offsetof(Card_t539529194, ___values_2)); }
	inline Int32U5BU5D_t3230847821* get_values_2() const { return ___values_2; }
	inline Int32U5BU5D_t3230847821** get_address_of_values_2() { return &___values_2; }
	inline void set_values_2(Int32U5BU5D_t3230847821* value)
	{
		___values_2 = value;
		Il2CppCodeGenWriteBarrier(&___values_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: Blackjack.Card
struct Card_t539529194_marshaled_pinvoke
{
	int32_t ___suit_0;
	int32_t ___type_1;
	int32_t* ___values_2;
};
// Native definition for marshalling of: Blackjack.Card
struct Card_t539529194_marshaled_com
{
	int32_t ___suit_0;
	int32_t ___type_1;
	int32_t* ___values_2;
};
