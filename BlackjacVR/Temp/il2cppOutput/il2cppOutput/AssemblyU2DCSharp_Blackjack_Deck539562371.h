﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Blackjack.Card[]
struct CardU5BU5D_t1346973999;
// System.Random
struct Random_t4255898871;
// System.Func`2<Blackjack.Card,Blackjack.Card>
struct Func_2_t1086561661;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Blackjack.Deck
struct  Deck_t539562371  : public Il2CppObject
{
public:
	// Blackjack.Card[] Blackjack.Deck::cardDeck
	CardU5BU5D_t1346973999* ___cardDeck_0;
	// System.Random Blackjack.Deck::gen
	Random_t4255898871 * ___gen_1;

public:
	inline static int32_t get_offset_of_cardDeck_0() { return static_cast<int32_t>(offsetof(Deck_t539562371, ___cardDeck_0)); }
	inline CardU5BU5D_t1346973999* get_cardDeck_0() const { return ___cardDeck_0; }
	inline CardU5BU5D_t1346973999** get_address_of_cardDeck_0() { return &___cardDeck_0; }
	inline void set_cardDeck_0(CardU5BU5D_t1346973999* value)
	{
		___cardDeck_0 = value;
		Il2CppCodeGenWriteBarrier(&___cardDeck_0, value);
	}

	inline static int32_t get_offset_of_gen_1() { return static_cast<int32_t>(offsetof(Deck_t539562371, ___gen_1)); }
	inline Random_t4255898871 * get_gen_1() const { return ___gen_1; }
	inline Random_t4255898871 ** get_address_of_gen_1() { return &___gen_1; }
	inline void set_gen_1(Random_t4255898871 * value)
	{
		___gen_1 = value;
		Il2CppCodeGenWriteBarrier(&___gen_1, value);
	}
};

struct Deck_t539562371_StaticFields
{
public:
	// System.Func`2<Blackjack.Card,Blackjack.Card> Blackjack.Deck::<>f__am$cache2
	Func_2_t1086561661 * ___U3CU3Ef__amU24cache2_2;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_2() { return static_cast<int32_t>(offsetof(Deck_t539562371_StaticFields, ___U3CU3Ef__amU24cache2_2)); }
	inline Func_2_t1086561661 * get_U3CU3Ef__amU24cache2_2() const { return ___U3CU3Ef__amU24cache2_2; }
	inline Func_2_t1086561661 ** get_address_of_U3CU3Ef__amU24cache2_2() { return &___U3CU3Ef__amU24cache2_2; }
	inline void set_U3CU3Ef__amU24cache2_2(Func_2_t1086561661 * value)
	{
		___U3CU3Ef__amU24cache2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
