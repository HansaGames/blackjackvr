﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OptionsTutorial
struct OptionsTutorial_t4157558780;
// UnityEngine.AudioClip
struct AudioClip_t794140988;

#include "codegen/il2cpp-codegen.h"

// System.Void OptionsTutorial::.ctor()
extern "C"  void OptionsTutorial__ctor_m1433187055 (OptionsTutorial_t4157558780 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OptionsTutorial::Start()
extern "C"  void OptionsTutorial_Start_m380324847 (OptionsTutorial_t4157558780 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OptionsTutorial::Update()
extern "C"  void OptionsTutorial_Update_m3205987838 (OptionsTutorial_t4157558780 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioClip OptionsTutorial::getTutorialClip()
extern "C"  AudioClip_t794140988 * OptionsTutorial_getTutorialClip_m734176309 (OptionsTutorial_t4157558780 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OptionsTutorial::OnDestroy()
extern "C"  void OptionsTutorial_OnDestroy_m683505768 (OptionsTutorial_t4157558780 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
