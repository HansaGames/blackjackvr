﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// UnityEngine.Material
struct Material_t3870600107;
// System.Object
struct Il2CppObject;
// BlackjackGame
struct BlackjackGame_t1439740240;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BlackjackGame/<playerPlayDrawGame>c__IteratorE
struct  U3CplayerPlayDrawGameU3Ec__IteratorE_t223069034  : public Il2CppObject
{
public:
	// UnityEngine.GameObject BlackjackGame/<playerPlayDrawGame>c__IteratorE::<playerBackground>__0
	GameObject_t3674682005 * ___U3CplayerBackgroundU3E__0_0;
	// UnityEngine.Material BlackjackGame/<playerPlayDrawGame>c__IteratorE::<newMat>__1
	Material_t3870600107 * ___U3CnewMatU3E__1_1;
	// System.Int32 BlackjackGame/<playerPlayDrawGame>c__IteratorE::$PC
	int32_t ___U24PC_2;
	// System.Object BlackjackGame/<playerPlayDrawGame>c__IteratorE::$current
	Il2CppObject * ___U24current_3;
	// BlackjackGame BlackjackGame/<playerPlayDrawGame>c__IteratorE::<>f__this
	BlackjackGame_t1439740240 * ___U3CU3Ef__this_4;

public:
	inline static int32_t get_offset_of_U3CplayerBackgroundU3E__0_0() { return static_cast<int32_t>(offsetof(U3CplayerPlayDrawGameU3Ec__IteratorE_t223069034, ___U3CplayerBackgroundU3E__0_0)); }
	inline GameObject_t3674682005 * get_U3CplayerBackgroundU3E__0_0() const { return ___U3CplayerBackgroundU3E__0_0; }
	inline GameObject_t3674682005 ** get_address_of_U3CplayerBackgroundU3E__0_0() { return &___U3CplayerBackgroundU3E__0_0; }
	inline void set_U3CplayerBackgroundU3E__0_0(GameObject_t3674682005 * value)
	{
		___U3CplayerBackgroundU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CplayerBackgroundU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CnewMatU3E__1_1() { return static_cast<int32_t>(offsetof(U3CplayerPlayDrawGameU3Ec__IteratorE_t223069034, ___U3CnewMatU3E__1_1)); }
	inline Material_t3870600107 * get_U3CnewMatU3E__1_1() const { return ___U3CnewMatU3E__1_1; }
	inline Material_t3870600107 ** get_address_of_U3CnewMatU3E__1_1() { return &___U3CnewMatU3E__1_1; }
	inline void set_U3CnewMatU3E__1_1(Material_t3870600107 * value)
	{
		___U3CnewMatU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CnewMatU3E__1_1, value);
	}

	inline static int32_t get_offset_of_U24PC_2() { return static_cast<int32_t>(offsetof(U3CplayerPlayDrawGameU3Ec__IteratorE_t223069034, ___U24PC_2)); }
	inline int32_t get_U24PC_2() const { return ___U24PC_2; }
	inline int32_t* get_address_of_U24PC_2() { return &___U24PC_2; }
	inline void set_U24PC_2(int32_t value)
	{
		___U24PC_2 = value;
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CplayerPlayDrawGameU3Ec__IteratorE_t223069034, ___U24current_3)); }
	inline Il2CppObject * get_U24current_3() const { return ___U24current_3; }
	inline Il2CppObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(Il2CppObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_3, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_4() { return static_cast<int32_t>(offsetof(U3CplayerPlayDrawGameU3Ec__IteratorE_t223069034, ___U3CU3Ef__this_4)); }
	inline BlackjackGame_t1439740240 * get_U3CU3Ef__this_4() const { return ___U3CU3Ef__this_4; }
	inline BlackjackGame_t1439740240 ** get_address_of_U3CU3Ef__this_4() { return &___U3CU3Ef__this_4; }
	inline void set_U3CU3Ef__this_4(BlackjackGame_t1439740240 * value)
	{
		___U3CU3Ef__this_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
