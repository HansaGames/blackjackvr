﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<Blackjack.Decision>
struct ReadOnlyCollection_1_t2496330454;
// System.Collections.Generic.IList`1<Blackjack.Decision>
struct IList_1_t3633900121;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// Blackjack.Decision[]
struct DecisionU5BU5D_t3084712883;
// System.Collections.Generic.IEnumerator`1<Blackjack.Decision>
struct IEnumerator_1_t2851117967;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Blackjack_Decision939252918.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Blackjack.Decision>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m1756256964_gshared (ReadOnlyCollection_1_t2496330454 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m1756256964(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t2496330454 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m1756256964_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Blackjack.Decision>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1453296302_gshared (ReadOnlyCollection_1_t2496330454 * __this, int32_t ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1453296302(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t2496330454 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1453296302_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Blackjack.Decision>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m4109203420_gshared (ReadOnlyCollection_1_t2496330454 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m4109203420(__this, method) ((  void (*) (ReadOnlyCollection_1_t2496330454 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m4109203420_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Blackjack.Decision>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m4180216853_gshared (ReadOnlyCollection_1_t2496330454 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m4180216853(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t2496330454 *, int32_t, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m4180216853_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Blackjack.Decision>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1104222665_gshared (ReadOnlyCollection_1_t2496330454 * __this, int32_t ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1104222665(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t2496330454 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1104222665_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Blackjack.Decision>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m2054069723_gshared (ReadOnlyCollection_1_t2496330454 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m2054069723(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t2496330454 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m2054069723_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Blackjack.Decision>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3696979073_gshared (ReadOnlyCollection_1_t2496330454 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3696979073(__this, ___index0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2496330454 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3696979073_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Blackjack.Decision>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3203570604_gshared (ReadOnlyCollection_1_t2496330454 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3203570604(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2496330454 *, int32_t, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3203570604_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Blackjack.Decision>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m993312678_gshared (ReadOnlyCollection_1_t2496330454 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m993312678(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2496330454 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m993312678_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Blackjack.Decision>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3445302131_gshared (ReadOnlyCollection_1_t2496330454 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3445302131(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t2496330454 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3445302131_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<Blackjack.Decision>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m523339074_gshared (ReadOnlyCollection_1_t2496330454 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m523339074(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2496330454 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m523339074_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Blackjack.Decision>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m3427697979_gshared (ReadOnlyCollection_1_t2496330454 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m3427697979(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2496330454 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m3427697979_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Blackjack.Decision>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m4166696577_gshared (ReadOnlyCollection_1_t2496330454 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m4166696577(__this, method) ((  void (*) (ReadOnlyCollection_1_t2496330454 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m4166696577_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Blackjack.Decision>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m3813943781_gshared (ReadOnlyCollection_1_t2496330454 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m3813943781(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t2496330454 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m3813943781_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Blackjack.Decision>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m4194543123_gshared (ReadOnlyCollection_1_t2496330454 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m4194543123(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2496330454 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m4194543123_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Blackjack.Decision>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m2547664134_gshared (ReadOnlyCollection_1_t2496330454 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m2547664134(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2496330454 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m2547664134_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Blackjack.Decision>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m3234837090_gshared (ReadOnlyCollection_1_t2496330454 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m3234837090(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t2496330454 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m3234837090_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Blackjack.Decision>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m3081057686_gshared (ReadOnlyCollection_1_t2496330454 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m3081057686(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t2496330454 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m3081057686_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Blackjack.Decision>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2129307479_gshared (ReadOnlyCollection_1_t2496330454 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2129307479(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2496330454 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2129307479_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Blackjack.Decision>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m3505632137_gshared (ReadOnlyCollection_1_t2496330454 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m3505632137(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2496330454 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m3505632137_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Blackjack.Decision>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1453746452_gshared (ReadOnlyCollection_1_t2496330454 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1453746452(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2496330454 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1453746452_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Blackjack.Decision>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1470735205_gshared (ReadOnlyCollection_1_t2496330454 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1470735205(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2496330454 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1470735205_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Blackjack.Decision>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m2723735248_gshared (ReadOnlyCollection_1_t2496330454 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m2723735248(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2496330454 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m2723735248_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Blackjack.Decision>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m787826525_gshared (ReadOnlyCollection_1_t2496330454 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m787826525(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2496330454 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m787826525_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Blackjack.Decision>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m1801249806_gshared (ReadOnlyCollection_1_t2496330454 * __this, int32_t ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m1801249806(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t2496330454 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_Contains_m1801249806_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Blackjack.Decision>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m337545182_gshared (ReadOnlyCollection_1_t2496330454 * __this, DecisionU5BU5D_t3084712883* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m337545182(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t2496330454 *, DecisionU5BU5D_t3084712883*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m337545182_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Blackjack.Decision>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m165780069_gshared (ReadOnlyCollection_1_t2496330454 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m165780069(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t2496330454 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m165780069_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Blackjack.Decision>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m1258298922_gshared (ReadOnlyCollection_1_t2496330454 * __this, int32_t ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m1258298922(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2496330454 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m1258298922_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Blackjack.Decision>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m169868433_gshared (ReadOnlyCollection_1_t2496330454 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m169868433(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t2496330454 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m169868433_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Blackjack.Decision>::get_Item(System.Int32)
extern "C"  int32_t ReadOnlyCollection_1_get_Item_m2029435585_gshared (ReadOnlyCollection_1_t2496330454 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m2029435585(__this, ___index0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2496330454 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m2029435585_gshared)(__this, ___index0, method)
