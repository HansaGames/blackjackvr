﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TestBlackjackGame
struct TestBlackjackGame_t2183801310;
// Blackjack.Game
struct Game_t539648204;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Blackjack_Game539648204.h"

// System.Void TestBlackjackGame::.ctor()
extern "C"  void TestBlackjackGame__ctor_m2142747981 (TestBlackjackGame_t2183801310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestBlackjackGame::newGame(Blackjack.Game)
extern "C"  void TestBlackjackGame_newGame_m3717307067 (TestBlackjackGame_t2183801310 * __this, Game_t539648204 * ___game0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator TestBlackjackGame::endGame()
extern "C"  Il2CppObject * TestBlackjackGame_endGame_m598036096 (TestBlackjackGame_t2183801310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
