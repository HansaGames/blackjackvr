﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<Blackjack.Decision>
struct List_1_t2307438470;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2327111240.h"
#include "AssemblyU2DCSharp_Blackjack_Decision939252918.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Blackjack.Decision>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m457784767_gshared (Enumerator_t2327111240 * __this, List_1_t2307438470 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m457784767(__this, ___l0, method) ((  void (*) (Enumerator_t2327111240 *, List_1_t2307438470 *, const MethodInfo*))Enumerator__ctor_m457784767_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Blackjack.Decision>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4265081651_gshared (Enumerator_t2327111240 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m4265081651(__this, method) ((  void (*) (Enumerator_t2327111240 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m4265081651_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Blackjack.Decision>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3717573289_gshared (Enumerator_t2327111240 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3717573289(__this, method) ((  Il2CppObject * (*) (Enumerator_t2327111240 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3717573289_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Blackjack.Decision>::Dispose()
extern "C"  void Enumerator_Dispose_m118877028_gshared (Enumerator_t2327111240 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m118877028(__this, method) ((  void (*) (Enumerator_t2327111240 *, const MethodInfo*))Enumerator_Dispose_m118877028_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Blackjack.Decision>::VerifyState()
extern "C"  void Enumerator_VerifyState_m3537721501_gshared (Enumerator_t2327111240 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m3537721501(__this, method) ((  void (*) (Enumerator_t2327111240 *, const MethodInfo*))Enumerator_VerifyState_m3537721501_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Blackjack.Decision>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1923894668_gshared (Enumerator_t2327111240 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1923894668(__this, method) ((  bool (*) (Enumerator_t2327111240 *, const MethodInfo*))Enumerator_MoveNext_m1923894668_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Blackjack.Decision>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m1550460752_gshared (Enumerator_t2327111240 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1550460752(__this, method) ((  int32_t (*) (Enumerator_t2327111240 *, const MethodInfo*))Enumerator_get_Current_m1550460752_gshared)(__this, method)
