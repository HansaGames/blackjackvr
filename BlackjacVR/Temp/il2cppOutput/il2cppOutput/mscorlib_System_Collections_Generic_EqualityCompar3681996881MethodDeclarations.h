﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Blackjack.Decision>
struct DefaultComparer_t3681996881;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Blackjack_Decision939252918.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Blackjack.Decision>::.ctor()
extern "C"  void DefaultComparer__ctor_m2737229528_gshared (DefaultComparer_t3681996881 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m2737229528(__this, method) ((  void (*) (DefaultComparer_t3681996881 *, const MethodInfo*))DefaultComparer__ctor_m2737229528_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Blackjack.Decision>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m558840859_gshared (DefaultComparer_t3681996881 * __this, int32_t ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m558840859(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t3681996881 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m558840859_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Blackjack.Decision>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m1303336557_gshared (DefaultComparer_t3681996881 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m1303336557(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t3681996881 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m1303336557_gshared)(__this, ___x0, ___y1, method)
