﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StrategyTutorial/<showCardCombinations>c__Iterator13
struct U3CshowCardCombinationsU3Ec__Iterator13_t3946452932;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void StrategyTutorial/<showCardCombinations>c__Iterator13::.ctor()
extern "C"  void U3CshowCardCombinationsU3Ec__Iterator13__ctor_m1594122071 (U3CshowCardCombinationsU3Ec__Iterator13_t3946452932 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object StrategyTutorial/<showCardCombinations>c__Iterator13::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CshowCardCombinationsU3Ec__Iterator13_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1502582373 (U3CshowCardCombinationsU3Ec__Iterator13_t3946452932 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object StrategyTutorial/<showCardCombinations>c__Iterator13::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CshowCardCombinationsU3Ec__Iterator13_System_Collections_IEnumerator_get_Current_m1505559545 (U3CshowCardCombinationsU3Ec__Iterator13_t3946452932 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean StrategyTutorial/<showCardCombinations>c__Iterator13::MoveNext()
extern "C"  bool U3CshowCardCombinationsU3Ec__Iterator13_MoveNext_m2057687333 (U3CshowCardCombinationsU3Ec__Iterator13_t3946452932 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StrategyTutorial/<showCardCombinations>c__Iterator13::Dispose()
extern "C"  void U3CshowCardCombinationsU3Ec__Iterator13_Dispose_m2841235924 (U3CshowCardCombinationsU3Ec__Iterator13_t3946452932 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StrategyTutorial/<showCardCombinations>c__Iterator13::Reset()
extern "C"  void U3CshowCardCombinationsU3Ec__Iterator13_Reset_m3535522308 (U3CshowCardCombinationsU3Ec__Iterator13_t3946452932 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
