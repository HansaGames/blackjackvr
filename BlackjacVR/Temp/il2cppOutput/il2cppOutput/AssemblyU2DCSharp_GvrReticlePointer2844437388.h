﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Material
struct Material_t3870600107;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<TimingScript>
struct EventFunction_1_t621096391;

#include "AssemblyU2DCSharp_GvrBasePointer3602508009.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrReticlePointer
struct  GvrReticlePointer_t2844437388  : public GvrBasePointer_t3602508009
{
public:
	// System.Int32 GvrReticlePointer::reticleSegments
	int32_t ___reticleSegments_8;
	// System.Single GvrReticlePointer::reticleGrowthSpeed
	float ___reticleGrowthSpeed_9;
	// UnityEngine.Material GvrReticlePointer::materialComp
	Material_t3870600107 * ___materialComp_10;
	// System.Single GvrReticlePointer::reticleInnerAngle
	float ___reticleInnerAngle_11;
	// System.Single GvrReticlePointer::reticleOuterAngle
	float ___reticleOuterAngle_12;
	// System.Single GvrReticlePointer::reticleDistanceInMeters
	float ___reticleDistanceInMeters_13;
	// System.Single GvrReticlePointer::reticleInnerDiameter
	float ___reticleInnerDiameter_14;
	// System.Single GvrReticlePointer::reticleOuterDiameter
	float ___reticleOuterDiameter_15;
	// System.Single GvrReticlePointer::gazeStartTime
	float ___gazeStartTime_16;
	// UnityEngine.GameObject GvrReticlePointer::gazedAt
	GameObject_t3674682005 * ___gazedAt_17;

public:
	inline static int32_t get_offset_of_reticleSegments_8() { return static_cast<int32_t>(offsetof(GvrReticlePointer_t2844437388, ___reticleSegments_8)); }
	inline int32_t get_reticleSegments_8() const { return ___reticleSegments_8; }
	inline int32_t* get_address_of_reticleSegments_8() { return &___reticleSegments_8; }
	inline void set_reticleSegments_8(int32_t value)
	{
		___reticleSegments_8 = value;
	}

	inline static int32_t get_offset_of_reticleGrowthSpeed_9() { return static_cast<int32_t>(offsetof(GvrReticlePointer_t2844437388, ___reticleGrowthSpeed_9)); }
	inline float get_reticleGrowthSpeed_9() const { return ___reticleGrowthSpeed_9; }
	inline float* get_address_of_reticleGrowthSpeed_9() { return &___reticleGrowthSpeed_9; }
	inline void set_reticleGrowthSpeed_9(float value)
	{
		___reticleGrowthSpeed_9 = value;
	}

	inline static int32_t get_offset_of_materialComp_10() { return static_cast<int32_t>(offsetof(GvrReticlePointer_t2844437388, ___materialComp_10)); }
	inline Material_t3870600107 * get_materialComp_10() const { return ___materialComp_10; }
	inline Material_t3870600107 ** get_address_of_materialComp_10() { return &___materialComp_10; }
	inline void set_materialComp_10(Material_t3870600107 * value)
	{
		___materialComp_10 = value;
		Il2CppCodeGenWriteBarrier(&___materialComp_10, value);
	}

	inline static int32_t get_offset_of_reticleInnerAngle_11() { return static_cast<int32_t>(offsetof(GvrReticlePointer_t2844437388, ___reticleInnerAngle_11)); }
	inline float get_reticleInnerAngle_11() const { return ___reticleInnerAngle_11; }
	inline float* get_address_of_reticleInnerAngle_11() { return &___reticleInnerAngle_11; }
	inline void set_reticleInnerAngle_11(float value)
	{
		___reticleInnerAngle_11 = value;
	}

	inline static int32_t get_offset_of_reticleOuterAngle_12() { return static_cast<int32_t>(offsetof(GvrReticlePointer_t2844437388, ___reticleOuterAngle_12)); }
	inline float get_reticleOuterAngle_12() const { return ___reticleOuterAngle_12; }
	inline float* get_address_of_reticleOuterAngle_12() { return &___reticleOuterAngle_12; }
	inline void set_reticleOuterAngle_12(float value)
	{
		___reticleOuterAngle_12 = value;
	}

	inline static int32_t get_offset_of_reticleDistanceInMeters_13() { return static_cast<int32_t>(offsetof(GvrReticlePointer_t2844437388, ___reticleDistanceInMeters_13)); }
	inline float get_reticleDistanceInMeters_13() const { return ___reticleDistanceInMeters_13; }
	inline float* get_address_of_reticleDistanceInMeters_13() { return &___reticleDistanceInMeters_13; }
	inline void set_reticleDistanceInMeters_13(float value)
	{
		___reticleDistanceInMeters_13 = value;
	}

	inline static int32_t get_offset_of_reticleInnerDiameter_14() { return static_cast<int32_t>(offsetof(GvrReticlePointer_t2844437388, ___reticleInnerDiameter_14)); }
	inline float get_reticleInnerDiameter_14() const { return ___reticleInnerDiameter_14; }
	inline float* get_address_of_reticleInnerDiameter_14() { return &___reticleInnerDiameter_14; }
	inline void set_reticleInnerDiameter_14(float value)
	{
		___reticleInnerDiameter_14 = value;
	}

	inline static int32_t get_offset_of_reticleOuterDiameter_15() { return static_cast<int32_t>(offsetof(GvrReticlePointer_t2844437388, ___reticleOuterDiameter_15)); }
	inline float get_reticleOuterDiameter_15() const { return ___reticleOuterDiameter_15; }
	inline float* get_address_of_reticleOuterDiameter_15() { return &___reticleOuterDiameter_15; }
	inline void set_reticleOuterDiameter_15(float value)
	{
		___reticleOuterDiameter_15 = value;
	}

	inline static int32_t get_offset_of_gazeStartTime_16() { return static_cast<int32_t>(offsetof(GvrReticlePointer_t2844437388, ___gazeStartTime_16)); }
	inline float get_gazeStartTime_16() const { return ___gazeStartTime_16; }
	inline float* get_address_of_gazeStartTime_16() { return &___gazeStartTime_16; }
	inline void set_gazeStartTime_16(float value)
	{
		___gazeStartTime_16 = value;
	}

	inline static int32_t get_offset_of_gazedAt_17() { return static_cast<int32_t>(offsetof(GvrReticlePointer_t2844437388, ___gazedAt_17)); }
	inline GameObject_t3674682005 * get_gazedAt_17() const { return ___gazedAt_17; }
	inline GameObject_t3674682005 ** get_address_of_gazedAt_17() { return &___gazedAt_17; }
	inline void set_gazedAt_17(GameObject_t3674682005 * value)
	{
		___gazedAt_17 = value;
		Il2CppCodeGenWriteBarrier(&___gazedAt_17, value);
	}
};

struct GvrReticlePointer_t2844437388_StaticFields
{
public:
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<TimingScript> GvrReticlePointer::<>f__am$cacheA
	EventFunction_1_t621096391 * ___U3CU3Ef__amU24cacheA_18;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheA_18() { return static_cast<int32_t>(offsetof(GvrReticlePointer_t2844437388_StaticFields, ___U3CU3Ef__amU24cacheA_18)); }
	inline EventFunction_1_t621096391 * get_U3CU3Ef__amU24cacheA_18() const { return ___U3CU3Ef__amU24cacheA_18; }
	inline EventFunction_1_t621096391 ** get_address_of_U3CU3Ef__amU24cacheA_18() { return &___U3CU3Ef__amU24cacheA_18; }
	inline void set_U3CU3Ef__amU24cacheA_18(EventFunction_1_t621096391 * value)
	{
		___U3CU3Ef__amU24cacheA_18 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheA_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
