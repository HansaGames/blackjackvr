﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TestBlackjackGame2183801310.h"
#include "AssemblyU2DCSharp_TestBlackjackGame_U3CendGameU3Ec_584020149.h"
#include "AssemblyU2DCSharp_TutorialManager900157007.h"
#include "AssemblyU2DCSharp_BaseTutorial2983299439.h"
#include "AssemblyU2DCSharp_IntroTutorial3943889354.h"
#include "AssemblyU2DCSharp_OptionsTutorial4157558780.h"
#include "AssemblyU2DCSharp_PointsTutorial2107975041.h"
#include "AssemblyU2DCSharp_PointsTutorial_U3CshowCardsU3Ec_1334761580.h"
#include "AssemblyU2DCSharp_RulesTutorial3317375317.h"
#include "AssemblyU2DCSharp_StrategyTutorial1981699089.h"
#include "AssemblyU2DCSharp_StrategyTutorial_U3CshowCardComb3946452932.h"
#include "AssemblyU2DCSharp_StrategyTutorial_U3CbothStiffStr3699004146.h"
#include "AssemblyU2DCSharp_StrategyTutorial_U3CstiffAndPatSt642946848.h"
#include "AssemblyU2DCSharp_StrategyTutorial_U3CpatStrategyU3235699248.h"
#include "AssemblyU2DCSharp_TestTutorial1071937072.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3053238933.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3379220377.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3379220385.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3379220352.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3379220443.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2000 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2000[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2001 = { sizeof (TestBlackjackGame_t2183801310), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2002 = { sizeof (U3CendGameU3Ec__Iterator11_t584020149), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2002[3] = 
{
	U3CendGameU3Ec__Iterator11_t584020149::get_offset_of_U24PC_0(),
	U3CendGameU3Ec__Iterator11_t584020149::get_offset_of_U24current_1(),
	U3CendGameU3Ec__Iterator11_t584020149::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2003 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2004 = { sizeof (TutorialManager_t900157007), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2004[3] = 
{
	TutorialManager_t900157007::get_offset_of_tutorialButtons_2(),
	TutorialManager_t900157007::get_offset_of_currentTutorial_3(),
	TutorialManager_t900157007::get_offset_of_currentTutorialGO_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2005 = { sizeof (BaseTutorial_t2983299439), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2005[1] = 
{
	BaseTutorial_t2983299439::get_offset_of_audio_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2006 = { sizeof (IntroTutorial_t3943889354), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2007 = { sizeof (OptionsTutorial_t4157558780), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2008 = { sizeof (PointsTutorial_t2107975041), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2009 = { sizeof (U3CshowCardsU3Ec__Iterator12_t1334761580), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2009[2] = 
{
	U3CshowCardsU3Ec__Iterator12_t1334761580::get_offset_of_U24PC_0(),
	U3CshowCardsU3Ec__Iterator12_t1334761580::get_offset_of_U24current_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2010 = { sizeof (RulesTutorial_t3317375317), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2011 = { sizeof (StrategyTutorial_t1981699089), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2012 = { sizeof (U3CshowCardCombinationsU3Ec__Iterator13_t3946452932), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2012[3] = 
{
	U3CshowCardCombinationsU3Ec__Iterator13_t3946452932::get_offset_of_U24PC_0(),
	U3CshowCardCombinationsU3Ec__Iterator13_t3946452932::get_offset_of_U24current_1(),
	U3CshowCardCombinationsU3Ec__Iterator13_t3946452932::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2013 = { sizeof (U3CbothStiffStrategyU3Ec__Iterator14_t3699004146), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2013[3] = 
{
	U3CbothStiffStrategyU3Ec__Iterator14_t3699004146::get_offset_of_U24PC_0(),
	U3CbothStiffStrategyU3Ec__Iterator14_t3699004146::get_offset_of_U24current_1(),
	U3CbothStiffStrategyU3Ec__Iterator14_t3699004146::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2014 = { sizeof (U3CstiffAndPatStrategyU3Ec__Iterator15_t642946848), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2014[3] = 
{
	U3CstiffAndPatStrategyU3Ec__Iterator15_t642946848::get_offset_of_U24PC_0(),
	U3CstiffAndPatStrategyU3Ec__Iterator15_t642946848::get_offset_of_U24current_1(),
	U3CstiffAndPatStrategyU3Ec__Iterator15_t642946848::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2015 = { sizeof (U3CpatStrategyU3Ec__Iterator16_t3235699248), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2015[3] = 
{
	U3CpatStrategyU3Ec__Iterator16_t3235699248::get_offset_of_U24PC_0(),
	U3CpatStrategyU3Ec__Iterator16_t3235699248::get_offset_of_U24current_1(),
	U3CpatStrategyU3Ec__Iterator16_t3235699248::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2016 = { sizeof (TestTutorial_t1071937072), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2016[1] = 
{
	TestTutorial_t1071937072::get_offset_of_numberOfTest_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2017 = { sizeof (U3CPrivateImplementationDetailsU3E_t3053238938), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3053238938_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2017[7] = 
{
	U3CPrivateImplementationDetailsU3E_t3053238938_StaticFields::get_offset_of_U24U24fieldU2D0_0(),
	U3CPrivateImplementationDetailsU3E_t3053238938_StaticFields::get_offset_of_U24U24fieldU2D1_1(),
	U3CPrivateImplementationDetailsU3E_t3053238938_StaticFields::get_offset_of_U24U24fieldU2D2_2(),
	U3CPrivateImplementationDetailsU3E_t3053238938_StaticFields::get_offset_of_U24U24fieldU2D3_3(),
	U3CPrivateImplementationDetailsU3E_t3053238938_StaticFields::get_offset_of_U24U24fieldU2D4_4(),
	U3CPrivateImplementationDetailsU3E_t3053238938_StaticFields::get_offset_of_U24U24fieldU2D5_5(),
	U3CPrivateImplementationDetailsU3E_t3053238938_StaticFields::get_offset_of_U24U24fieldU2D6_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2018 = { sizeof (U24ArrayTypeU2420_t3379220379)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2420_t3379220379_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2019 = { sizeof (U24ArrayTypeU2428_t3379220385)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2428_t3379220385_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2020 = { sizeof (U24ArrayTypeU2416_t3379220354)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2416_t3379220354_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2021 = { sizeof (U24ArrayTypeU2444_t3379220443)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2444_t3379220443_marshaled_pinvoke), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
