﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StrategyTutorial/<patStrategy>c__Iterator16
struct U3CpatStrategyU3Ec__Iterator16_t3235699248;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void StrategyTutorial/<patStrategy>c__Iterator16::.ctor()
extern "C"  void U3CpatStrategyU3Ec__Iterator16__ctor_m488863547 (U3CpatStrategyU3Ec__Iterator16_t3235699248 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object StrategyTutorial/<patStrategy>c__Iterator16::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CpatStrategyU3Ec__Iterator16_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3941220983 (U3CpatStrategyU3Ec__Iterator16_t3235699248 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object StrategyTutorial/<patStrategy>c__Iterator16::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CpatStrategyU3Ec__Iterator16_System_Collections_IEnumerator_get_Current_m3544585227 (U3CpatStrategyU3Ec__Iterator16_t3235699248 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean StrategyTutorial/<patStrategy>c__Iterator16::MoveNext()
extern "C"  bool U3CpatStrategyU3Ec__Iterator16_MoveNext_m2878162649 (U3CpatStrategyU3Ec__Iterator16_t3235699248 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StrategyTutorial/<patStrategy>c__Iterator16::Dispose()
extern "C"  void U3CpatStrategyU3Ec__Iterator16_Dispose_m1544716472 (U3CpatStrategyU3Ec__Iterator16_t3235699248 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StrategyTutorial/<patStrategy>c__Iterator16::Reset()
extern "C"  void U3CpatStrategyU3Ec__Iterator16_Reset_m2430263784 (U3CpatStrategyU3Ec__Iterator16_t3235699248 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
