﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Int32[]
struct Int32U5BU5D_t3230847821;
// Blackjack.Card
struct Card_t539529194;
struct Card_t539529194_marshaled_pinvoke;
struct Card_t539529194_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Blackjack_Card539529194.h"
#include "AssemblyU2DCSharp_Blackjack_Suit540024807.h"
#include "AssemblyU2DCSharp_Blackjack_CardType365306692.h"

// System.Void Blackjack.Card::.ctor(Blackjack.Suit,Blackjack.CardType,System.Int32[])
extern "C"  void Card__ctor_m3450322919 (Card_t539529194 * __this, int32_t ___suit0, int32_t ___type1, Int32U5BU5D_t3230847821* ___values2, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct Card_t539529194;
struct Card_t539529194_marshaled_pinvoke;

extern "C" void Card_t539529194_marshal_pinvoke(const Card_t539529194& unmarshaled, Card_t539529194_marshaled_pinvoke& marshaled);
extern "C" void Card_t539529194_marshal_pinvoke_back(const Card_t539529194_marshaled_pinvoke& marshaled, Card_t539529194& unmarshaled);
extern "C" void Card_t539529194_marshal_pinvoke_cleanup(Card_t539529194_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct Card_t539529194;
struct Card_t539529194_marshaled_com;

extern "C" void Card_t539529194_marshal_com(const Card_t539529194& unmarshaled, Card_t539529194_marshaled_com& marshaled);
extern "C" void Card_t539529194_marshal_com_back(const Card_t539529194_marshaled_com& marshaled, Card_t539529194& unmarshaled);
extern "C" void Card_t539529194_marshal_com_cleanup(Card_t539529194_marshaled_com& marshaled);
