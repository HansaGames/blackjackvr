﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StrategyTutorial/<bothStiffStrategy>c__Iterator14
struct U3CbothStiffStrategyU3Ec__Iterator14_t3699004146;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void StrategyTutorial/<bothStiffStrategy>c__Iterator14::.ctor()
extern "C"  void U3CbothStiffStrategyU3Ec__Iterator14__ctor_m372566713 (U3CbothStiffStrategyU3Ec__Iterator14_t3699004146 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object StrategyTutorial/<bothStiffStrategy>c__Iterator14::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CbothStiffStrategyU3Ec__Iterator14_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2777671033 (U3CbothStiffStrategyU3Ec__Iterator14_t3699004146 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object StrategyTutorial/<bothStiffStrategy>c__Iterator14::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CbothStiffStrategyU3Ec__Iterator14_System_Collections_IEnumerator_get_Current_m2202401549 (U3CbothStiffStrategyU3Ec__Iterator14_t3699004146 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean StrategyTutorial/<bothStiffStrategy>c__Iterator14::MoveNext()
extern "C"  bool U3CbothStiffStrategyU3Ec__Iterator14_MoveNext_m243986843 (U3CbothStiffStrategyU3Ec__Iterator14_t3699004146 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StrategyTutorial/<bothStiffStrategy>c__Iterator14::Dispose()
extern "C"  void U3CbothStiffStrategyU3Ec__Iterator14_Dispose_m1452608694 (U3CbothStiffStrategyU3Ec__Iterator14_t3699004146 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StrategyTutorial/<bothStiffStrategy>c__Iterator14::Reset()
extern "C"  void U3CbothStiffStrategyU3Ec__Iterator14_Reset_m2313966950 (U3CbothStiffStrategyU3Ec__Iterator14_t3699004146 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
