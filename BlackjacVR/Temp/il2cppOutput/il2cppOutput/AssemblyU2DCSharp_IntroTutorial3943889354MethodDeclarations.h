﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IntroTutorial
struct IntroTutorial_t3943889354;
// UnityEngine.AudioClip
struct AudioClip_t794140988;

#include "codegen/il2cpp-codegen.h"

// System.Void IntroTutorial::.ctor()
extern "C"  void IntroTutorial__ctor_m2162346721 (IntroTutorial_t3943889354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IntroTutorial::Start()
extern "C"  void IntroTutorial_Start_m1109484513 (IntroTutorial_t3943889354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioClip IntroTutorial::getTutorialClip()
extern "C"  AudioClip_t794140988 * IntroTutorial_getTutorialClip_m724801191 (IntroTutorial_t3943889354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
