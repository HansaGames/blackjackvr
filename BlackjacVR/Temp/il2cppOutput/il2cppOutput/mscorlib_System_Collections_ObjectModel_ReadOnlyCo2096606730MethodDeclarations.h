﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<Blackjack.Card>
struct ReadOnlyCollection_1_t2096606730;
// System.Collections.Generic.IList`1<Blackjack.Card>
struct IList_1_t3234176397;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// Blackjack.Card[]
struct CardU5BU5D_t1346973999;
// System.Collections.Generic.IEnumerator`1<Blackjack.Card>
struct IEnumerator_1_t2451394243;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Blackjack_Card539529194.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Blackjack.Card>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m1606289680_gshared (ReadOnlyCollection_1_t2096606730 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m1606289680(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t2096606730 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m1606289680_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Blackjack.Card>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3327603450_gshared (ReadOnlyCollection_1_t2096606730 * __this, Card_t539529194  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3327603450(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t2096606730 *, Card_t539529194 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3327603450_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Blackjack.Card>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m2083182864_gshared (ReadOnlyCollection_1_t2096606730 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m2083182864(__this, method) ((  void (*) (ReadOnlyCollection_1_t2096606730 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m2083182864_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Blackjack.Card>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3109274977_gshared (ReadOnlyCollection_1_t2096606730 * __this, int32_t ___index0, Card_t539529194  ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3109274977(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t2096606730 *, int32_t, Card_t539529194 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3109274977_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Blackjack.Card>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m892817149_gshared (ReadOnlyCollection_1_t2096606730 * __this, Card_t539529194  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m892817149(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t2096606730 *, Card_t539529194 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m892817149_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Blackjack.Card>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m983127847_gshared (ReadOnlyCollection_1_t2096606730 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m983127847(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t2096606730 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m983127847_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Blackjack.Card>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  Card_t539529194  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1320926797_gshared (ReadOnlyCollection_1_t2096606730 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1320926797(__this, ___index0, method) ((  Card_t539529194  (*) (ReadOnlyCollection_1_t2096606730 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1320926797_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Blackjack.Card>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m525611512_gshared (ReadOnlyCollection_1_t2096606730 * __this, int32_t ___index0, Card_t539529194  ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m525611512(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2096606730 *, int32_t, Card_t539529194 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m525611512_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Blackjack.Card>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2165704690_gshared (ReadOnlyCollection_1_t2096606730 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2165704690(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2096606730 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2165704690_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Blackjack.Card>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1529918399_gshared (ReadOnlyCollection_1_t2096606730 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1529918399(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t2096606730 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1529918399_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<Blackjack.Card>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3360470414_gshared (ReadOnlyCollection_1_t2096606730 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3360470414(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2096606730 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3360470414_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Blackjack.Card>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m2190035823_gshared (ReadOnlyCollection_1_t2096606730 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m2190035823(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2096606730 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m2190035823_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Blackjack.Card>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m3240394445_gshared (ReadOnlyCollection_1_t2096606730 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m3240394445(__this, method) ((  void (*) (ReadOnlyCollection_1_t2096606730 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m3240394445_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Blackjack.Card>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m2715361585_gshared (ReadOnlyCollection_1_t2096606730 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m2715361585(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t2096606730 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m2715361585_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Blackjack.Card>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3964154439_gshared (ReadOnlyCollection_1_t2096606730 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3964154439(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2096606730 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3964154439_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Blackjack.Card>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m3708204346_gshared (ReadOnlyCollection_1_t2096606730 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m3708204346(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2096606730 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m3708204346_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Blackjack.Card>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m850197934_gshared (ReadOnlyCollection_1_t2096606730 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m850197934(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t2096606730 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m850197934_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Blackjack.Card>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m2171687882_gshared (ReadOnlyCollection_1_t2096606730 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m2171687882(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t2096606730 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m2171687882_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Blackjack.Card>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1917901963_gshared (ReadOnlyCollection_1_t2096606730 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1917901963(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2096606730 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1917901963_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Blackjack.Card>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m2634969405_gshared (ReadOnlyCollection_1_t2096606730 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m2634969405(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2096606730 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m2634969405_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Blackjack.Card>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m3168389472_gshared (ReadOnlyCollection_1_t2096606730 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m3168389472(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2096606730 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m3168389472_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Blackjack.Card>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m279120281_gshared (ReadOnlyCollection_1_t2096606730 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m279120281(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2096606730 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m279120281_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Blackjack.Card>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m3535472516_gshared (ReadOnlyCollection_1_t2096606730 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m3535472516(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2096606730 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m3535472516_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Blackjack.Card>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m3670440593_gshared (ReadOnlyCollection_1_t2096606730 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m3670440593(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2096606730 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m3670440593_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Blackjack.Card>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m3957808450_gshared (ReadOnlyCollection_1_t2096606730 * __this, Card_t539529194  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m3957808450(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t2096606730 *, Card_t539529194 , const MethodInfo*))ReadOnlyCollection_1_Contains_m3957808450_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Blackjack.Card>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m2485343274_gshared (ReadOnlyCollection_1_t2096606730 * __this, CardU5BU5D_t1346973999* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m2485343274(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t2096606730 *, CardU5BU5D_t1346973999*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m2485343274_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Blackjack.Card>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m2385868441_gshared (ReadOnlyCollection_1_t2096606730 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m2385868441(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t2096606730 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m2385868441_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Blackjack.Card>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m2885284214_gshared (ReadOnlyCollection_1_t2096606730 * __this, Card_t539529194  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m2885284214(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2096606730 *, Card_t539529194 , const MethodInfo*))ReadOnlyCollection_1_IndexOf_m2885284214_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Blackjack.Card>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m3361772229_gshared (ReadOnlyCollection_1_t2096606730 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m3361772229(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t2096606730 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m3361772229_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Blackjack.Card>::get_Item(System.Int32)
extern "C"  Card_t539529194  ReadOnlyCollection_1_get_Item_m393347725_gshared (ReadOnlyCollection_1_t2096606730 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m393347725(__this, ___index0, method) ((  Card_t539529194  (*) (ReadOnlyCollection_1_t2096606730 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m393347725_gshared)(__this, ___index0, method)
