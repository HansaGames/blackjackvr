﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<System.Action`1<System.Int32>>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m2609488736(__this, ___l0, method) ((  void (*) (Enumerator_t2937512958 *, List_1_t2917840188 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Action`1<System.Int32>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2936329458(__this, method) ((  void (*) (Enumerator_t2937512958 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Action`1<System.Int32>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m458842782(__this, method) ((  Il2CppObject * (*) (Enumerator_t2937512958 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Action`1<System.Int32>>::Dispose()
#define Enumerator_Dispose_m1188890949(__this, method) ((  void (*) (Enumerator_t2937512958 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Action`1<System.Int32>>::VerifyState()
#define Enumerator_VerifyState_m2083560958(__this, method) ((  void (*) (Enumerator_t2937512958 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Action`1<System.Int32>>::MoveNext()
#define Enumerator_MoveNext_m1832265694(__this, method) ((  bool (*) (Enumerator_t2937512958 *, const MethodInfo*))Enumerator_MoveNext_m844464217_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Action`1<System.Int32>>::get_Current()
#define Enumerator_get_Current_m4137244661(__this, method) ((  Action_1_t1549654636 * (*) (Enumerator_t2937512958 *, const MethodInfo*))Enumerator_get_Current_m4198990746_gshared)(__this, method)
