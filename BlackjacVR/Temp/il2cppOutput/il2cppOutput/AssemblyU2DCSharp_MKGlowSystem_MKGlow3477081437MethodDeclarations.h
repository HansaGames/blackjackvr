﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MKGlowSystem.MKGlow
struct MKGlow_t3477081437;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UnityEngine.Camera
struct Camera_t2727095145;
// UnityEngine.Material
struct Material_t3870600107;
// UnityEngine.RenderTexture
struct RenderTexture_t1963041563;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_LayerMask3236759763.h"
#include "AssemblyU2DCSharp_MKGlowSystem_MKGlowType2044315959.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "UnityEngine_UnityEngine_RenderTexture1963041563.h"

// System.Void MKGlowSystem.MKGlow::.ctor()
extern "C"  void MKGlow__ctor_m3646184428 (MKGlow_t3477081437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MKGlowSystem.MKGlow::.cctor()
extern "C"  void MKGlow__cctor_m880471361 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject MKGlowSystem.MKGlow::get_GlowCameraObject()
extern "C"  GameObject_t3674682005 * MKGlow_get_GlowCameraObject_m172077117 (MKGlow_t3477081437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera MKGlowSystem.MKGlow::get_GlowCamera()
extern "C"  Camera_t2727095145 * MKGlow_get_GlowCamera_m2202260914 (MKGlow_t3477081437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.LayerMask MKGlowSystem.MKGlow::get_GlowLayer()
extern "C"  LayerMask_t3236759763  MKGlow_get_GlowLayer_m4134986482 (MKGlow_t3477081437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MKGlowSystem.MKGlow::set_GlowLayer(UnityEngine.LayerMask)
extern "C"  void MKGlow_set_GlowLayer_m3648715547 (MKGlow_t3477081437 * __this, LayerMask_t3236759763  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MKGlowSystem.MKGlow::get_ShowCutoutGlow()
extern "C"  bool MKGlow_get_ShowCutoutGlow_m3225942019 (MKGlow_t3477081437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MKGlowSystem.MKGlow::set_ShowCutoutGlow(System.Boolean)
extern "C"  void MKGlow_set_ShowCutoutGlow_m4160718562 (MKGlow_t3477081437 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MKGlowSystem.MKGlow::get_ShowTransparentGlow()
extern "C"  bool MKGlow_get_ShowTransparentGlow_m230517015 (MKGlow_t3477081437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MKGlowSystem.MKGlow::set_ShowTransparentGlow(System.Boolean)
extern "C"  void MKGlow_set_ShowTransparentGlow_m3298605222 (MKGlow_t3477081437 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MKGlowSystem.MKGlowType MKGlowSystem.MKGlow::get_GlowType()
extern "C"  int32_t MKGlow_get_GlowType_m1784944590 (MKGlow_t3477081437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MKGlowSystem.MKGlow::set_GlowType(MKGlowSystem.MKGlowType)
extern "C"  void MKGlow_set_GlowType_m1356761693 (MKGlow_t3477081437 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color MKGlowSystem.MKGlow::get_GlowTint()
extern "C"  Color_t4194546905  MKGlow_get_GlowTint_m502332386 (MKGlow_t3477081437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MKGlowSystem.MKGlow::set_GlowTint(UnityEngine.Color)
extern "C"  void MKGlow_set_GlowTint_m849701577 (MKGlow_t3477081437 * __this, Color_t4194546905  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 MKGlowSystem.MKGlow::get_Samples()
extern "C"  int32_t MKGlow_get_Samples_m326245624 (MKGlow_t3477081437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MKGlowSystem.MKGlow::set_Samples(System.Int32)
extern "C"  void MKGlow_set_Samples_m2892103879 (MKGlow_t3477081437 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 MKGlowSystem.MKGlow::get_BlurIterations()
extern "C"  int32_t MKGlow_get_BlurIterations_m805189840 (MKGlow_t3477081437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MKGlowSystem.MKGlow::set_BlurIterations(System.Int32)
extern "C"  void MKGlow_set_BlurIterations_m3749784035 (MKGlow_t3477081437 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single MKGlowSystem.MKGlow::get_GlowIntensity()
extern "C"  float MKGlow_get_GlowIntensity_m3385771283 (MKGlow_t3477081437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MKGlowSystem.MKGlow::set_GlowIntensity(System.Single)
extern "C"  void MKGlow_set_GlowIntensity_m3773433656 (MKGlow_t3477081437 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single MKGlowSystem.MKGlow::get_BlurSpread()
extern "C"  float MKGlow_get_BlurSpread_m3192822351 (MKGlow_t3477081437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MKGlowSystem.MKGlow::set_BlurSpread(System.Single)
extern "C"  void MKGlow_set_BlurSpread_m2635777020 (MKGlow_t3477081437 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material MKGlowSystem.MKGlow::get_BlurMaterial()
extern "C"  Material_t3870600107 * MKGlow_get_BlurMaterial_m3855755792 (MKGlow_t3477081437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material MKGlowSystem.MKGlow::get_CompositeMaterial()
extern "C"  Material_t3870600107 * MKGlow_get_CompositeMaterial_m2692603150 (MKGlow_t3477081437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MKGlowSystem.MKGlow::Main()
extern "C"  void MKGlow_Main_m2942607185 (MKGlow_t3477081437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MKGlowSystem.MKGlow::Reset()
extern "C"  void MKGlow_Reset_m1292617369 (MKGlow_t3477081437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MKGlowSystem.MKGlow::OnEnable()
extern "C"  void MKGlow_OnEnable_m1533989914 (MKGlow_t3477081437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MKGlowSystem.MKGlow::SetupKeywords()
extern "C"  void MKGlow_SetupKeywords_m2803491729 (MKGlow_t3477081437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MKGlowSystem.MKGlow::SetupShaders()
extern "C"  void MKGlow_SetupShaders_m3642798217 (MKGlow_t3477081437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MKGlowSystem.MKGlow::OnDisable()
extern "C"  void MKGlow_OnDisable_m749984339 (MKGlow_t3477081437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MKGlowSystem.MKGlow::SetupGlowCamera()
extern "C"  void MKGlow_SetupGlowCamera_m1636192121 (MKGlow_t3477081437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MKGlowSystem.MKGlow::OnPreRender()
extern "C"  void MKGlow_OnPreRender_m3501151940 (MKGlow_t3477081437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MKGlowSystem.MKGlow::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void MKGlow_OnRenderImage_m2448112882 (MKGlow_t3477081437 * __this, RenderTexture_t1963041563 * ___src0, RenderTexture_t1963041563 * ___dest1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MKGlowSystem.MKGlow::PerformBlur(UnityEngine.RenderTexture&,UnityEngine.RenderTexture&)
extern "C"  void MKGlow_PerformBlur_m3202426772 (MKGlow_t3477081437 * __this, RenderTexture_t1963041563 ** ___src0, RenderTexture_t1963041563 ** ___dest1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MKGlowSystem.MKGlow::PerformBlur(UnityEngine.RenderTexture&,UnityEngine.RenderTexture&,System.Int32)
extern "C"  void MKGlow_PerformBlur_m2211571139 (MKGlow_t3477081437 * __this, RenderTexture_t1963041563 ** ___src0, RenderTexture_t1963041563 ** ___dest1, int32_t ___iteration2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MKGlowSystem.MKGlow::PerformGlow(UnityEngine.RenderTexture&,UnityEngine.RenderTexture&,UnityEngine.RenderTexture&)
extern "C"  void MKGlow_PerformGlow_m1302786496 (MKGlow_t3477081437 * __this, RenderTexture_t1963041563 ** ___glowBuffer0, RenderTexture_t1963041563 ** ___dest1, RenderTexture_t1963041563 ** ___src2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MKGlowSystem.MKGlow::PerformSelectiveGlow(UnityEngine.RenderTexture&,UnityEngine.RenderTexture&)
extern "C"  void MKGlow_PerformSelectiveGlow_m420627462 (MKGlow_t3477081437 * __this, RenderTexture_t1963041563 ** ___source0, RenderTexture_t1963041563 ** ___dest1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MKGlowSystem.MKGlow::PerformFullScreenGlow(UnityEngine.RenderTexture&,UnityEngine.RenderTexture&)
extern "C"  void MKGlow_PerformFullScreenGlow_m873422675 (MKGlow_t3477081437 * __this, RenderTexture_t1963041563 ** ___source0, RenderTexture_t1963041563 ** ___destination1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
