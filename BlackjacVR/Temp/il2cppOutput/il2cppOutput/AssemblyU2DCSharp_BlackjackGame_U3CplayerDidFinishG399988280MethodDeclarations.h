﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BlackjackGame/<playerDidFinishGame>c__Iterator10
struct U3CplayerDidFinishGameU3Ec__Iterator10_t399988280;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void BlackjackGame/<playerDidFinishGame>c__Iterator10::.ctor()
extern "C"  void U3CplayerDidFinishGameU3Ec__Iterator10__ctor_m2561732451 (U3CplayerDidFinishGameU3Ec__Iterator10_t399988280 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object BlackjackGame/<playerDidFinishGame>c__Iterator10::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CplayerDidFinishGameU3Ec__Iterator10_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3168925785 (U3CplayerDidFinishGameU3Ec__Iterator10_t399988280 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object BlackjackGame/<playerDidFinishGame>c__Iterator10::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CplayerDidFinishGameU3Ec__Iterator10_System_Collections_IEnumerator_get_Current_m869912045 (U3CplayerDidFinishGameU3Ec__Iterator10_t399988280 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BlackjackGame/<playerDidFinishGame>c__Iterator10::MoveNext()
extern "C"  bool U3CplayerDidFinishGameU3Ec__Iterator10_MoveNext_m1563439001 (U3CplayerDidFinishGameU3Ec__Iterator10_t399988280 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BlackjackGame/<playerDidFinishGame>c__Iterator10::Dispose()
extern "C"  void U3CplayerDidFinishGameU3Ec__Iterator10_Dispose_m706907872 (U3CplayerDidFinishGameU3Ec__Iterator10_t399988280 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BlackjackGame/<playerDidFinishGame>c__Iterator10::Reset()
extern "C"  void U3CplayerDidFinishGameU3Ec__Iterator10_Reset_m208165392 (U3CplayerDidFinishGameU3Ec__Iterator10_t399988280 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
