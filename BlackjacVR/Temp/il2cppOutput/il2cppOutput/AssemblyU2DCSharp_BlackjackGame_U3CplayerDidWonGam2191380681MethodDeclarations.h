﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BlackjackGame/<playerDidWonGame>c__IteratorC
struct U3CplayerDidWonGameU3Ec__IteratorC_t2191380681;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void BlackjackGame/<playerDidWonGame>c__IteratorC::.ctor()
extern "C"  void U3CplayerDidWonGameU3Ec__IteratorC__ctor_m2339345394 (U3CplayerDidWonGameU3Ec__IteratorC_t2191380681 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object BlackjackGame/<playerDidWonGame>c__IteratorC::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CplayerDidWonGameU3Ec__IteratorC_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3074281898 (U3CplayerDidWonGameU3Ec__IteratorC_t2191380681 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object BlackjackGame/<playerDidWonGame>c__IteratorC::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CplayerDidWonGameU3Ec__IteratorC_System_Collections_IEnumerator_get_Current_m1930193214 (U3CplayerDidWonGameU3Ec__IteratorC_t2191380681 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BlackjackGame/<playerDidWonGame>c__IteratorC::MoveNext()
extern "C"  bool U3CplayerDidWonGameU3Ec__IteratorC_MoveNext_m730367658 (U3CplayerDidWonGameU3Ec__IteratorC_t2191380681 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BlackjackGame/<playerDidWonGame>c__IteratorC::Dispose()
extern "C"  void U3CplayerDidWonGameU3Ec__IteratorC_Dispose_m1741310895 (U3CplayerDidWonGameU3Ec__IteratorC_t2191380681 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BlackjackGame/<playerDidWonGame>c__IteratorC::Reset()
extern "C"  void U3CplayerDidWonGameU3Ec__IteratorC_Reset_m4280745631 (U3CplayerDidWonGameU3Ec__IteratorC_t2191380681 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
