﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<Blackjack.Card>
struct List_1_t1907714746;
// System.Collections.Generic.IEnumerable`1<Blackjack.Card>
struct IEnumerable_1_t3840442151;
// Blackjack.Card[]
struct CardU5BU5D_t1346973999;
// System.Collections.Generic.IEnumerator`1<Blackjack.Card>
struct IEnumerator_1_t2451394243;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<Blackjack.Card>
struct ICollection_1_t1434119181;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Blackjack.Card>
struct ReadOnlyCollection_1_t2096606730;
// System.Predicate`1<Blackjack.Card>
struct Predicate_1_t150586077;
// System.Comparison`1<Blackjack.Card>
struct Comparison_1_t3550857677;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Blackjack_Card539529194.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1927387516.h"

// System.Void System.Collections.Generic.List`1<Blackjack.Card>::.ctor()
extern "C"  void List_1__ctor_m2146757131_gshared (List_1_t1907714746 * __this, const MethodInfo* method);
#define List_1__ctor_m2146757131(__this, method) ((  void (*) (List_1_t1907714746 *, const MethodInfo*))List_1__ctor_m2146757131_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Blackjack.Card>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m1996916773_gshared (List_1_t1907714746 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m1996916773(__this, ___collection0, method) ((  void (*) (List_1_t1907714746 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m1996916773_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Blackjack.Card>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m3070922661_gshared (List_1_t1907714746 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m3070922661(__this, ___capacity0, method) ((  void (*) (List_1_t1907714746 *, int32_t, const MethodInfo*))List_1__ctor_m3070922661_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<Blackjack.Card>::.ctor(T[],System.Int32)
extern "C"  void List_1__ctor_m2195700143_gshared (List_1_t1907714746 * __this, CardU5BU5D_t1346973999* ___data0, int32_t ___size1, const MethodInfo* method);
#define List_1__ctor_m2195700143(__this, ___data0, ___size1, method) ((  void (*) (List_1_t1907714746 *, CardU5BU5D_t1346973999*, int32_t, const MethodInfo*))List_1__ctor_m2195700143_gshared)(__this, ___data0, ___size1, method)
// System.Void System.Collections.Generic.List`1<Blackjack.Card>::.cctor()
extern "C"  void List_1__cctor_m29892057_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m29892057(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m29892057_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Blackjack.Card>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m4149875294_gshared (List_1_t1907714746 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m4149875294(__this, method) ((  Il2CppObject* (*) (List_1_t1907714746 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m4149875294_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Blackjack.Card>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m1847917424_gshared (List_1_t1907714746 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m1847917424(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1907714746 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m1847917424_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Blackjack.Card>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m159362559_gshared (List_1_t1907714746 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m159362559(__this, method) ((  Il2CppObject * (*) (List_1_t1907714746 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m159362559_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Blackjack.Card>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m609781278_gshared (List_1_t1907714746 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m609781278(__this, ___item0, method) ((  int32_t (*) (List_1_t1907714746 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m609781278_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Blackjack.Card>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m2212929122_gshared (List_1_t1907714746 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m2212929122(__this, ___item0, method) ((  bool (*) (List_1_t1907714746 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m2212929122_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Blackjack.Card>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m1233943926_gshared (List_1_t1907714746 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m1233943926(__this, ___item0, method) ((  int32_t (*) (List_1_t1907714746 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m1233943926_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Blackjack.Card>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m1738449641_gshared (List_1_t1907714746 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m1738449641(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1907714746 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m1738449641_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Blackjack.Card>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m133791647_gshared (List_1_t1907714746 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m133791647(__this, ___item0, method) ((  void (*) (List_1_t1907714746 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m133791647_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Blackjack.Card>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m569474019_gshared (List_1_t1907714746 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m569474019(__this, method) ((  bool (*) (List_1_t1907714746 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m569474019_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Blackjack.Card>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m1913423290_gshared (List_1_t1907714746 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m1913423290(__this, method) ((  bool (*) (List_1_t1907714746 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m1913423290_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Blackjack.Card>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m2650226796_gshared (List_1_t1907714746 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m2650226796(__this, method) ((  Il2CppObject * (*) (List_1_t1907714746 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m2650226796_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Blackjack.Card>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m559234897_gshared (List_1_t1907714746 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m559234897(__this, method) ((  bool (*) (List_1_t1907714746 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m559234897_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Blackjack.Card>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m4074279304_gshared (List_1_t1907714746 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m4074279304(__this, method) ((  bool (*) (List_1_t1907714746 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m4074279304_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Blackjack.Card>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m1017956083_gshared (List_1_t1907714746 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m1017956083(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t1907714746 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m1017956083_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Blackjack.Card>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m521779328_gshared (List_1_t1907714746 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m521779328(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1907714746 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m521779328_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<Blackjack.Card>::Add(T)
extern "C"  void List_1_Add_m1841117947_gshared (List_1_t1907714746 * __this, Card_t539529194  ___item0, const MethodInfo* method);
#define List_1_Add_m1841117947(__this, ___item0, method) ((  void (*) (List_1_t1907714746 *, Card_t539529194 , const MethodInfo*))List_1_Add_m1841117947_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Blackjack.Card>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m558405734_gshared (List_1_t1907714746 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m558405734(__this, ___newCount0, method) ((  void (*) (List_1_t1907714746 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m558405734_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<Blackjack.Card>::CheckRange(System.Int32,System.Int32)
extern "C"  void List_1_CheckRange_m488271809_gshared (List_1_t1907714746 * __this, int32_t ___idx0, int32_t ___count1, const MethodInfo* method);
#define List_1_CheckRange_m488271809(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t1907714746 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m488271809_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<Blackjack.Card>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m1775097700_gshared (List_1_t1907714746 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m1775097700(__this, ___collection0, method) ((  void (*) (List_1_t1907714746 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m1775097700_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Blackjack.Card>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m1036069924_gshared (List_1_t1907714746 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m1036069924(__this, ___enumerable0, method) ((  void (*) (List_1_t1907714746 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m1036069924_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<Blackjack.Card>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m2222308339_gshared (List_1_t1907714746 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m2222308339(__this, ___collection0, method) ((  void (*) (List_1_t1907714746 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m2222308339_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Blackjack.Card>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t2096606730 * List_1_AsReadOnly_m2994942770_gshared (List_1_t1907714746 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m2994942770(__this, method) ((  ReadOnlyCollection_1_t2096606730 * (*) (List_1_t1907714746 *, const MethodInfo*))List_1_AsReadOnly_m2994942770_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Blackjack.Card>::Clear()
extern "C"  void List_1_Clear_m3241636991_gshared (List_1_t1907714746 * __this, const MethodInfo* method);
#define List_1_Clear_m3241636991(__this, method) ((  void (*) (List_1_t1907714746 *, const MethodInfo*))List_1_Clear_m3241636991_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Blackjack.Card>::Contains(T)
extern "C"  bool List_1_Contains_m743383409_gshared (List_1_t1907714746 * __this, Card_t539529194  ___item0, const MethodInfo* method);
#define List_1_Contains_m743383409(__this, ___item0, method) ((  bool (*) (List_1_t1907714746 *, Card_t539529194 , const MethodInfo*))List_1_Contains_m743383409_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Blackjack.Card>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m4006994139_gshared (List_1_t1907714746 * __this, CardU5BU5D_t1346973999* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m4006994139(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1907714746 *, CardU5BU5D_t1346973999*, int32_t, const MethodInfo*))List_1_CopyTo_m4006994139_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<Blackjack.Card>::Find(System.Predicate`1<T>)
extern "C"  Card_t539529194  List_1_Find_m1977201163_gshared (List_1_t1907714746 * __this, Predicate_1_t150586077 * ___match0, const MethodInfo* method);
#define List_1_Find_m1977201163(__this, ___match0, method) ((  Card_t539529194  (*) (List_1_t1907714746 *, Predicate_1_t150586077 *, const MethodInfo*))List_1_Find_m1977201163_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Blackjack.Card>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m2631664744_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t150586077 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m2631664744(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t150586077 *, const MethodInfo*))List_1_CheckMatch_m2631664744_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<Blackjack.Card>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m4119991109_gshared (List_1_t1907714746 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t150586077 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m4119991109(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t1907714746 *, int32_t, int32_t, Predicate_1_t150586077 *, const MethodInfo*))List_1_GetIndex_m4119991109_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Blackjack.Card>::GetEnumerator()
extern "C"  Enumerator_t1927387516  List_1_GetEnumerator_m2432129368_gshared (List_1_t1907714746 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m2432129368(__this, method) ((  Enumerator_t1927387516  (*) (List_1_t1907714746 *, const MethodInfo*))List_1_GetEnumerator_m2432129368_gshared)(__this, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<Blackjack.Card>::GetRange(System.Int32,System.Int32)
extern "C"  List_1_t1907714746 * List_1_GetRange_m4218825992_gshared (List_1_t1907714746 * __this, int32_t ___index0, int32_t ___count1, const MethodInfo* method);
#define List_1_GetRange_m4218825992(__this, ___index0, ___count1, method) ((  List_1_t1907714746 * (*) (List_1_t1907714746 *, int32_t, int32_t, const MethodInfo*))List_1_GetRange_m4218825992_gshared)(__this, ___index0, ___count1, method)
// System.Int32 System.Collections.Generic.List`1<Blackjack.Card>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m1802709223_gshared (List_1_t1907714746 * __this, Card_t539529194  ___item0, const MethodInfo* method);
#define List_1_IndexOf_m1802709223(__this, ___item0, method) ((  int32_t (*) (List_1_t1907714746 *, Card_t539529194 , const MethodInfo*))List_1_IndexOf_m1802709223_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Blackjack.Card>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m1842916402_gshared (List_1_t1907714746 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m1842916402(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t1907714746 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m1842916402_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<Blackjack.Card>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m3753361323_gshared (List_1_t1907714746 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m3753361323(__this, ___index0, method) ((  void (*) (List_1_t1907714746 *, int32_t, const MethodInfo*))List_1_CheckIndex_m3753361323_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Blackjack.Card>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m178024978_gshared (List_1_t1907714746 * __this, int32_t ___index0, Card_t539529194  ___item1, const MethodInfo* method);
#define List_1_Insert_m178024978(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1907714746 *, int32_t, Card_t539529194 , const MethodInfo*))List_1_Insert_m178024978_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Blackjack.Card>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m1293541639_gshared (List_1_t1907714746 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m1293541639(__this, ___collection0, method) ((  void (*) (List_1_t1907714746 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m1293541639_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<Blackjack.Card>::Remove(T)
extern "C"  bool List_1_Remove_m286230956_gshared (List_1_t1907714746 * __this, Card_t539529194  ___item0, const MethodInfo* method);
#define List_1_Remove_m286230956(__this, ___item0, method) ((  bool (*) (List_1_t1907714746 *, Card_t539529194 , const MethodInfo*))List_1_Remove_m286230956_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Blackjack.Card>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m4076943722_gshared (List_1_t1907714746 * __this, Predicate_1_t150586077 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m4076943722(__this, ___match0, method) ((  int32_t (*) (List_1_t1907714746 *, Predicate_1_t150586077 *, const MethodInfo*))List_1_RemoveAll_m4076943722_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Blackjack.Card>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m2319273281_gshared (List_1_t1907714746 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m2319273281(__this, ___index0, method) ((  void (*) (List_1_t1907714746 *, int32_t, const MethodInfo*))List_1_RemoveAt_m2319273281_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Blackjack.Card>::Reverse()
extern "C"  void List_1_Reverse_m3024545684_gshared (List_1_t1907714746 * __this, const MethodInfo* method);
#define List_1_Reverse_m3024545684(__this, method) ((  void (*) (List_1_t1907714746 *, const MethodInfo*))List_1_Reverse_m3024545684_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Blackjack.Card>::Sort()
extern "C"  void List_1_Sort_m704356494_gshared (List_1_t1907714746 * __this, const MethodInfo* method);
#define List_1_Sort_m704356494(__this, method) ((  void (*) (List_1_t1907714746 *, const MethodInfo*))List_1_Sort_m704356494_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Blackjack.Card>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m2089594465_gshared (List_1_t1907714746 * __this, Comparison_1_t3550857677 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m2089594465(__this, ___comparison0, method) ((  void (*) (List_1_t1907714746 *, Comparison_1_t3550857677 *, const MethodInfo*))List_1_Sort_m2089594465_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<Blackjack.Card>::ToArray()
extern "C"  CardU5BU5D_t1346973999* List_1_ToArray_m2831875181_gshared (List_1_t1907714746 * __this, const MethodInfo* method);
#define List_1_ToArray_m2831875181(__this, method) ((  CardU5BU5D_t1346973999* (*) (List_1_t1907714746 *, const MethodInfo*))List_1_ToArray_m2831875181_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Blackjack.Card>::TrimExcess()
extern "C"  void List_1_TrimExcess_m2796002727_gshared (List_1_t1907714746 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m2796002727(__this, method) ((  void (*) (List_1_t1907714746 *, const MethodInfo*))List_1_TrimExcess_m2796002727_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Blackjack.Card>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m2591446423_gshared (List_1_t1907714746 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m2591446423(__this, method) ((  int32_t (*) (List_1_t1907714746 *, const MethodInfo*))List_1_get_Capacity_m2591446423_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Blackjack.Card>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m2063561848_gshared (List_1_t1907714746 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m2063561848(__this, ___value0, method) ((  void (*) (List_1_t1907714746 *, int32_t, const MethodInfo*))List_1_set_Capacity_m2063561848_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<Blackjack.Card>::get_Count()
extern "C"  int32_t List_1_get_Count_m4161685876_gshared (List_1_t1907714746 * __this, const MethodInfo* method);
#define List_1_get_Count_m4161685876(__this, method) ((  int32_t (*) (List_1_t1907714746 *, const MethodInfo*))List_1_get_Count_m4161685876_gshared)(__this, method)
// T System.Collections.Generic.List`1<Blackjack.Card>::get_Item(System.Int32)
extern "C"  Card_t539529194  List_1_get_Item_m3216046232_gshared (List_1_t1907714746 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m3216046232(__this, ___index0, method) ((  Card_t539529194  (*) (List_1_t1907714746 *, int32_t, const MethodInfo*))List_1_get_Item_m3216046232_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Blackjack.Card>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m1092908649_gshared (List_1_t1907714746 * __this, int32_t ___index0, Card_t539529194  ___value1, const MethodInfo* method);
#define List_1_set_Item_m1092908649(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1907714746 *, int32_t, Card_t539529194 , const MethodInfo*))List_1_set_Item_m1092908649_gshared)(__this, ___index0, ___value1, method)
