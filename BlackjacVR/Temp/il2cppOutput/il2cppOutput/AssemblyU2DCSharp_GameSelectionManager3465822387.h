﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// BlackjackGame
struct BlackjackGame_t1439740240;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSelectionManager
struct  GameSelectionManager_t3465822387  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GameObject GameSelectionManager::allButtons
	GameObject_t3674682005 * ___allButtons_2;
	// BlackjackGame GameSelectionManager::currentGame
	BlackjackGame_t1439740240 * ___currentGame_3;

public:
	inline static int32_t get_offset_of_allButtons_2() { return static_cast<int32_t>(offsetof(GameSelectionManager_t3465822387, ___allButtons_2)); }
	inline GameObject_t3674682005 * get_allButtons_2() const { return ___allButtons_2; }
	inline GameObject_t3674682005 ** get_address_of_allButtons_2() { return &___allButtons_2; }
	inline void set_allButtons_2(GameObject_t3674682005 * value)
	{
		___allButtons_2 = value;
		Il2CppCodeGenWriteBarrier(&___allButtons_2, value);
	}

	inline static int32_t get_offset_of_currentGame_3() { return static_cast<int32_t>(offsetof(GameSelectionManager_t3465822387, ___currentGame_3)); }
	inline BlackjackGame_t1439740240 * get_currentGame_3() const { return ___currentGame_3; }
	inline BlackjackGame_t1439740240 ** get_address_of_currentGame_3() { return &___currentGame_3; }
	inline void set_currentGame_3(BlackjackGame_t1439740240 * value)
	{
		___currentGame_3 = value;
		Il2CppCodeGenWriteBarrier(&___currentGame_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
