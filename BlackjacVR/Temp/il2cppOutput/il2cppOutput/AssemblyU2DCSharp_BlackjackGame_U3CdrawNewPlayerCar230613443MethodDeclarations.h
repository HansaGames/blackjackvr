﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BlackjackGame/<drawNewPlayerCards>c__Iterator9
struct U3CdrawNewPlayerCardsU3Ec__Iterator9_t230613443;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void BlackjackGame/<drawNewPlayerCards>c__Iterator9::.ctor()
extern "C"  void U3CdrawNewPlayerCardsU3Ec__Iterator9__ctor_m1017042488 (U3CdrawNewPlayerCardsU3Ec__Iterator9_t230613443 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object BlackjackGame/<drawNewPlayerCards>c__Iterator9::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CdrawNewPlayerCardsU3Ec__Iterator9_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2791417316 (U3CdrawNewPlayerCardsU3Ec__Iterator9_t230613443 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object BlackjackGame/<drawNewPlayerCards>c__Iterator9::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CdrawNewPlayerCardsU3Ec__Iterator9_System_Collections_IEnumerator_get_Current_m734261624 (U3CdrawNewPlayerCardsU3Ec__Iterator9_t230613443 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BlackjackGame/<drawNewPlayerCards>c__Iterator9::MoveNext()
extern "C"  bool U3CdrawNewPlayerCardsU3Ec__Iterator9_MoveNext_m2777938340 (U3CdrawNewPlayerCardsU3Ec__Iterator9_t230613443 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BlackjackGame/<drawNewPlayerCards>c__Iterator9::Dispose()
extern "C"  void U3CdrawNewPlayerCardsU3Ec__Iterator9_Dispose_m2318537845 (U3CdrawNewPlayerCardsU3Ec__Iterator9_t230613443 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BlackjackGame/<drawNewPlayerCards>c__Iterator9::Reset()
extern "C"  void U3CdrawNewPlayerCardsU3Ec__Iterator9_Reset_m2958442725 (U3CdrawNewPlayerCardsU3Ec__Iterator9_t230613443 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
