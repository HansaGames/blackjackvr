﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BlackjackGame/<endGame>c__IteratorF
struct U3CendGameU3Ec__IteratorF_t2718692611;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void BlackjackGame/<endGame>c__IteratorF::.ctor()
extern "C"  void U3CendGameU3Ec__IteratorF__ctor_m674185672 (U3CendGameU3Ec__IteratorF_t2718692611 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object BlackjackGame/<endGame>c__IteratorF::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CendGameU3Ec__IteratorF_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4215312650 (U3CendGameU3Ec__IteratorF_t2718692611 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object BlackjackGame/<endGame>c__IteratorF::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CendGameU3Ec__IteratorF_System_Collections_IEnumerator_get_Current_m741992606 (U3CendGameU3Ec__IteratorF_t2718692611 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BlackjackGame/<endGame>c__IteratorF::MoveNext()
extern "C"  bool U3CendGameU3Ec__IteratorF_MoveNext_m1701765420 (U3CendGameU3Ec__IteratorF_t2718692611 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BlackjackGame/<endGame>c__IteratorF::Dispose()
extern "C"  void U3CendGameU3Ec__IteratorF_Dispose_m3545619461 (U3CendGameU3Ec__IteratorF_t2718692611 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BlackjackGame/<endGame>c__IteratorF::Reset()
extern "C"  void U3CendGameU3Ec__IteratorF_Reset_m2615585909 (U3CendGameU3Ec__IteratorF_t2718692611 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
