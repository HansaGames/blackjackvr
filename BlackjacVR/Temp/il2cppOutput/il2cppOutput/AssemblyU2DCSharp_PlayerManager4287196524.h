﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Blackjack.Deck
struct Deck_t539562371;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "AssemblyU2DCSharp_Singleton_1_gen245044621.h"
#include "AssemblyU2DCSharp_PlayerState2213905936.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerManager
struct  PlayerManager_t4287196524  : public Singleton_1_t245044621
{
public:
	// System.Int32 PlayerManager::playerMoney
	int32_t ___playerMoney_5;
	// Blackjack.Deck PlayerManager::deck
	Deck_t539562371 * ___deck_6;
	// PlayerState PlayerManager::state
	int32_t ___state_7;
	// UnityEngine.GameObject PlayerManager::blackJackManager
	GameObject_t3674682005 * ___blackJackManager_8;
	// UnityEngine.GameObject PlayerManager::player
	GameObject_t3674682005 * ___player_9;
	// UnityEngine.GameObject PlayerManager::mainDealer
	GameObject_t3674682005 * ___mainDealer_10;
	// UnityEngine.GameObject PlayerManager::playerScoreText
	GameObject_t3674682005 * ___playerScoreText_11;

public:
	inline static int32_t get_offset_of_playerMoney_5() { return static_cast<int32_t>(offsetof(PlayerManager_t4287196524, ___playerMoney_5)); }
	inline int32_t get_playerMoney_5() const { return ___playerMoney_5; }
	inline int32_t* get_address_of_playerMoney_5() { return &___playerMoney_5; }
	inline void set_playerMoney_5(int32_t value)
	{
		___playerMoney_5 = value;
	}

	inline static int32_t get_offset_of_deck_6() { return static_cast<int32_t>(offsetof(PlayerManager_t4287196524, ___deck_6)); }
	inline Deck_t539562371 * get_deck_6() const { return ___deck_6; }
	inline Deck_t539562371 ** get_address_of_deck_6() { return &___deck_6; }
	inline void set_deck_6(Deck_t539562371 * value)
	{
		___deck_6 = value;
		Il2CppCodeGenWriteBarrier(&___deck_6, value);
	}

	inline static int32_t get_offset_of_state_7() { return static_cast<int32_t>(offsetof(PlayerManager_t4287196524, ___state_7)); }
	inline int32_t get_state_7() const { return ___state_7; }
	inline int32_t* get_address_of_state_7() { return &___state_7; }
	inline void set_state_7(int32_t value)
	{
		___state_7 = value;
	}

	inline static int32_t get_offset_of_blackJackManager_8() { return static_cast<int32_t>(offsetof(PlayerManager_t4287196524, ___blackJackManager_8)); }
	inline GameObject_t3674682005 * get_blackJackManager_8() const { return ___blackJackManager_8; }
	inline GameObject_t3674682005 ** get_address_of_blackJackManager_8() { return &___blackJackManager_8; }
	inline void set_blackJackManager_8(GameObject_t3674682005 * value)
	{
		___blackJackManager_8 = value;
		Il2CppCodeGenWriteBarrier(&___blackJackManager_8, value);
	}

	inline static int32_t get_offset_of_player_9() { return static_cast<int32_t>(offsetof(PlayerManager_t4287196524, ___player_9)); }
	inline GameObject_t3674682005 * get_player_9() const { return ___player_9; }
	inline GameObject_t3674682005 ** get_address_of_player_9() { return &___player_9; }
	inline void set_player_9(GameObject_t3674682005 * value)
	{
		___player_9 = value;
		Il2CppCodeGenWriteBarrier(&___player_9, value);
	}

	inline static int32_t get_offset_of_mainDealer_10() { return static_cast<int32_t>(offsetof(PlayerManager_t4287196524, ___mainDealer_10)); }
	inline GameObject_t3674682005 * get_mainDealer_10() const { return ___mainDealer_10; }
	inline GameObject_t3674682005 ** get_address_of_mainDealer_10() { return &___mainDealer_10; }
	inline void set_mainDealer_10(GameObject_t3674682005 * value)
	{
		___mainDealer_10 = value;
		Il2CppCodeGenWriteBarrier(&___mainDealer_10, value);
	}

	inline static int32_t get_offset_of_playerScoreText_11() { return static_cast<int32_t>(offsetof(PlayerManager_t4287196524, ___playerScoreText_11)); }
	inline GameObject_t3674682005 * get_playerScoreText_11() const { return ___playerScoreText_11; }
	inline GameObject_t3674682005 ** get_address_of_playerScoreText_11() { return &___playerScoreText_11; }
	inline void set_playerScoreText_11(GameObject_t3674682005 * value)
	{
		___playerScoreText_11 = value;
		Il2CppCodeGenWriteBarrier(&___playerScoreText_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
