﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<Blackjack.Card>
struct DefaultComparer_t3342742267;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Blackjack_Card539529194.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<Blackjack.Card>::.ctor()
extern "C"  void DefaultComparer__ctor_m4222519782_gshared (DefaultComparer_t3342742267 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m4222519782(__this, method) ((  void (*) (DefaultComparer_t3342742267 *, const MethodInfo*))DefaultComparer__ctor_m4222519782_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<Blackjack.Card>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m3448875857_gshared (DefaultComparer_t3342742267 * __this, Card_t539529194  ___x0, Card_t539529194  ___y1, const MethodInfo* method);
#define DefaultComparer_Compare_m3448875857(__this, ___x0, ___y1, method) ((  int32_t (*) (DefaultComparer_t3342742267 *, Card_t539529194 , Card_t539529194 , const MethodInfo*))DefaultComparer_Compare_m3448875857_gshared)(__this, ___x0, ___y1, method)
