﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Object
struct Il2CppObject;
// BlackjackGame
struct BlackjackGame_t1439740240;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BlackjackGame/<showDealerCards>c__IteratorA
struct  U3CshowDealerCardsU3Ec__IteratorA_t1840808414  : public Il2CppObject
{
public:
	// UnityEngine.GameObject BlackjackGame/<showDealerCards>c__IteratorA::<cardGO>__0
	GameObject_t3674682005 * ___U3CcardGOU3E__0_0;
	// System.Int32 BlackjackGame/<showDealerCards>c__IteratorA::$PC
	int32_t ___U24PC_1;
	// System.Object BlackjackGame/<showDealerCards>c__IteratorA::$current
	Il2CppObject * ___U24current_2;
	// BlackjackGame BlackjackGame/<showDealerCards>c__IteratorA::<>f__this
	BlackjackGame_t1439740240 * ___U3CU3Ef__this_3;

public:
	inline static int32_t get_offset_of_U3CcardGOU3E__0_0() { return static_cast<int32_t>(offsetof(U3CshowDealerCardsU3Ec__IteratorA_t1840808414, ___U3CcardGOU3E__0_0)); }
	inline GameObject_t3674682005 * get_U3CcardGOU3E__0_0() const { return ___U3CcardGOU3E__0_0; }
	inline GameObject_t3674682005 ** get_address_of_U3CcardGOU3E__0_0() { return &___U3CcardGOU3E__0_0; }
	inline void set_U3CcardGOU3E__0_0(GameObject_t3674682005 * value)
	{
		___U3CcardGOU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcardGOU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U24PC_1() { return static_cast<int32_t>(offsetof(U3CshowDealerCardsU3Ec__IteratorA_t1840808414, ___U24PC_1)); }
	inline int32_t get_U24PC_1() const { return ___U24PC_1; }
	inline int32_t* get_address_of_U24PC_1() { return &___U24PC_1; }
	inline void set_U24PC_1(int32_t value)
	{
		___U24PC_1 = value;
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CshowDealerCardsU3Ec__IteratorA_t1840808414, ___U24current_2)); }
	inline Il2CppObject * get_U24current_2() const { return ___U24current_2; }
	inline Il2CppObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(Il2CppObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_3() { return static_cast<int32_t>(offsetof(U3CshowDealerCardsU3Ec__IteratorA_t1840808414, ___U3CU3Ef__this_3)); }
	inline BlackjackGame_t1439740240 * get_U3CU3Ef__this_3() const { return ___U3CU3Ef__this_3; }
	inline BlackjackGame_t1439740240 ** get_address_of_U3CU3Ef__this_3() { return &___U3CU3Ef__this_3; }
	inline void set_U3CU3Ef__this_3(BlackjackGame_t1439740240 * value)
	{
		___U3CU3Ef__this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
