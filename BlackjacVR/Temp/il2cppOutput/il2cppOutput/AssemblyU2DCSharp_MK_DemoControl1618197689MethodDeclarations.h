﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MK_DemoControl
struct MK_DemoControl_t1618197689;

#include "codegen/il2cpp-codegen.h"

// System.Void MK_DemoControl::.ctor()
extern "C"  void MK_DemoControl__ctor_m1328041986 (MK_DemoControl_t1618197689 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MK_DemoControl::Awake()
extern "C"  void MK_DemoControl_Awake_m1565647205 (MK_DemoControl_t1618197689 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MK_DemoControl::Update()
extern "C"  void MK_DemoControl_Update_m4241457995 (MK_DemoControl_t1618197689 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MK_DemoControl::InitRoom1()
extern "C"  void MK_DemoControl_InitRoom1_m2663756710 (MK_DemoControl_t1618197689 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MK_DemoControl::InitRoom2()
extern "C"  void MK_DemoControl_InitRoom2_m2663757671 (MK_DemoControl_t1618197689 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MK_DemoControl::InitGlowSystem()
extern "C"  void MK_DemoControl_InitGlowSystem_m671893518 (MK_DemoControl_t1618197689 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MK_DemoControl::CreateMatrix(System.Int32,System.Single,System.Single)
extern "C"  void MK_DemoControl_CreateMatrix_m1494126714 (MK_DemoControl_t1618197689 * __this, int32_t ___depth0, float ___nativeWidth1, float ___nativeHeight2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MK_DemoControl::OnGUI()
extern "C"  void MK_DemoControl_OnGUI_m823440636 (MK_DemoControl_t1618197689 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MK_DemoControl::ManageRoom()
extern "C"  void MK_DemoControl_ManageRoom_m3500961826 (MK_DemoControl_t1618197689 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MK_DemoControl::SwitchRoom()
extern "C"  void MK_DemoControl_SwitchRoom_m89174705 (MK_DemoControl_t1618197689 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MK_DemoControl::ManageRoom0()
extern "C"  void MK_DemoControl_ManageRoom0_m1155641904 (MK_DemoControl_t1618197689 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MK_DemoControl::ManageRoom1()
extern "C"  void MK_DemoControl_ManageRoom1_m1155642865 (MK_DemoControl_t1618197689 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MK_DemoControl::ManageRoom2()
extern "C"  void MK_DemoControl_ManageRoom2_m1155643826 (MK_DemoControl_t1618197689 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
