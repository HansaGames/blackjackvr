﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GvrVideoPlayerTexture/<InternalOnExceptionCallback>c__AnonStorey18
struct U3CInternalOnExceptionCallbackU3Ec__AnonStorey18_t2058796985;

#include "codegen/il2cpp-codegen.h"

// System.Void GvrVideoPlayerTexture/<InternalOnExceptionCallback>c__AnonStorey18::.ctor()
extern "C"  void U3CInternalOnExceptionCallbackU3Ec__AnonStorey18__ctor_m1371492610 (U3CInternalOnExceptionCallbackU3Ec__AnonStorey18_t2058796985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GvrVideoPlayerTexture/<InternalOnExceptionCallback>c__AnonStorey18::<>m__B()
extern "C"  void U3CInternalOnExceptionCallbackU3Ec__AnonStorey18_U3CU3Em__B_m3797486905 (U3CInternalOnExceptionCallbackU3Ec__AnonStorey18_t2058796985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
