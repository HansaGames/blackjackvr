﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Blackjack.Deck
struct Deck_t539562371;
// System.Collections.Generic.List`1<Blackjack.Card>
struct List_1_t1907714746;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Blackjack_Card539529194.h"

// System.Void Blackjack.Deck::.ctor()
extern "C"  void Deck__ctor_m4275070018 (Deck_t539562371 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Blackjack.Card> Blackjack.Deck::createDeck()
extern "C"  List_1_t1907714746 * Deck_createDeck_m933783514 (Deck_t539562371 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Blackjack.Card> Blackjack.Deck::crateScenario1()
extern "C"  List_1_t1907714746 * Deck_crateScenario1_m2557804019 (Deck_t539562371 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Blackjack.Card> Blackjack.Deck::crateScenario2()
extern "C"  List_1_t1907714746 * Deck_crateScenario2_m2557804980 (Deck_t539562371 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Blackjack.Card> Blackjack.Deck::crateScenario3()
extern "C"  List_1_t1907714746 * Deck_crateScenario3_m2557805941 (Deck_t539562371 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Blackjack.Card> Blackjack.Deck::crateScenario4()
extern "C"  List_1_t1907714746 * Deck_crateScenario4_m2557806902 (Deck_t539562371 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Blackjack.Card> Blackjack.Deck::crateScenario5()
extern "C"  List_1_t1907714746 * Deck_crateScenario5_m2557807863 (Deck_t539562371 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Blackjack.Card> Blackjack.Deck::crateScenario6()
extern "C"  List_1_t1907714746 * Deck_crateScenario6_m2557808824 (Deck_t539562371 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Blackjack.Card> Blackjack.Deck::crateScenario7()
extern "C"  List_1_t1907714746 * Deck_crateScenario7_m2557809785 (Deck_t539562371 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Blackjack.Card> Blackjack.Deck::crateScenario8()
extern "C"  List_1_t1907714746 * Deck_crateScenario8_m2557810746 (Deck_t539562371 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Blackjack.Card> Blackjack.Deck::crateScenario9()
extern "C"  List_1_t1907714746 * Deck_crateScenario9_m2557811707 (Deck_t539562371 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Blackjack.Card Blackjack.Deck::<createDeck>m__C(Blackjack.Card)
extern "C"  Card_t539529194  Deck_U3CcreateDeckU3Em__C_m3876201554 (Il2CppObject * __this /* static, unused */, Card_t539529194  ___card0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
