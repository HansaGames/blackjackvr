﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GvrBasePointer
struct GvrBasePointer_t3602508009;
// UnityEngine.Transform
struct Transform_t1659122786;

#include "codegen/il2cpp-codegen.h"

// System.Void GvrBasePointer::.ctor()
extern "C"  void GvrBasePointer__ctor_m4107852242 (GvrBasePointer_t3602508009 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GvrBasePointer::Start()
extern "C"  void GvrBasePointer_Start_m3054990034 (GvrBasePointer_t3602508009 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GvrBasePointer::get_ShouldUseExitRadiusForRaycast()
extern "C"  bool GvrBasePointer_get_ShouldUseExitRadiusForRaycast_m2708271895 (GvrBasePointer_t3602508009 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GvrBasePointer::set_ShouldUseExitRadiusForRaycast(System.Boolean)
extern "C"  void GvrBasePointer_set_ShouldUseExitRadiusForRaycast_m1030792334 (GvrBasePointer_t3602508009 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform GvrBasePointer::GetPointerTransform()
extern "C"  Transform_t1659122786 * GvrBasePointer_GetPointerTransform_m857517815 (GvrBasePointer_t3602508009 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
