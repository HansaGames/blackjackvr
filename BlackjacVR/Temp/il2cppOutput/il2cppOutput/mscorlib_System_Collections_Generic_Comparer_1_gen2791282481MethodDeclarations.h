﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1<Blackjack.Card>
struct Comparer_1_t2791282481;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.Generic.Comparer`1<Blackjack.Card>::.ctor()
extern "C"  void Comparer_1__ctor_m2704823909_gshared (Comparer_1_t2791282481 * __this, const MethodInfo* method);
#define Comparer_1__ctor_m2704823909(__this, method) ((  void (*) (Comparer_1_t2791282481 *, const MethodInfo*))Comparer_1__ctor_m2704823909_gshared)(__this, method)
// System.Void System.Collections.Generic.Comparer`1<Blackjack.Card>::.cctor()
extern "C"  void Comparer_1__cctor_m1763066344_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1__cctor_m1763066344(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Comparer_1__cctor_m1763066344_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.Comparer`1<Blackjack.Card>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m908548426_gshared (Comparer_1_t2791282481 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
#define Comparer_1_System_Collections_IComparer_Compare_m908548426(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparer_1_t2791282481 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Comparer_1_System_Collections_IComparer_Compare_m908548426_gshared)(__this, ___x0, ___y1, method)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<Blackjack.Card>::get_Default()
extern "C"  Comparer_1_t2791282481 * Comparer_1_get_Default_m562959437_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1_get_Default_m562959437(__this /* static, unused */, method) ((  Comparer_1_t2791282481 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Comparer_1_get_Default_m562959437_gshared)(__this /* static, unused */, method)
