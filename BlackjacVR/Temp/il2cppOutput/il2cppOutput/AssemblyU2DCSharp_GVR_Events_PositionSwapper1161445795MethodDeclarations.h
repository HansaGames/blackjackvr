﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GVR.Events.PositionSwapper
struct PositionSwapper_t1161445795;

#include "codegen/il2cpp-codegen.h"

// System.Void GVR.Events.PositionSwapper::.ctor()
extern "C"  void PositionSwapper__ctor_m2020373252 (PositionSwapper_t1161445795 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GVR.Events.PositionSwapper::SetConstraint(System.Int32)
extern "C"  void PositionSwapper_SetConstraint_m3521356562 (PositionSwapper_t1161445795 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GVR.Events.PositionSwapper::SetPosition(System.Int32)
extern "C"  void PositionSwapper_SetPosition_m2256657950 (PositionSwapper_t1161445795 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
