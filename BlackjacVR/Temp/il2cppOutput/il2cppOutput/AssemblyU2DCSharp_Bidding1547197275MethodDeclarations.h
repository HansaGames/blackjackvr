﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Bidding
struct Bidding_t1547197275;

#include "codegen/il2cpp-codegen.h"

// System.Void Bidding::.ctor()
extern "C"  void Bidding__ctor_m2252499568 (Bidding_t1547197275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Bidding::Start()
extern "C"  void Bidding_Start_m1199637360 (Bidding_t1547197275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Bidding::HandleTimedInput()
extern "C"  void Bidding_HandleTimedInput_m1291172015 (Bidding_t1547197275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Bidding::Update()
extern "C"  void Bidding_Update_m2834871965 (Bidding_t1547197275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Bidding::startBidding()
extern "C"  void Bidding_startBidding_m686809133 (Bidding_t1547197275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Bidding::makeBide(System.Int32)
extern "C"  void Bidding_makeBide_m2075350555 (Bidding_t1547197275 * __this, int32_t ___newBid0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Bidding::clearBid()
extern "C"  void Bidding_clearBid_m3141577380 (Bidding_t1547197275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Bidding::dealCards()
extern "C"  void Bidding_dealCards_m947109317 (Bidding_t1547197275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Bidding::checkIfPossibleMakeBid(System.Int32)
extern "C"  bool Bidding_checkIfPossibleMakeBid_m3553267146 (Bidding_t1547197275 * __this, int32_t ___bid0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Bidding::updateBidText()
extern "C"  void Bidding_updateBidText_m482384175 (Bidding_t1547197275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
