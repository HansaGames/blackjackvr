﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_ExecuteEven864200549MethodDeclarations.h"

// System.Void UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<TimingScript>::.ctor(System.Object,System.IntPtr)
#define EventFunction_1__ctor_m576472937(__this, ___object0, ___method1, method) ((  void (*) (EventFunction_1_t621096391 *, Il2CppObject *, IntPtr_t, const MethodInfo*))EventFunction_1__ctor_m252996987_gshared)(__this, ___object0, ___method1, method)
// System.Void UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<TimingScript>::Invoke(T1,UnityEngine.EventSystems.BaseEventData)
#define EventFunction_1_Invoke_m546631095(__this, ___handler0, ___eventData1, method) ((  void (*) (EventFunction_1_t621096391 *, Il2CppObject *, BaseEventData_t2054899105 *, const MethodInfo*))EventFunction_1_Invoke_m1750245256_gshared)(__this, ___handler0, ___eventData1, method)
// System.IAsyncResult UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<TimingScript>::BeginInvoke(T1,UnityEngine.EventSystems.BaseEventData,System.AsyncCallback,System.Object)
#define EventFunction_1_BeginInvoke_m248019942(__this, ___handler0, ___eventData1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (EventFunction_1_t621096391 *, Il2CppObject *, BaseEventData_t2054899105 *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))EventFunction_1_BeginInvoke_m2337669695_gshared)(__this, ___handler0, ___eventData1, ___callback2, ___object3, method)
// System.Void UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<TimingScript>::EndInvoke(System.IAsyncResult)
#define EventFunction_1_EndInvoke_m4074327868(__this, ___result0, method) ((  void (*) (EventFunction_1_t621096391 *, Il2CppObject *, const MethodInfo*))EventFunction_1_EndInvoke_m1184355595_gshared)(__this, ___result0, method)
