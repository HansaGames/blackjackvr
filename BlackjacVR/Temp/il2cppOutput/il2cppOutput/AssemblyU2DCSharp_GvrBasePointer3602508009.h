﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrBasePointer
struct  GvrBasePointer_t3602508009  : public MonoBehaviour_t667441552
{
public:
	// System.Boolean GvrBasePointer::<ShouldUseExitRadiusForRaycast>k__BackingField
	bool ___U3CShouldUseExitRadiusForRaycastU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CShouldUseExitRadiusForRaycastU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(GvrBasePointer_t3602508009, ___U3CShouldUseExitRadiusForRaycastU3Ek__BackingField_2)); }
	inline bool get_U3CShouldUseExitRadiusForRaycastU3Ek__BackingField_2() const { return ___U3CShouldUseExitRadiusForRaycastU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CShouldUseExitRadiusForRaycastU3Ek__BackingField_2() { return &___U3CShouldUseExitRadiusForRaycastU3Ek__BackingField_2; }
	inline void set_U3CShouldUseExitRadiusForRaycastU3Ek__BackingField_2(bool value)
	{
		___U3CShouldUseExitRadiusForRaycastU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
