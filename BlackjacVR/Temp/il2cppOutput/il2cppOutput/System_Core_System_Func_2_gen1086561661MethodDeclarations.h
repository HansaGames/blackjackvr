﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Func`2<Blackjack.Card,Blackjack.Card>
struct Func_2_t1086561661;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_Blackjack_Card539529194.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void System.Func`2<Blackjack.Card,Blackjack.Card>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_2__ctor_m52151584_gshared (Func_2_t1086561661 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Func_2__ctor_m52151584(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t1086561661 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m52151584_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<Blackjack.Card,Blackjack.Card>::Invoke(T)
extern "C"  Card_t539529194  Func_2_Invoke_m3971610674_gshared (Func_2_t1086561661 * __this, Card_t539529194  ___arg10, const MethodInfo* method);
#define Func_2_Invoke_m3971610674(__this, ___arg10, method) ((  Card_t539529194  (*) (Func_2_t1086561661 *, Card_t539529194 , const MethodInfo*))Func_2_Invoke_m3971610674_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<Blackjack.Card,Blackjack.Card>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Func_2_BeginInvoke_m407525089_gshared (Func_2_t1086561661 * __this, Card_t539529194  ___arg10, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method);
#define Func_2_BeginInvoke_m407525089(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t1086561661 *, Card_t539529194 , AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m407525089_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<Blackjack.Card,Blackjack.Card>::EndInvoke(System.IAsyncResult)
extern "C"  Card_t539529194  Func_2_EndInvoke_m2191769314_gshared (Func_2_t1086561661 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Func_2_EndInvoke_m2191769314(__this, ___result0, method) ((  Card_t539529194  (*) (Func_2_t1086561661 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m2191769314_gshared)(__this, ___result0, method)
