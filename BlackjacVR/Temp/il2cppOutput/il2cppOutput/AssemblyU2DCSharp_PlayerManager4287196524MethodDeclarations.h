﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayerManager
struct PlayerManager_t4287196524;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayerManager::.ctor()
extern "C"  void PlayerManager__ctor_m2776059263 (PlayerManager_t4287196524 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerManager::Start()
extern "C"  void PlayerManager_Start_m1723197055 (PlayerManager_t4287196524 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerManager::Update()
extern "C"  void PlayerManager_Update_m1885353326 (PlayerManager_t4287196524 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PlayerManager::getPlayerPoints()
extern "C"  int32_t PlayerManager_getPlayerPoints_m3453719781 (PlayerManager_t4287196524 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerManager::updatePlayerStats(System.Int32)
extern "C"  void PlayerManager_updatePlayerStats_m1088907139 (PlayerManager_t4287196524 * __this, int32_t ___deltaPoints0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
