﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType1744280289.h"
#include "AssemblyU2DCSharp_Blackjack_WinType832601200.h"
#include "AssemblyU2DCSharp_Blackjack_User540082341.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Blackjack.GameResult
struct  GameResult_t3329670025 
{
public:
	// Blackjack.WinType Blackjack.GameResult::type
	int32_t ___type_0;
	// Blackjack.User Blackjack.GameResult::winner
	int32_t ___winner_1;
	// System.Int32 Blackjack.GameResult::delaerPoints
	int32_t ___delaerPoints_2;
	// System.Int32 Blackjack.GameResult::playerPoints
	int32_t ___playerPoints_3;
	// System.Double Blackjack.GameResult::winAmount
	double ___winAmount_4;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(GameResult_t3329670025, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_winner_1() { return static_cast<int32_t>(offsetof(GameResult_t3329670025, ___winner_1)); }
	inline int32_t get_winner_1() const { return ___winner_1; }
	inline int32_t* get_address_of_winner_1() { return &___winner_1; }
	inline void set_winner_1(int32_t value)
	{
		___winner_1 = value;
	}

	inline static int32_t get_offset_of_delaerPoints_2() { return static_cast<int32_t>(offsetof(GameResult_t3329670025, ___delaerPoints_2)); }
	inline int32_t get_delaerPoints_2() const { return ___delaerPoints_2; }
	inline int32_t* get_address_of_delaerPoints_2() { return &___delaerPoints_2; }
	inline void set_delaerPoints_2(int32_t value)
	{
		___delaerPoints_2 = value;
	}

	inline static int32_t get_offset_of_playerPoints_3() { return static_cast<int32_t>(offsetof(GameResult_t3329670025, ___playerPoints_3)); }
	inline int32_t get_playerPoints_3() const { return ___playerPoints_3; }
	inline int32_t* get_address_of_playerPoints_3() { return &___playerPoints_3; }
	inline void set_playerPoints_3(int32_t value)
	{
		___playerPoints_3 = value;
	}

	inline static int32_t get_offset_of_winAmount_4() { return static_cast<int32_t>(offsetof(GameResult_t3329670025, ___winAmount_4)); }
	inline double get_winAmount_4() const { return ___winAmount_4; }
	inline double* get_address_of_winAmount_4() { return &___winAmount_4; }
	inline void set_winAmount_4(double value)
	{
		___winAmount_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: Blackjack.GameResult
struct GameResult_t3329670025_marshaled_pinvoke
{
	int32_t ___type_0;
	int32_t ___winner_1;
	int32_t ___delaerPoints_2;
	int32_t ___playerPoints_3;
	double ___winAmount_4;
};
// Native definition for marshalling of: Blackjack.GameResult
struct GameResult_t3329670025_marshaled_com
{
	int32_t ___type_0;
	int32_t ___winner_1;
	int32_t ___delaerPoints_2;
	int32_t ___playerPoints_3;
	double ___winAmount_4;
};
