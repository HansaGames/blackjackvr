﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Blackjack.Card>
struct DefaultComparer_t3282273157;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Blackjack_Card539529194.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Blackjack.Card>::.ctor()
extern "C"  void DefaultComparer__ctor_m300825740_gshared (DefaultComparer_t3282273157 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m300825740(__this, method) ((  void (*) (DefaultComparer_t3282273157 *, const MethodInfo*))DefaultComparer__ctor_m300825740_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Blackjack.Card>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m2114063847_gshared (DefaultComparer_t3282273157 * __this, Card_t539529194  ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m2114063847(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t3282273157 *, Card_t539529194 , const MethodInfo*))DefaultComparer_GetHashCode_m2114063847_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Blackjack.Card>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m798022433_gshared (DefaultComparer_t3282273157 * __this, Card_t539529194  ___x0, Card_t539529194  ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m798022433(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t3282273157 *, Card_t539529194 , Card_t539529194 , const MethodInfo*))DefaultComparer_Equals_m798022433_gshared)(__this, ___x0, ___y1, method)
