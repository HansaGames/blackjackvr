﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen62446588MethodDeclarations.h"

// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.GameObject>::.ctor()
#define UnityEvent_1__ctor_m3850240253(__this, method) ((  void (*) (UnityEvent_1_t3861279518 *, const MethodInfo*))UnityEvent_1__ctor_m4139691420_gshared)(__this, method)
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.GameObject>::AddListener(UnityEngine.Events.UnityAction`1<T0>)
#define UnityEvent_1_AddListener_m2390571779(__this, ___call0, method) ((  void (*) (UnityEvent_1_t3861279518 *, UnityAction_1_t3568862008 *, const MethodInfo*))UnityEvent_1_AddListener_m1298407787_gshared)(__this, ___call0, method)
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.GameObject>::RemoveListener(UnityEngine.Events.UnityAction`1<T0>)
#define UnityEvent_1_RemoveListener_m510279652(__this, ___call0, method) ((  void (*) (UnityEvent_1_t3861279518 *, UnityAction_1_t3568862008 *, const MethodInfo*))UnityEvent_1_RemoveListener_m2525028476_gshared)(__this, ___call0, method)
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`1<UnityEngine.GameObject>::FindMethod_Impl(System.String,System.Object)
#define UnityEvent_1_FindMethod_Impl_m3404144541(__this, ___name0, ___targetObj1, method) ((  MethodInfo_t * (*) (UnityEvent_1_t3861279518 *, String_t*, Il2CppObject *, const MethodInfo*))UnityEvent_1_FindMethod_Impl_m936319469_gshared)(__this, ___name0, ___targetObj1, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<UnityEngine.GameObject>::GetDelegate(System.Object,System.Reflection.MethodInfo)
#define UnityEvent_1_GetDelegate_m2686697029(__this, ___target0, ___theFunction1, method) ((  BaseInvokableCall_t1559630662 * (*) (UnityEvent_1_t3861279518 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))UnityEvent_1_GetDelegate_m882504923_gshared)(__this, ___target0, ___theFunction1, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<UnityEngine.GameObject>::GetDelegate(UnityEngine.Events.UnityAction`1<T0>)
#define UnityEvent_1_GetDelegate_m2692744354(__this /* static, unused */, ___action0, method) ((  BaseInvokableCall_t1559630662 * (*) (Il2CppObject * /* static, unused */, UnityAction_1_t3568862008 *, const MethodInfo*))UnityEvent_1_GetDelegate_m2100323256_gshared)(__this /* static, unused */, ___action0, method)
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.GameObject>::Invoke(T0)
#define UnityEvent_1_Invoke_m406275500(__this, ___arg00, method) ((  void (*) (UnityEvent_1_t3861279518 *, GameObject_t3674682005 *, const MethodInfo*))UnityEvent_1_Invoke_m3779699780_gshared)(__this, ___arg00, method)
