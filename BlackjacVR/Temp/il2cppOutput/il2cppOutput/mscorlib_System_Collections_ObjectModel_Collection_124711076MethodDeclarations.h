﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<Blackjack.Decision>
struct Collection_1_t124711076;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// Blackjack.Decision[]
struct DecisionU5BU5D_t3084712883;
// System.Collections.Generic.IEnumerator`1<Blackjack.Decision>
struct IEnumerator_1_t2851117967;
// System.Collections.Generic.IList`1<Blackjack.Decision>
struct IList_1_t3633900121;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Blackjack_Decision939252918.h"

// System.Void System.Collections.ObjectModel.Collection`1<Blackjack.Decision>::.ctor()
extern "C"  void Collection_1__ctor_m1470373939_gshared (Collection_1_t124711076 * __this, const MethodInfo* method);
#define Collection_1__ctor_m1470373939(__this, method) ((  void (*) (Collection_1_t124711076 *, const MethodInfo*))Collection_1__ctor_m1470373939_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Blackjack.Decision>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3820087588_gshared (Collection_1_t124711076 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3820087588(__this, method) ((  bool (*) (Collection_1_t124711076 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3820087588_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Blackjack.Decision>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m3662190705_gshared (Collection_1_t124711076 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m3662190705(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t124711076 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m3662190705_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<Blackjack.Decision>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m1000286144_gshared (Collection_1_t124711076 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m1000286144(__this, method) ((  Il2CppObject * (*) (Collection_1_t124711076 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m1000286144_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Blackjack.Decision>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m1377438077_gshared (Collection_1_t124711076 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m1377438077(__this, ___value0, method) ((  int32_t (*) (Collection_1_t124711076 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m1377438077_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Blackjack.Decision>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m4071717091_gshared (Collection_1_t124711076 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m4071717091(__this, ___value0, method) ((  bool (*) (Collection_1_t124711076 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m4071717091_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Blackjack.Decision>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m3926866261_gshared (Collection_1_t124711076 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m3926866261(__this, ___value0, method) ((  int32_t (*) (Collection_1_t124711076 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m3926866261_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Blackjack.Decision>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m1887119688_gshared (Collection_1_t124711076 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m1887119688(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t124711076 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m1887119688_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Blackjack.Decision>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m1107757792_gshared (Collection_1_t124711076 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m1107757792(__this, ___value0, method) ((  void (*) (Collection_1_t124711076 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m1107757792_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Blackjack.Decision>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m2052460441_gshared (Collection_1_t124711076 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m2052460441(__this, method) ((  bool (*) (Collection_1_t124711076 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m2052460441_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Blackjack.Decision>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m40818763_gshared (Collection_1_t124711076 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m40818763(__this, method) ((  Il2CppObject * (*) (Collection_1_t124711076 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m40818763_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Blackjack.Decision>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m1307725458_gshared (Collection_1_t124711076 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m1307725458(__this, method) ((  bool (*) (Collection_1_t124711076 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m1307725458_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Blackjack.Decision>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m2158761511_gshared (Collection_1_t124711076 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m2158761511(__this, method) ((  bool (*) (Collection_1_t124711076 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m2158761511_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Blackjack.Decision>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m1637737234_gshared (Collection_1_t124711076 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m1637737234(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t124711076 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m1637737234_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Blackjack.Decision>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m1659773727_gshared (Collection_1_t124711076 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m1659773727(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t124711076 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m1659773727_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Blackjack.Decision>::Add(T)
extern "C"  void Collection_1_Add_m2669972972_gshared (Collection_1_t124711076 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Add_m2669972972(__this, ___item0, method) ((  void (*) (Collection_1_t124711076 *, int32_t, const MethodInfo*))Collection_1_Add_m2669972972_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Blackjack.Decision>::Clear()
extern "C"  void Collection_1_Clear_m3171474526_gshared (Collection_1_t124711076 * __this, const MethodInfo* method);
#define Collection_1_Clear_m3171474526(__this, method) ((  void (*) (Collection_1_t124711076 *, const MethodInfo*))Collection_1_Clear_m3171474526_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Blackjack.Decision>::ClearItems()
extern "C"  void Collection_1_ClearItems_m2314456932_gshared (Collection_1_t124711076 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m2314456932(__this, method) ((  void (*) (Collection_1_t124711076 *, const MethodInfo*))Collection_1_ClearItems_m2314456932_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Blackjack.Decision>::Contains(T)
extern "C"  bool Collection_1_Contains_m3115791952_gshared (Collection_1_t124711076 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Contains_m3115791952(__this, ___item0, method) ((  bool (*) (Collection_1_t124711076 *, int32_t, const MethodInfo*))Collection_1_Contains_m3115791952_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Blackjack.Decision>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m3694351068_gshared (Collection_1_t124711076 * __this, DecisionU5BU5D_t3084712883* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m3694351068(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t124711076 *, DecisionU5BU5D_t3084712883*, int32_t, const MethodInfo*))Collection_1_CopyTo_m3694351068_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<Blackjack.Decision>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m589149607_gshared (Collection_1_t124711076 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m589149607(__this, method) ((  Il2CppObject* (*) (Collection_1_t124711076 *, const MethodInfo*))Collection_1_GetEnumerator_m589149607_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Blackjack.Decision>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m1905230248_gshared (Collection_1_t124711076 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m1905230248(__this, ___item0, method) ((  int32_t (*) (Collection_1_t124711076 *, int32_t, const MethodInfo*))Collection_1_IndexOf_m1905230248_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Blackjack.Decision>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m16805971_gshared (Collection_1_t124711076 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_Insert_m16805971(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t124711076 *, int32_t, int32_t, const MethodInfo*))Collection_1_Insert_m16805971_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Blackjack.Decision>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m294731014_gshared (Collection_1_t124711076 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m294731014(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t124711076 *, int32_t, int32_t, const MethodInfo*))Collection_1_InsertItem_m294731014_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Blackjack.Decision>::Remove(T)
extern "C"  bool Collection_1_Remove_m1021659723_gshared (Collection_1_t124711076 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Remove_m1021659723(__this, ___item0, method) ((  bool (*) (Collection_1_t124711076 *, int32_t, const MethodInfo*))Collection_1_Remove_m1021659723_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Blackjack.Decision>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m2185626137_gshared (Collection_1_t124711076 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m2185626137(__this, ___index0, method) ((  void (*) (Collection_1_t124711076 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m2185626137_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Blackjack.Decision>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m3047334905_gshared (Collection_1_t124711076 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m3047334905(__this, ___index0, method) ((  void (*) (Collection_1_t124711076 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m3047334905_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Blackjack.Decision>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m3044870355_gshared (Collection_1_t124711076 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m3044870355(__this, method) ((  int32_t (*) (Collection_1_t124711076 *, const MethodInfo*))Collection_1_get_Count_m3044870355_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<Blackjack.Decision>::get_Item(System.Int32)
extern "C"  int32_t Collection_1_get_Item_m3868073535_gshared (Collection_1_t124711076 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m3868073535(__this, ___index0, method) ((  int32_t (*) (Collection_1_t124711076 *, int32_t, const MethodInfo*))Collection_1_get_Item_m3868073535_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Blackjack.Decision>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m780265578_gshared (Collection_1_t124711076 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m780265578(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t124711076 *, int32_t, int32_t, const MethodInfo*))Collection_1_set_Item_m780265578_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Blackjack.Decision>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m1837574863_gshared (Collection_1_t124711076 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m1837574863(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t124711076 *, int32_t, int32_t, const MethodInfo*))Collection_1_SetItem_m1837574863_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Blackjack.Decision>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m3071005148_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m3071005148(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m3071005148_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<Blackjack.Decision>::ConvertItem(System.Object)
extern "C"  int32_t Collection_1_ConvertItem_m3976238494_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m3976238494(__this /* static, unused */, ___item0, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m3976238494_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Blackjack.Decision>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m354990748_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m354990748(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m354990748_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Blackjack.Decision>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m1541580648_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m1541580648(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m1541580648_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Blackjack.Decision>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m315824183_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m315824183(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m315824183_gshared)(__this /* static, unused */, ___list0, method)
