﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GVR.Input.AppButtonInput
struct AppButtonInput_t2269773419;

#include "codegen/il2cpp-codegen.h"

// System.Void GVR.Input.AppButtonInput::.ctor()
extern "C"  void AppButtonInput__ctor_m1236845525 (AppButtonInput_t2269773419 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
