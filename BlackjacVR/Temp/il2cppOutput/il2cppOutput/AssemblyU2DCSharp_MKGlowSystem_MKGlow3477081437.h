﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Single[]
struct SingleU5BU5D_t2316563989;
// UnityEngine.Shader
struct Shader_t3191267369;
// UnityEngine.Material
struct Material_t3870600107;
// UnityEngine.Camera
struct Camera_t2727095145;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UnityEngine.RenderTexture
struct RenderTexture_t1963041563;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_LayerMask3236759763.h"
#include "AssemblyU2DCSharp_MKGlowSystem_MKGlowType2044315959.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MKGlowSystem.MKGlow
struct  MKGlow_t3477081437  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Shader MKGlowSystem.MKGlow::blurShader
	Shader_t3191267369 * ___blurShader_3;
	// UnityEngine.Shader MKGlowSystem.MKGlow::compositeShader
	Shader_t3191267369 * ___compositeShader_4;
	// UnityEngine.Shader MKGlowSystem.MKGlow::glowRenderShader
	Shader_t3191267369 * ___glowRenderShader_5;
	// UnityEngine.Material MKGlowSystem.MKGlow::compositeMaterial
	Material_t3870600107 * ___compositeMaterial_6;
	// UnityEngine.Material MKGlowSystem.MKGlow::blurMaterial
	Material_t3870600107 * ___blurMaterial_7;
	// UnityEngine.Camera MKGlowSystem.MKGlow::glowCamera
	Camera_t2727095145 * ___glowCamera_8;
	// UnityEngine.GameObject MKGlowSystem.MKGlow::glowCameraObject
	GameObject_t3674682005 * ___glowCameraObject_9;
	// UnityEngine.RenderTexture MKGlowSystem.MKGlow::glowTexture
	RenderTexture_t1963041563 * ___glowTexture_10;
	// UnityEngine.LayerMask MKGlowSystem.MKGlow::glowLayer
	LayerMask_t3236759763  ___glowLayer_11;
	// UnityEngine.Camera MKGlowSystem.MKGlow::renderCamera
	Camera_t2727095145 * ___renderCamera_12;
	// System.Boolean MKGlowSystem.MKGlow::showCutoutGlow
	bool ___showCutoutGlow_13;
	// System.Boolean MKGlowSystem.MKGlow::showTransparentGlow
	bool ___showTransparentGlow_14;
	// MKGlowSystem.MKGlowType MKGlowSystem.MKGlow::glowType
	int32_t ___glowType_15;
	// UnityEngine.Color MKGlowSystem.MKGlow::fullScreenGlowTint
	Color_t4194546905  ___fullScreenGlowTint_16;
	// System.Single MKGlowSystem.MKGlow::blurSpread
	float ___blurSpread_17;
	// System.Int32 MKGlowSystem.MKGlow::blurIterations
	int32_t ___blurIterations_18;
	// System.Single MKGlowSystem.MKGlow::glowIntensity
	float ___glowIntensity_19;
	// System.Int32 MKGlowSystem.MKGlow::samples
	int32_t ___samples_20;

public:
	inline static int32_t get_offset_of_blurShader_3() { return static_cast<int32_t>(offsetof(MKGlow_t3477081437, ___blurShader_3)); }
	inline Shader_t3191267369 * get_blurShader_3() const { return ___blurShader_3; }
	inline Shader_t3191267369 ** get_address_of_blurShader_3() { return &___blurShader_3; }
	inline void set_blurShader_3(Shader_t3191267369 * value)
	{
		___blurShader_3 = value;
		Il2CppCodeGenWriteBarrier(&___blurShader_3, value);
	}

	inline static int32_t get_offset_of_compositeShader_4() { return static_cast<int32_t>(offsetof(MKGlow_t3477081437, ___compositeShader_4)); }
	inline Shader_t3191267369 * get_compositeShader_4() const { return ___compositeShader_4; }
	inline Shader_t3191267369 ** get_address_of_compositeShader_4() { return &___compositeShader_4; }
	inline void set_compositeShader_4(Shader_t3191267369 * value)
	{
		___compositeShader_4 = value;
		Il2CppCodeGenWriteBarrier(&___compositeShader_4, value);
	}

	inline static int32_t get_offset_of_glowRenderShader_5() { return static_cast<int32_t>(offsetof(MKGlow_t3477081437, ___glowRenderShader_5)); }
	inline Shader_t3191267369 * get_glowRenderShader_5() const { return ___glowRenderShader_5; }
	inline Shader_t3191267369 ** get_address_of_glowRenderShader_5() { return &___glowRenderShader_5; }
	inline void set_glowRenderShader_5(Shader_t3191267369 * value)
	{
		___glowRenderShader_5 = value;
		Il2CppCodeGenWriteBarrier(&___glowRenderShader_5, value);
	}

	inline static int32_t get_offset_of_compositeMaterial_6() { return static_cast<int32_t>(offsetof(MKGlow_t3477081437, ___compositeMaterial_6)); }
	inline Material_t3870600107 * get_compositeMaterial_6() const { return ___compositeMaterial_6; }
	inline Material_t3870600107 ** get_address_of_compositeMaterial_6() { return &___compositeMaterial_6; }
	inline void set_compositeMaterial_6(Material_t3870600107 * value)
	{
		___compositeMaterial_6 = value;
		Il2CppCodeGenWriteBarrier(&___compositeMaterial_6, value);
	}

	inline static int32_t get_offset_of_blurMaterial_7() { return static_cast<int32_t>(offsetof(MKGlow_t3477081437, ___blurMaterial_7)); }
	inline Material_t3870600107 * get_blurMaterial_7() const { return ___blurMaterial_7; }
	inline Material_t3870600107 ** get_address_of_blurMaterial_7() { return &___blurMaterial_7; }
	inline void set_blurMaterial_7(Material_t3870600107 * value)
	{
		___blurMaterial_7 = value;
		Il2CppCodeGenWriteBarrier(&___blurMaterial_7, value);
	}

	inline static int32_t get_offset_of_glowCamera_8() { return static_cast<int32_t>(offsetof(MKGlow_t3477081437, ___glowCamera_8)); }
	inline Camera_t2727095145 * get_glowCamera_8() const { return ___glowCamera_8; }
	inline Camera_t2727095145 ** get_address_of_glowCamera_8() { return &___glowCamera_8; }
	inline void set_glowCamera_8(Camera_t2727095145 * value)
	{
		___glowCamera_8 = value;
		Il2CppCodeGenWriteBarrier(&___glowCamera_8, value);
	}

	inline static int32_t get_offset_of_glowCameraObject_9() { return static_cast<int32_t>(offsetof(MKGlow_t3477081437, ___glowCameraObject_9)); }
	inline GameObject_t3674682005 * get_glowCameraObject_9() const { return ___glowCameraObject_9; }
	inline GameObject_t3674682005 ** get_address_of_glowCameraObject_9() { return &___glowCameraObject_9; }
	inline void set_glowCameraObject_9(GameObject_t3674682005 * value)
	{
		___glowCameraObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___glowCameraObject_9, value);
	}

	inline static int32_t get_offset_of_glowTexture_10() { return static_cast<int32_t>(offsetof(MKGlow_t3477081437, ___glowTexture_10)); }
	inline RenderTexture_t1963041563 * get_glowTexture_10() const { return ___glowTexture_10; }
	inline RenderTexture_t1963041563 ** get_address_of_glowTexture_10() { return &___glowTexture_10; }
	inline void set_glowTexture_10(RenderTexture_t1963041563 * value)
	{
		___glowTexture_10 = value;
		Il2CppCodeGenWriteBarrier(&___glowTexture_10, value);
	}

	inline static int32_t get_offset_of_glowLayer_11() { return static_cast<int32_t>(offsetof(MKGlow_t3477081437, ___glowLayer_11)); }
	inline LayerMask_t3236759763  get_glowLayer_11() const { return ___glowLayer_11; }
	inline LayerMask_t3236759763 * get_address_of_glowLayer_11() { return &___glowLayer_11; }
	inline void set_glowLayer_11(LayerMask_t3236759763  value)
	{
		___glowLayer_11 = value;
	}

	inline static int32_t get_offset_of_renderCamera_12() { return static_cast<int32_t>(offsetof(MKGlow_t3477081437, ___renderCamera_12)); }
	inline Camera_t2727095145 * get_renderCamera_12() const { return ___renderCamera_12; }
	inline Camera_t2727095145 ** get_address_of_renderCamera_12() { return &___renderCamera_12; }
	inline void set_renderCamera_12(Camera_t2727095145 * value)
	{
		___renderCamera_12 = value;
		Il2CppCodeGenWriteBarrier(&___renderCamera_12, value);
	}

	inline static int32_t get_offset_of_showCutoutGlow_13() { return static_cast<int32_t>(offsetof(MKGlow_t3477081437, ___showCutoutGlow_13)); }
	inline bool get_showCutoutGlow_13() const { return ___showCutoutGlow_13; }
	inline bool* get_address_of_showCutoutGlow_13() { return &___showCutoutGlow_13; }
	inline void set_showCutoutGlow_13(bool value)
	{
		___showCutoutGlow_13 = value;
	}

	inline static int32_t get_offset_of_showTransparentGlow_14() { return static_cast<int32_t>(offsetof(MKGlow_t3477081437, ___showTransparentGlow_14)); }
	inline bool get_showTransparentGlow_14() const { return ___showTransparentGlow_14; }
	inline bool* get_address_of_showTransparentGlow_14() { return &___showTransparentGlow_14; }
	inline void set_showTransparentGlow_14(bool value)
	{
		___showTransparentGlow_14 = value;
	}

	inline static int32_t get_offset_of_glowType_15() { return static_cast<int32_t>(offsetof(MKGlow_t3477081437, ___glowType_15)); }
	inline int32_t get_glowType_15() const { return ___glowType_15; }
	inline int32_t* get_address_of_glowType_15() { return &___glowType_15; }
	inline void set_glowType_15(int32_t value)
	{
		___glowType_15 = value;
	}

	inline static int32_t get_offset_of_fullScreenGlowTint_16() { return static_cast<int32_t>(offsetof(MKGlow_t3477081437, ___fullScreenGlowTint_16)); }
	inline Color_t4194546905  get_fullScreenGlowTint_16() const { return ___fullScreenGlowTint_16; }
	inline Color_t4194546905 * get_address_of_fullScreenGlowTint_16() { return &___fullScreenGlowTint_16; }
	inline void set_fullScreenGlowTint_16(Color_t4194546905  value)
	{
		___fullScreenGlowTint_16 = value;
	}

	inline static int32_t get_offset_of_blurSpread_17() { return static_cast<int32_t>(offsetof(MKGlow_t3477081437, ___blurSpread_17)); }
	inline float get_blurSpread_17() const { return ___blurSpread_17; }
	inline float* get_address_of_blurSpread_17() { return &___blurSpread_17; }
	inline void set_blurSpread_17(float value)
	{
		___blurSpread_17 = value;
	}

	inline static int32_t get_offset_of_blurIterations_18() { return static_cast<int32_t>(offsetof(MKGlow_t3477081437, ___blurIterations_18)); }
	inline int32_t get_blurIterations_18() const { return ___blurIterations_18; }
	inline int32_t* get_address_of_blurIterations_18() { return &___blurIterations_18; }
	inline void set_blurIterations_18(int32_t value)
	{
		___blurIterations_18 = value;
	}

	inline static int32_t get_offset_of_glowIntensity_19() { return static_cast<int32_t>(offsetof(MKGlow_t3477081437, ___glowIntensity_19)); }
	inline float get_glowIntensity_19() const { return ___glowIntensity_19; }
	inline float* get_address_of_glowIntensity_19() { return &___glowIntensity_19; }
	inline void set_glowIntensity_19(float value)
	{
		___glowIntensity_19 = value;
	}

	inline static int32_t get_offset_of_samples_20() { return static_cast<int32_t>(offsetof(MKGlow_t3477081437, ___samples_20)); }
	inline int32_t get_samples_20() const { return ___samples_20; }
	inline int32_t* get_address_of_samples_20() { return &___samples_20; }
	inline void set_samples_20(int32_t value)
	{
		___samples_20 = value;
	}
};

struct MKGlow_t3477081437_StaticFields
{
public:
	// System.Single[] MKGlowSystem.MKGlow::gaussFilter
	SingleU5BU5D_t2316563989* ___gaussFilter_2;

public:
	inline static int32_t get_offset_of_gaussFilter_2() { return static_cast<int32_t>(offsetof(MKGlow_t3477081437_StaticFields, ___gaussFilter_2)); }
	inline SingleU5BU5D_t2316563989* get_gaussFilter_2() const { return ___gaussFilter_2; }
	inline SingleU5BU5D_t2316563989** get_address_of_gaussFilter_2() { return &___gaussFilter_2; }
	inline void set_gaussFilter_2(SingleU5BU5D_t2316563989* value)
	{
		___gaussFilter_2 = value;
		Il2CppCodeGenWriteBarrier(&___gaussFilter_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
