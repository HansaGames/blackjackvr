﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BlackjackGame/<playerPlayDrawGame>c__IteratorE
struct U3CplayerPlayDrawGameU3Ec__IteratorE_t223069034;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void BlackjackGame/<playerPlayDrawGame>c__IteratorE::.ctor()
extern "C"  void U3CplayerPlayDrawGameU3Ec__IteratorE__ctor_m3806847089 (U3CplayerPlayDrawGameU3Ec__IteratorE_t223069034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object BlackjackGame/<playerPlayDrawGame>c__IteratorE::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CplayerPlayDrawGameU3Ec__IteratorE_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3200912715 (U3CplayerPlayDrawGameU3Ec__IteratorE_t223069034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object BlackjackGame/<playerPlayDrawGame>c__IteratorE::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CplayerPlayDrawGameU3Ec__IteratorE_System_Collections_IEnumerator_get_Current_m3567126239 (U3CplayerPlayDrawGameU3Ec__IteratorE_t223069034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BlackjackGame/<playerPlayDrawGame>c__IteratorE::MoveNext()
extern "C"  bool U3CplayerPlayDrawGameU3Ec__IteratorE_MoveNext_m1934661835 (U3CplayerPlayDrawGameU3Ec__IteratorE_t223069034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BlackjackGame/<playerPlayDrawGame>c__IteratorE::Dispose()
extern "C"  void U3CplayerPlayDrawGameU3Ec__IteratorE_Dispose_m3261166702 (U3CplayerPlayDrawGameU3Ec__IteratorE_t223069034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BlackjackGame/<playerPlayDrawGame>c__IteratorE::Reset()
extern "C"  void U3CplayerPlayDrawGameU3Ec__IteratorE_Reset_m1453280030 (U3CplayerPlayDrawGameU3Ec__IteratorE_t223069034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
