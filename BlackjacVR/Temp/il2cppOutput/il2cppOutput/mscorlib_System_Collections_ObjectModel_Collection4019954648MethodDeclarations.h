﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<Blackjack.Card>
struct Collection_1_t4019954648;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// Blackjack.Card[]
struct CardU5BU5D_t1346973999;
// System.Collections.Generic.IEnumerator`1<Blackjack.Card>
struct IEnumerator_1_t2451394243;
// System.Collections.Generic.IList`1<Blackjack.Card>
struct IList_1_t3234176397;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Blackjack_Card539529194.h"

// System.Void System.Collections.ObjectModel.Collection`1<Blackjack.Card>::.ctor()
extern "C"  void Collection_1__ctor_m2389209959_gshared (Collection_1_t4019954648 * __this, const MethodInfo* method);
#define Collection_1__ctor_m2389209959(__this, method) ((  void (*) (Collection_1_t4019954648 *, const MethodInfo*))Collection_1__ctor_m2389209959_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Blackjack.Card>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2033164400_gshared (Collection_1_t4019954648 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2033164400(__this, method) ((  bool (*) (Collection_1_t4019954648 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2033164400_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Blackjack.Card>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m2527877053_gshared (Collection_1_t4019954648 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m2527877053(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t4019954648 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m2527877053_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<Blackjack.Card>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m695944972_gshared (Collection_1_t4019954648 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m695944972(__this, method) ((  Il2CppObject * (*) (Collection_1_t4019954648 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m695944972_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Blackjack.Card>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m364200113_gshared (Collection_1_t4019954648 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m364200113(__this, ___value0, method) ((  int32_t (*) (Collection_1_t4019954648 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m364200113_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Blackjack.Card>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m4123107119_gshared (Collection_1_t4019954648 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m4123107119(__this, ___value0, method) ((  bool (*) (Collection_1_t4019954648 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m4123107119_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Blackjack.Card>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m1913894537_gshared (Collection_1_t4019954648 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m1913894537(__this, ___value0, method) ((  int32_t (*) (Collection_1_t4019954648 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m1913894537_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Blackjack.Card>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m1152911996_gshared (Collection_1_t4019954648 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m1152911996(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t4019954648 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m1152911996_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Blackjack.Card>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m502285612_gshared (Collection_1_t4019954648 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m502285612(__this, ___value0, method) ((  void (*) (Collection_1_t4019954648 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m502285612_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Blackjack.Card>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m4004427213_gshared (Collection_1_t4019954648 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m4004427213(__this, method) ((  bool (*) (Collection_1_t4019954648 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m4004427213_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Blackjack.Card>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m2177779455_gshared (Collection_1_t4019954648 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m2177779455(__this, method) ((  Il2CppObject * (*) (Collection_1_t4019954648 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m2177779455_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Blackjack.Card>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m1350573534_gshared (Collection_1_t4019954648 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m1350573534(__this, method) ((  bool (*) (Collection_1_t4019954648 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m1350573534_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Blackjack.Card>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m2160143707_gshared (Collection_1_t4019954648 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m2160143707(__this, method) ((  bool (*) (Collection_1_t4019954648 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m2160143707_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Blackjack.Card>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m2262594758_gshared (Collection_1_t4019954648 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m2262594758(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t4019954648 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m2262594758_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Blackjack.Card>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m460818259_gshared (Collection_1_t4019954648 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m460818259(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t4019954648 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m460818259_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Blackjack.Card>::Add(T)
extern "C"  void Collection_1_Add_m3115254840_gshared (Collection_1_t4019954648 * __this, Card_t539529194  ___item0, const MethodInfo* method);
#define Collection_1_Add_m3115254840(__this, ___item0, method) ((  void (*) (Collection_1_t4019954648 *, Card_t539529194 , const MethodInfo*))Collection_1_Add_m3115254840_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Blackjack.Card>::Clear()
extern "C"  void Collection_1_Clear_m4090310546_gshared (Collection_1_t4019954648 * __this, const MethodInfo* method);
#define Collection_1_Clear_m4090310546(__this, method) ((  void (*) (Collection_1_t4019954648 *, const MethodInfo*))Collection_1_Clear_m4090310546_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Blackjack.Card>::ClearItems()
extern "C"  void Collection_1_ClearItems_m3903282352_gshared (Collection_1_t4019954648 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m3903282352(__this, method) ((  void (*) (Collection_1_t4019954648 *, const MethodInfo*))Collection_1_ClearItems_m3903282352_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Blackjack.Card>::Contains(T)
extern "C"  bool Collection_1_Contains_m2335776900_gshared (Collection_1_t4019954648 * __this, Card_t539529194  ___item0, const MethodInfo* method);
#define Collection_1_Contains_m2335776900(__this, ___item0, method) ((  bool (*) (Collection_1_t4019954648 *, Card_t539529194 , const MethodInfo*))Collection_1_Contains_m2335776900_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Blackjack.Card>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m3219213864_gshared (Collection_1_t4019954648 * __this, CardU5BU5D_t1346973999* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m3219213864(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t4019954648 *, CardU5BU5D_t1346973999*, int32_t, const MethodInfo*))Collection_1_CopyTo_m3219213864_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<Blackjack.Card>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m984974043_gshared (Collection_1_t4019954648 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m984974043(__this, method) ((  Il2CppObject* (*) (Collection_1_t4019954648 *, const MethodInfo*))Collection_1_GetEnumerator_m984974043_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Blackjack.Card>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m676512756_gshared (Collection_1_t4019954648 * __this, Card_t539529194  ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m676512756(__this, ___item0, method) ((  int32_t (*) (Collection_1_t4019954648 *, Card_t539529194 , const MethodInfo*))Collection_1_IndexOf_m676512756_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Blackjack.Card>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m105696927_gshared (Collection_1_t4019954648 * __this, int32_t ___index0, Card_t539529194  ___item1, const MethodInfo* method);
#define Collection_1_Insert_m105696927(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t4019954648 *, int32_t, Card_t539529194 , const MethodInfo*))Collection_1_Insert_m105696927_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Blackjack.Card>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m3249378642_gshared (Collection_1_t4019954648 * __this, int32_t ___index0, Card_t539529194  ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m3249378642(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t4019954648 *, int32_t, Card_t539529194 , const MethodInfo*))Collection_1_InsertItem_m3249378642_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Blackjack.Card>::Remove(T)
extern "C"  bool Collection_1_Remove_m4202967423_gshared (Collection_1_t4019954648 * __this, Card_t539529194  ___item0, const MethodInfo* method);
#define Collection_1_Remove_m4202967423(__this, ___item0, method) ((  bool (*) (Collection_1_t4019954648 *, Card_t539529194 , const MethodInfo*))Collection_1_Remove_m4202967423_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Blackjack.Card>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m2274517093_gshared (Collection_1_t4019954648 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m2274517093(__this, ___index0, method) ((  void (*) (Collection_1_t4019954648 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m2274517093_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Blackjack.Card>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m2572197701_gshared (Collection_1_t4019954648 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m2572197701(__this, ___index0, method) ((  void (*) (Collection_1_t4019954648 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m2572197701_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Blackjack.Card>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m3609333767_gshared (Collection_1_t4019954648 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m3609333767(__this, method) ((  int32_t (*) (Collection_1_t4019954648 *, const MethodInfo*))Collection_1_get_Count_m3609333767_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<Blackjack.Card>::get_Item(System.Int32)
extern "C"  Card_t539529194  Collection_1_get_Item_m2664617227_gshared (Collection_1_t4019954648 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m2664617227(__this, ___index0, method) ((  Card_t539529194  (*) (Collection_1_t4019954648 *, int32_t, const MethodInfo*))Collection_1_get_Item_m2664617227_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Blackjack.Card>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m305128374_gshared (Collection_1_t4019954648 * __this, int32_t ___index0, Card_t539529194  ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m305128374(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t4019954648 *, int32_t, Card_t539529194 , const MethodInfo*))Collection_1_set_Item_m305128374_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Blackjack.Card>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m298227203_gshared (Collection_1_t4019954648 * __this, int32_t ___index0, Card_t539529194  ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m298227203(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t4019954648 *, int32_t, Card_t539529194 , const MethodInfo*))Collection_1_SetItem_m298227203_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Blackjack.Card>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m3118053672_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m3118053672(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m3118053672_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<Blackjack.Card>::ConvertItem(System.Object)
extern "C"  Card_t539529194  Collection_1_ConvertItem_m4080338538_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m4080338538(__this /* static, unused */, ___item0, method) ((  Card_t539529194  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m4080338538_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Blackjack.Card>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m2081950696_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m2081950696(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m2081950696_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Blackjack.Card>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m480940188_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m480940188(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m480940188_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Blackjack.Card>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m2457000835_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m2457000835(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m2457000835_gshared)(__this /* static, unused */, ___list0, method)
