﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1<Blackjack.Card>
struct EqualityComparer_1_t3942333371;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.Generic.EqualityComparer`1<Blackjack.Card>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m2693666059_gshared (EqualityComparer_1_t3942333371 * __this, const MethodInfo* method);
#define EqualityComparer_1__ctor_m2693666059(__this, method) ((  void (*) (EqualityComparer_1_t3942333371 *, const MethodInfo*))EqualityComparer_1__ctor_m2693666059_gshared)(__this, method)
// System.Void System.Collections.Generic.EqualityComparer`1<Blackjack.Card>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m1417172994_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1__cctor_m1417172994(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1__cctor_m1417172994_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<Blackjack.Card>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m2935480988_gshared (EqualityComparer_1_t3942333371 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m2935480988(__this, ___obj0, method) ((  int32_t (*) (EqualityComparer_1_t3942333371 *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m2935480988_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1<Blackjack.Card>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m446061026_gshared (EqualityComparer_1_t3942333371 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m446061026(__this, ___x0, ___y1, method) ((  bool (*) (EqualityComparer_1_t3942333371 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m446061026_gshared)(__this, ___x0, ___y1, method)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Blackjack.Card>::get_Default()
extern "C"  EqualityComparer_1_t3942333371 * EqualityComparer_1_get_Default_m207171085_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1_get_Default_m207171085(__this /* static, unused */, method) ((  EqualityComparer_1_t3942333371 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1_get_Default_m207171085_gshared)(__this /* static, unused */, method)
