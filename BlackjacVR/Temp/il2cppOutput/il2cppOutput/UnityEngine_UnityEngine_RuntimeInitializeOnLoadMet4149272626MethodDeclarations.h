﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.RuntimeInitializeOnLoadMethodAttribute
struct RuntimeInitializeOnLoadMethodAttribute_t4149272626;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RuntimeInitializeLoadType1896817118.h"

// System.Void UnityEngine.RuntimeInitializeOnLoadMethodAttribute::.ctor(UnityEngine.RuntimeInitializeLoadType)
extern "C"  void RuntimeInitializeOnLoadMethodAttribute__ctor_m3846352610 (RuntimeInitializeOnLoadMethodAttribute_t4149272626 * __this, int32_t ___loadType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RuntimeInitializeOnLoadMethodAttribute::set_loadType(UnityEngine.RuntimeInitializeLoadType)
extern "C"  void RuntimeInitializeOnLoadMethodAttribute_set_loadType_m2327033279 (RuntimeInitializeOnLoadMethodAttribute_t4149272626 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
