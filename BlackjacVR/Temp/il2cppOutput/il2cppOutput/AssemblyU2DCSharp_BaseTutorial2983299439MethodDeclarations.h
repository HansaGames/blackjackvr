﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BaseTutorial
struct BaseTutorial_t2983299439;

#include "codegen/il2cpp-codegen.h"

// System.Void BaseTutorial::.ctor()
extern "C"  void BaseTutorial__ctor_m2323158540 (BaseTutorial_t2983299439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BaseTutorial::startAudio()
extern "C"  void BaseTutorial_startAudio_m1593551852 (BaseTutorial_t2983299439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BaseTutorial::Update()
extern "C"  void BaseTutorial_Update_m730332801 (BaseTutorial_t2983299439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
