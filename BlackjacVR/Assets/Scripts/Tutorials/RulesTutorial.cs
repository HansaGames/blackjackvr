﻿using UnityEngine;
using System.Collections;

public class RulesTutorial : BaseTutorial {

	// Use this for initialization
	 void Start () {
		base.startAudio ();
		GameObject.Find ("Tutorial GO").transform.Find ("RulesTutorialGO").gameObject.SetActive (true);
	}
		
	override protected AudioClip getTutorialClip() {
		return Resources.Load("Sounds/Blackjack_tutorial_sounds/RulesTutorialSound", typeof(AudioClip)) as AudioClip;
	}



	void OnDestroy() {
		GameObject.Find ("Tutorial GO").transform.Find ("RulesTutorialGO").gameObject.SetActive (false);
	}
}
