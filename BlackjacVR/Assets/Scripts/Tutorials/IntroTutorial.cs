﻿using UnityEngine;
using System.Collections;

public class IntroTutorial : BaseTutorial {

	void Start() {
		base.startAudio ();
	}

	protected override AudioClip getTutorialClip() {
		return Resources.Load("Sounds/Blackjack_tutorial_sounds/IntroTutorialSound", typeof(AudioClip)) as AudioClip;
	}
}
