﻿using UnityEngine;
using System.Collections;

public abstract class BaseTutorial : MonoBehaviour {

	public AudioSource audio;
	// Use this for initialization
	public void startAudio () {
		audio = PlayerManager.Instance.mainDealer.GetComponent<AudioSource> () as AudioSource;
		AudioClip clip = getTutorialClip();
		if (clip != null) {
			audio.clip = clip;
		}
		audio.Play();
	}
	
	// Update is called once per frame
	void Update () {
		if (!audio.isPlaying) {
			TutorialManager manager = PlayerManager.Instance.blackJackManager.GetComponent<TutorialManager> ();
			if (manager != null) {
				manager.enableTutorialButtons (true);
			}
		} 
	}

	abstract protected AudioClip getTutorialClip();
}
