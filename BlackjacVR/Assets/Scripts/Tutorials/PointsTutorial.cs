﻿using UnityEngine;
using System.Collections;

public class PointsTutorial : BaseTutorial {

	// Use this for initialization
 	 void Start () {
		base.startAudio ();
		StartCoroutine (showCards());
	}

	IEnumerator showCards() {
		yield return new WaitForSeconds(18);
		GameObject.Find ("Tutorial GO").transform.Find ("PointsTutorialGO").transform.Find ("NumberCards").gameObject.SetActive (true);
		yield return new WaitForSeconds(12);
		GameObject.Find ("Tutorial GO").transform.Find ("PointsTutorialGO").transform.Find ("10PointsCards").gameObject.SetActive (true);
		yield return new WaitForSeconds(10);
		GameObject.Find ("Tutorial GO").transform.Find ("PointsTutorialGO").transform.Find ("Ace").gameObject.SetActive (true);
	}

	// Update is called once per frame
	void Update () {
		if (!audio.isPlaying) {
			PlayerManager.Instance.blackJackManager.GetComponent<TutorialManager> ().enableTutorialButtons (true);
		} 
	}

	override protected AudioClip getTutorialClip() {
		return Resources.Load("Sounds/Blackjack_tutorial_sounds/PointsTutorialSound", typeof(AudioClip)) as AudioClip;
	}

	void OnDestroy() {
		GameObject.Find ("Tutorial GO").transform.Find ("PointsTutorialGO").transform.Find ("NumberCards").gameObject.SetActive (false);
		GameObject.Find ("Tutorial GO").transform.Find ("PointsTutorialGO").transform.Find ("10PointsCards").gameObject.SetActive (false);
		GameObject.Find ("Tutorial GO").transform.Find ("PointsTutorialGO").transform.Find ("Ace").gameObject.SetActive (false);
	}
}
