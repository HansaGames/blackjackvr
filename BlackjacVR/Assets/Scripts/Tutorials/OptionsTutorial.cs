﻿using UnityEngine;
using System.Collections;

public class OptionsTutorial : BaseTutorial {

	// Use this for initialization
    void Start () {
		base.startAudio ();
		GameObject.Find ("Tutorial GO").transform.Find ("OptionsTutorialGO").gameObject.SetActive (true);
	}
	
	// Update is called once per frame
	void Update () {
		if (!audio.isPlaying) {
			PlayerManager.Instance.blackJackManager.GetComponent<TutorialManager> ().enableTutorialButtons (true);
		} 
	}

	override protected AudioClip getTutorialClip() {
		return Resources.Load("Sounds/Blackjack_tutorial_sounds/OptionsTutorialSound", typeof(AudioClip)) as AudioClip;
	}

	void OnDestroy() {
		GameObject.Find ("Tutorial GO").transform.Find ("OptionsTutorialGO").gameObject.SetActive (false);
	}
}
