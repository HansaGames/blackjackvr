﻿using UnityEngine;
using System.Collections;
using Blackjack;

public class TestBlackjackGame : BlackjackGame {

	public void newGame(Game game) {
		base.newGame (game, 0);
	}

	protected override IEnumerator endGame() {
		clearObjects ();
		yield return new WaitForEndOfFrame ();
		(PlayerManager.Instance.blackJackManager.GetComponent<TutorialManager>().currentTutorialGO as TestTutorial).startTutorial ();
	}

}
