using System;
using System.Collections.Generic;
using System.Linq;
namespace Blackjack {
    public enum Suit {
        Hearts = 1,
        Diamonds,
        Clubs,
        Spades
    }
    public enum CardType {
        Ace = 1,
        King,
        Queen,
        Jack,
        C10,
        C9,
        C8,
        C7,
        C6,
        C5,
        C4,
        C3,
        C2
    }
    public struct Card {
        public Suit suit;
        public CardType type;
        public int[] values;
        public Card(Suit suit, CardType type, int[] values) {
            this.suit = suit;
            this.type = type;
            this.values = values;
        }
    }
    public class Deck {
        private Card[] cardDeck;
        private Random gen = new Random();
        public Deck() {
            cardDeck = new Card[52]{
                new Card(Suit.Hearts, CardType.Ace, new int[2]{11, 1}),
                new Card(Suit.Diamonds, CardType.Ace,new int[2]{11, 1}),
                new Card(Suit.Clubs, CardType.Ace,new int[2]{11, 1}),
                new Card(Suit.Spades, CardType.Ace,new int[2]{11, 1}),
                new Card(Suit.Hearts, CardType.King,new int[1]{10}),
                new Card(Suit.Diamonds, CardType.King,new int[1]{10}),
                new Card(Suit.Clubs, CardType.King,new int[1]{10}),
                new Card(Suit.Spades, CardType.King,new int[1]{10}),
                new Card(Suit.Hearts, CardType.Queen,new int[1]{10}),
                new Card(Suit.Diamonds, CardType.Queen,new int[1]{10}),
                new Card(Suit.Clubs, CardType.Queen,new int[1]{10}),
                new Card(Suit.Spades, CardType.Queen,new int[1]{10}),
                new Card(Suit.Hearts, CardType.Jack,new int[1]{10}),
                new Card(Suit.Diamonds, CardType.Jack,new int[1]{10}),
                new Card(Suit.Clubs, CardType.Jack,new int[1]{10}),
                new Card(Suit.Spades, CardType.Jack,new int[1]{10}),
                new Card(Suit.Hearts, CardType.C10,new int[1]{10}),
                new Card(Suit.Diamonds, CardType.C10,new int[1]{10}),
                new Card(Suit.Clubs, CardType.C10,new int[1]{10}),
                new Card(Suit.Spades, CardType.C10,new int[1]{10}),
                new Card(Suit.Hearts, CardType.C9,new int[1]{9}),
                new Card(Suit.Diamonds, CardType.C9,new int[1]{9}),
                new Card(Suit.Clubs, CardType.C9,new int[1]{9}),
                new Card(Suit.Spades, CardType.C9,new int[1]{9}),
                new Card(Suit.Hearts, CardType.C8,new int[1]{8}),
                new Card(Suit.Diamonds, CardType.C8,new int[1]{8}),
                new Card(Suit.Clubs, CardType.C8,new int[1]{8}),
                new Card(Suit.Spades, CardType.C8,new int[1]{8}),
                new Card(Suit.Hearts, CardType.C7,new int[1]{7}),
                new Card(Suit.Diamonds, CardType.C7,new int[1]{7}),
                new Card(Suit.Clubs, CardType.C7,new int[1]{7}),
                new Card(Suit.Spades, CardType.C7,new int[1]{7}),
                new Card(Suit.Hearts, CardType.C6,new int[1]{6}),
                new Card(Suit.Diamonds, CardType.C6,new int[1]{6}),
                new Card(Suit.Clubs, CardType.C6,new int[1]{6}),
                new Card(Suit.Spades, CardType.C6,new int[1]{6}),
                new Card(Suit.Hearts, CardType.C5,new int[1]{5}),
                new Card(Suit.Diamonds, CardType.C5,new int[1]{5}),
                new Card(Suit.Clubs, CardType.C5,new int[1]{5}),
                new Card(Suit.Spades, CardType.C5,new int[1]{5}),
                new Card(Suit.Hearts, CardType.C4,new int[1]{4}),
                new Card(Suit.Diamonds, CardType.C4,new int[1]{4}),
                new Card(Suit.Clubs, CardType.C4,new int[1]{4}),
                new Card(Suit.Spades, CardType.C4,new int[1]{4}),
                new Card(Suit.Hearts, CardType.C3,new int[1]{3}),
                new Card(Suit.Diamonds, CardType.C3,new int[1]{3}),
                new Card(Suit.Clubs, CardType.C3,new int[1]{3}),
                new Card(Suit.Spades, CardType.C3,new int[1]{3}),
                new Card(Suit.Hearts, CardType.C2,new int[1]{2}),
                new Card(Suit.Diamonds, CardType.C2,new int[1]{2}),
                new Card(Suit.Clubs, CardType.C2,new int[1]{2}),
                new Card(Suit.Spades, CardType.C2,new int[1]{2})
            };
        }
        public List<Card> createDeck() {
            Card[] cards = cardDeck.Select(card => card).ToArray();
            shuffle(cards);
            return new List<Card>(cards);
        }
        private void shuffle<T>(T[] cards) {
            var length = cards.Length - 1;
            T tempCard;
            int randomIndex;
            for (var i = length; i > 0; i--) {
                tempCard = cards[i];
                randomIndex = gen.Next(0, i);
                cards[i] = cards[randomIndex];
                cards[randomIndex] = tempCard;
            }
        }
        public List<Card> crateScenario1() {
            var deck = new Card[6] {
                new Card(Suit.Clubs, CardType.C8,new int[1]{8}),
                new Card(Suit.Clubs, CardType.Jack,new int[1]{10}),
                new Card(Suit.Spades, CardType.C8,new int[1]{8}),
                new Card(Suit.Clubs, CardType.C9,new int[1]{9}),
                new Card(Suit.Spades, CardType.Ace,new int[2]{11, 1}),
                new Card(Suit.Spades, CardType.Queen,new int[1]{10})
            };
            return new List<Card>(deck);
        }
        public List<Card> crateScenario2() {
            var deck = new Card[6]{
                new Card(Suit.Hearts, CardType.Jack,new int[1]{10}),
                new Card(Suit.Clubs, CardType.King,new int[1]{10}),
                new Card(Suit.Spades, CardType.Jack,new int[1]{10}),
                new Card(Suit.Diamonds, CardType.C4,new int[1]{4}),
                new Card(Suit.Diamonds, CardType.C8,new int[1]{8}),
                new Card(Suit.Spades, CardType.C10,new int[1]{10}),
            };
            return new List<Card>(deck);
        }
        public List<Card> crateScenario3() {
            var deck = new Card[6]{
                new Card(Suit.Diamonds, CardType.C4,new int[1]{4}),
                new Card(Suit.Diamonds, CardType.C8,new int[1]{8}),
                new Card(Suit.Hearts, CardType.C3,new int[1]{3}),
                new Card(Suit.Clubs, CardType.Jack,new int[1]{10}),
                new Card(Suit.Spades, CardType.C7,new int[1]{7}),
                new Card(Suit.Clubs, CardType.C5,new int[1]{5})
            };
            return new List<Card>(deck);
        }
        public List<Card> crateScenario4() {
            var deck = new Card[7]{
                new Card(Suit.Spades, CardType.C9,new int[1]{9}),
                new Card(Suit.Clubs, CardType.C9,new int[1]{9}),
                new Card(Suit.Hearts, CardType.C2,new int[1]{2}),
                new Card(Suit.Spades, CardType.C2,new int[1]{2}),
                new Card(Suit.Diamonds, CardType.C5,new int[1]{5}),
                new Card(Suit.Spades, CardType.C7,new int[1]{7}),
                new Card(Suit.Spades, CardType.Ace,new int[2]{11, 1})
            };
            return new List<Card>(deck);
        }
        public List<Card> crateScenario5() {
            var deck = new Card[6]{
                new Card(Suit.Hearts, CardType.C5,new int[1]{5}),
                new Card(Suit.Clubs, CardType.Queen,new int[1]{10}),
                new Card(Suit.Hearts, CardType.C3,new int[1]{3}),
                new Card(Suit.Clubs, CardType.C9,new int[1]{9}),
                new Card(Suit.Hearts, CardType.C4,new int[1]{4}),
                new Card(Suit.Spades, CardType.King,new int[1]{10})
            };
            return new List<Card>(deck);
        }
        public List<Card> crateScenario6() {
            var deck = new Card[6]{
                new Card(Suit.Diamonds, CardType.C10,new int[1]{10}),
                new Card(Suit.Spades, CardType.C2,new int[1]{2}),
                new Card(Suit.Hearts, CardType.Jack,new int[1]{10}),
                new Card(Suit.Hearts, CardType.C3,new int[1]{3}),
                new Card(Suit.Hearts, CardType.King,new int[1]{10}),
                new Card(Suit.Clubs, CardType.C8,new int[1]{8})
            };
            return new List<Card>(deck);
        }
        public List<Card> crateScenario7() {
            var deck = new Card[6]{
                new Card(Suit.Hearts, CardType.Jack,new int[1]{10}),
                new Card(Suit.Spades, CardType.C4,new int[1]{4}),
                new Card(Suit.Clubs, CardType.C2,new int[1]{2}),
                new Card(Suit.Spades, CardType.C2,new int[1]{2}),
                new Card(Suit.Hearts, CardType.Queen,new int[1]{10}),
                new Card(Suit.Clubs, CardType.King,new int[1]{10})
            };
            return new List<Card>(deck);
        }
        public List<Card> crateScenario8() {
            var deck = new Card[6]{
                new Card(Suit.Diamonds, CardType.Jack,new int[1]{10}),
                new Card(Suit.Clubs, CardType.C6,new int[1]{6}),
                new Card(Suit.Hearts, CardType.C4,new int[1]{4}),
                new Card(Suit.Spades, CardType.C8,new int[1]{8}),
                new Card(Suit.Diamonds, CardType.C3,new int[1]{3}),
                new Card(Suit.Hearts, CardType.C4,new int[1]{4})
            };
            return new List<Card>(deck);
        }
        public List<Card> crateScenario9() {
            var deck = new Card[6]{
                new Card(Suit.Clubs, CardType.C8,new int[1]{8}),
                new Card(Suit.Spades, CardType.C8,new int[1]{8}),
                new Card(Suit.Spades, CardType.C9,new int[1]{9}),
                new Card(Suit.Hearts, CardType.C4,new int[1]{4}),
                new Card(Suit.Diamonds, CardType.C10,new int[1]{10}),
                new Card(Suit.Diamonds, CardType.C4,new int[1]{4})
            };
            return new List<Card>(deck);
        }
    }
}