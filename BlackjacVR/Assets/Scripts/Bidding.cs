﻿using UnityEngine;
using System.Collections;

public class Bidding : MonoBehaviour,TimingScript {

	GameObject biddingText;
	GameObject clearButton;
	GameObject dealButton;

	private int bid = 0;

	// Use this for initialization
	void Start () {

	}

	public void HandleTimedInput() {
		if (transform.gameObject == GameObject.Find("clearButton")) {
			PlayerManager.Instance.blackJackManager.GetComponent<Bidding>().clearBid();
		} else if (transform.gameObject == GameObject.Find("dealButton")) {
			PlayerManager.Instance.blackJackManager.GetComponent<Bidding>().dealCards();
		} else {
			if (transform.gameObject ==  GameObject.Find("1RateButton")) {
				PlayerManager.Instance.blackJackManager.GetComponent<Bidding>().makeBide (1);
			} else if (transform.gameObject ==  GameObject.Find("5RateButton")) {
				PlayerManager.Instance.blackJackManager.GetComponent<Bidding>().makeBide (5);
			} else if (transform.gameObject ==  GameObject.Find("20RateButton")) {
				PlayerManager.Instance.blackJackManager.GetComponent<Bidding>().makeBide (20);
			} else if (transform.gameObject ==  GameObject.Find("100RateButton")) {
				PlayerManager.Instance.blackJackManager.GetComponent<Bidding>().makeBide (100);
			} else if (transform.gameObject ==  GameObject.Find("500RateButton")) {
				PlayerManager.Instance.blackJackManager.GetComponent<Bidding>().makeBide (500);
			}
		}
	}

	// Update is called once per frame
	void Update () {
	
	}

	public void startBidding() {
		biddingText = GameObject.Find("BidText");
		dealButton = GameObject.Find("dealButton");
		clearButton = GameObject.Find("clearButton");
		clearBid ();
	}

	//

	public void makeBide(int newBid) {
		if (checkIfPossibleMakeBid(bid + newBid) == false) { return; }
		bid += newBid;
		updateBidText ();
		dealButton.GetComponent<Renderer>().material.color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
		clearButton.GetComponent<Renderer>().material.color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
	}

	public void clearBid() {
		bid = 0;
		updateBidText ();
		clearButton.GetComponent<Renderer>().material.color = new Color(1.0f, 1.0f, 1.0f, 0.5f);
		dealButton.GetComponent<Renderer>().material.color = new Color(1.0f, 1.0f, 1.0f, 0.5f);
	}

	public void dealCards() {
		if (bid == 0) {
			return;
		}

		PlayerManager.Instance.blackJackManager.GetComponent<GameSelectionManager> ().enterGameState (bid: bid);
		clearBid ();
	}

	//Actions

	private bool checkIfPossibleMakeBid(int bid) {
		if (PlayerManager.Instance.getPlayerPoints() > bid) {
			return true;
		} else {
			return false;
		}
	}

	public void updateBidText() {
		biddingText.GetComponent<TextMesh>().text = "Likme: " + bid.ToString();
	}
}
