﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Blackjack;
using System.Linq;

public class BlackjackGame : MonoBehaviour {

	GameObject dealerScore;
	GameObject playerScore;
	GameObject playerBid;
	GameObject dealerCardsGO;
	GameObject playerCardsGO;

	protected Game game;
	public void newGame(Game game, int bid) {
		this.game = game;
		GameObject.Find ("PointUI").transform.Find ("Dealer point UI").gameObject.SetActive (true);
		GameObject.Find ("PointUI").transform.Find ("Player point UI").gameObject.SetActive (true);
		playerCardsGO = GameObject.Find ("Player cards").gameObject;
		dealerCardsGO = GameObject.Find ("Dealer cards").gameObject;

		if (dealerScore == null) {
			dealerScore = GameObject.Find ("Dealer game score text");
			playerScore = GameObject.Find ("Player game score text");
			if (bid != 0) {
				playerBid = GameObject.Find ("Player bid count").gameObject;
			} else {
				playerBid = null;
			}
		}

		dealerScore.GetComponent<TextMesh> ().text = "0";
		playerScore.GetComponent<TextMesh> ().text = "0";
		if (playerBid != null) {
			playerBid.GetComponent<TextMesh> ().text = bid.ToString () + "ƍ";
		}

		game.addChips (bid);
		StartCoroutine (drawNewPlayerCards (game.getPlayerCards ()));
		StartCoroutine (drawNewDealerCards (game.getDealerCards (), false));
		StartCoroutine (initCardActionUpdate ());
	}

	IEnumerator initCardActionUpdate() {
		yield return new WaitForSeconds(1);
		Card dealerCard = game.getDealerCards () [1];
		dealerScore.GetComponent<TextMesh> ().text = dealerCard.values[0].ToString ();
		updateAllAfterAction ();
	}

	IEnumerator drawNewPlayerCards(Card[] cards) {
		for (int i = 0; i < cards.Length; i++) {
			Card card = cards[i];
			GameObject cardGO;
			bool drawWithDelay = false;
			if (playerCardsGO.transform.childCount > i) {
				cardGO = playerCardsGO.transform.GetChild(i).gameObject;
			} else {
				cardGO = cardGameObject (card, true);
				cardGO.transform.localScale = new Vector3 (8.4f, 24.9441f, 13.3f);
				drawWithDelay = true;
			}
			float deltaX = i * 0.04f;
			float deltaY = i * 0.001f;
			float deltaZ = i * 0.004f;
			cardGO.transform.position = new Vector3 (-5.029f + deltaX, 1.568f + deltaY, 0.337f - deltaZ);
			playerScore.GetComponent<TextMesh> ().text = game.countPoints(cards.ToList().GetRange(0,i+1)).ToString();
			if (drawWithDelay) {
				yield return new WaitForSeconds(1);
			}
		}
			
	}

	IEnumerator showDealerCards() {
		GameObject cardGO = dealerCardsGO.transform.GetChild(0).gameObject;
		cardGO.transform.localRotation = Quaternion.Euler (0.0f, 4.9f, 0.0f);
		dealerScore.GetComponent<TextMesh> ().text = game.getDealerPoints().ToString();
		yield return new WaitForSeconds(1);
		StartCoroutine (drawNewDealerCards (game.getDealerCards(), true));
	}

	IEnumerator drawNewDealerCards(Card[] cards, bool showResult) {

		for (int i = 0; i < cards.Length; i++) {
			Card card = cards[i];
			GameObject cardGO;
			bool drawWithDelay = false;

			if (dealerCardsGO.transform.childCount > i) {
				cardGO = dealerCardsGO.transform.GetChild(i).gameObject;
				cardGO.transform.localRotation = Quaternion.Euler (0.0f, 4.9f, 0.0f);
			} else {
				cardGO = cardGameObject (card, false);
				if (i == 0) {
					cardGO.transform.localRotation = Quaternion.Euler (0.0f, 4.9f, 180.0f);
				}
				cardGO.transform.localScale = new Vector3 (10.3f, 24.9441f, 23.2f);
				drawWithDelay = true;
			}

			float deltaX = i * 0.04f;
			float deltaY = i * 0.001f;
			float deltaZ = i * 0.006f;
			cardGO.transform.position = new Vector3 (-4.961f + deltaX, 1.545f + deltaY, 0.996f - deltaZ);

			if (showResult == true) {
				dealerScore.GetComponent<TextMesh> ().text = game.countPoints(cards.ToList().GetRange(0,i+1)).ToString();
			}

			if (drawWithDelay) {
				yield return new WaitForSeconds(1);
			}
		}
	}

	//Update button state

	private void updateGameButtonStates(List<Decision> options) {
		GameObject hitButton = GameObject.Find ("GameButtons").transform.Find ("hitButton").gameObject;
		GameObject stayButton = GameObject.Find ("GameButtons").transform.Find ("standButton").gameObject;
		GameObject doubleButton = GameObject.Find ("GameButtons").transform.Find ("doubleButton").gameObject;

		hitButton.GetComponent<Renderer>().material.color = new Color(1.0f, 1.0f, 1.0f, 0.5f);
		stayButton.GetComponent<Renderer>().material.color = new Color(1.0f, 1.0f, 1.0f, 0.5f);
		doubleButton.GetComponent<Renderer>().material.color = new Color(1.0f, 1.0f, 1.0f, 0.5f);

		foreach (Decision option in options) {
			switch (option){
			case Decision.Hit:
				hitButton.GetComponent<Renderer>().material.color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
				break;
			case Decision.Double:
				doubleButton.GetComponent<Renderer>().material.color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
				break;
			case Decision.Stand:
				stayButton.GetComponent<Renderer>().material.color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
				break;
			default:
				break;
			}
		}
	}

	//Getting card game object

	private GameObject cardGameObject(Card card, bool isPlayerCard) {
		GameObject parent;
		if (isPlayerCard == true) {
			parent = playerCardsGO;
		} else { 
			parent = dealerCardsGO;
		}
		string path = "Blackjack/cards_fbx/" + getCardGameObjectTitle (card);
		GameObject prefab = Resources.Load (path, typeof(GameObject)) as GameObject;
		GameObject clone = Instantiate(prefab, Vector3.zero, Quaternion.Euler (0.0f, 4.9f, 0.0f), parent.transform) as GameObject;
		return clone;
	}

	//Card object title

	private string getCardGameObjectTitle(Card card) {
		string title = "";
		switch (card.type) {
			case CardType.Ace:
				title = "ace";
				break;
			case CardType.King:
				title = "king";
				break;
			case CardType.Queen:
				title = "queen";
				break;
			case CardType.Jack:
				title = "jack";
				break;
			case CardType.C10:
				title = "ten";
				break;
			case CardType.C9:
				title = "nine";
				break;
			case CardType.C8:
				title = "eight";
				break;
			case CardType.C7:
				title = "seven";
				break;
			case CardType.C6:
				title = "six";
				break;
			case CardType.C5:
				title = "five";
				break;
			case CardType.C4:
				title = "four";
				break;
			case CardType.C3:
				title = "three";
				break;
			case CardType.C2:
				title = "two";
				break;
			default: break;	
		}

		switch (card.suit) {
		case Suit.Clubs:		
			title += "cubs";
			break;
		case Suit.Diamonds:
			title += "diamonds";
			break;
		case Suit.Hearts:
			title += "hearts";
			break;
		case Suit.Spades:
			title += "spades";
			break;
		default:
			break;
		}

		return title;
	}


	//States

	public void clearObjects() {
		GameObject playerBackground = GameObject.Find ("Player score bg").gameObject;
		Material newMat = Resources.Load("Textures/Black", typeof(Material)) as Material;
		playerBackground.GetComponent<Renderer> ().material = newMat;
		GameObject.Find ("PointUI").transform.Find ("Dealer point UI").gameObject.SetActive (false);
		GameObject.Find ("PointUI").transform.Find ("Player point UI").gameObject.SetActive (false);

		foreach (Transform childTransform in dealerCardsGO.transform) Destroy(childTransform.gameObject);
		foreach (Transform childTransform in playerCardsGO.transform) Destroy(childTransform.gameObject);
	}

	IEnumerator playerDidWonGame() {
		GameObject playerBackground = GameObject.Find ("Player score bg").gameObject;
		Material newMat = Resources.Load("Textures/WinMaterial", typeof(Material)) as Material;
		playerBackground.GetComponent<Renderer> ().material = newMat;
		yield return new WaitForSeconds(5);
		StartCoroutine(endGame ());
	}

	IEnumerator playerDidLostGame() {
		GameObject playerBackground = GameObject.Find ("Player score bg").gameObject;
		Material newMat = Resources.Load("Textures/LoseMaterial", typeof(Material)) as Material;
		playerBackground.GetComponent<Renderer> ().material = newMat;
		yield return new WaitForSeconds(5);
		StartCoroutine(endGame ());
	}

	IEnumerator playerPlayDrawGame() {
		GameObject playerBackground = GameObject.Find ("Player score bg").gameObject;
		Material newMat = Resources.Load("Textures/DrawMaterial", typeof(Material)) as Material;
		playerBackground.GetComponent<Renderer> ().material = newMat;
		yield return new WaitForSeconds(5);
		StartCoroutine(endGame ());
	}

	protected virtual IEnumerator endGame() {
		clearObjects ();
		yield return new WaitForEndOfFrame ();
		PlayerManager.Instance.blackJackManager.GetComponent<GameSelectionManager> ().enterBidsState ();
	}

	IEnumerator playerDidFinishGame(GameResult result) {
		yield return new WaitForSeconds(0);
		if (result.type != WinType.Draw) {
			if (result.winner == User.Player) {
				StartCoroutine (playerDidWonGame ());
				PlayerManager.Instance.updatePlayerStats ((int)game.getCurrentChips ());
			} else {
				PlayerManager.Instance.updatePlayerStats (- (int)game.getCurrentChips ());
				StartCoroutine (playerDidLostGame ());
			}
		} else {
			StartCoroutine (playerDidLostGame ());
		}

	}

	private void updateAllAfterAction() {
		updateGameButtonStates (game.getDecisions());

		if (game.isGameOver()) {
			var result = game.getGameResult ();
			//StartCoroutine (showDealerCards ());
			StartCoroutine (playerDidFinishGame (result));
		} 
	}
	//Actions


	public void hit() {
	    game.playerHit ();
		StartCoroutine (drawNewPlayerCards (game.getPlayerCards()));
		updateAllAfterAction ();
	}

	public void stand() {
		game.playerStands ();
		StartCoroutine (showDealerCards ());
		updateAllAfterAction ();
	}

	public void doubleAction() {
		game.playerDouble ();
		if (playerBid != null) {
			playerBid.GetComponent<TextMesh> ().text = game.getCurrentChips ().ToString () + "ƍ";
		}
		StartCoroutine (drawNewPlayerCards (game.getPlayerCards()));
		StartCoroutine (showDealerCards ());
		updateAllAfterAction ();
	}
}
