﻿using UnityEngine;
using System.Collections;

public class GameButtonsSelection : MonoBehaviour, TimingScript {

	// Use this for initialization

	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

	}

	public void HandleTimedInput() {
		if (gameObject.GetComponent<Renderer>().material.color.a == 0.5) {
			return;
		}
		
		GameObject hitButton = GameObject.Find ("GameButtons").transform.Find ("hitButton").gameObject;
		GameObject stayButton = GameObject.Find ("GameButtons").transform.Find ("standButton").gameObject;
		GameObject doubleButton = GameObject.Find ("GameButtons").transform.Find ("doubleButton").gameObject;

		if (gameObject == hitButton) {
			PlayerManager.Instance.blackJackManager.transform.GetComponent<GameSelectionManager> ().currentGame.hit ();
		} else if (gameObject == stayButton) {
			PlayerManager.Instance.blackJackManager.transform.GetComponent<GameSelectionManager>().currentGame.stand();
		} else if (gameObject == doubleButton) {
			PlayerManager.Instance.blackJackManager.transform.GetComponent<GameSelectionManager>().currentGame.doubleAction();
		}
	}
}
