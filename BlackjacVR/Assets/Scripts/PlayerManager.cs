﻿using UnityEngine;
using System.Collections;
using Blackjack;

public enum PlayerState
{
	lobby,
	table,
	bids,
	game,
	tutorial
}

public class PlayerManager : Singleton<PlayerManager> {


	protected PlayerManager () {} // guarantee this will be always a singleton only - can't use the constructor!
	private int playerMoney = 5000;
	public Deck deck = new Deck();
	public PlayerState state = PlayerState.lobby;
	public GameObject blackJackManager;
	public GameObject player;
	public GameObject mainDealer;

	private GameObject playerScoreText;
		// Use this for initialization
	void Start () {
		blackJackManager = GameObject.Find("Player chair");
		mainDealer = GameObject.Find("Game dealer");
	    player = GameObject.Find("Player");
		playerScoreText = GameObject.Find("Player score text");
		updatePlayerStats (0);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public int getPlayerPoints() {
		return playerMoney;
	}

	public void updatePlayerStats(int deltaPoints) {
		playerMoney += deltaPoints;
		playerScoreText.transform.GetComponent<TextMesh> ().text = "Rezultāts: " + playerMoney.ToString () + "ƍ";
	}


}
