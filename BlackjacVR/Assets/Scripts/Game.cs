using Blackjack;
using System.Collections.Generic;
using System.Linq;

namespace Blackjack {
    public enum User {
        Player = 1,
        Dealer,
        None
    }
    public enum WinType {
        Standard = 1,
        Blackjack,
        Draw
    }
    public enum Decision {
        Hit = 1,
        Stand,
        Double,
        Split
    }
    public struct GameResult {
        public WinType type;
        public User winner;
        public int delaerPoints;
        public int playerPoints;
		public double winAmount;
		public GameResult(User winner, WinType type, int delaerPoints, int playerPoints, double winAmount) {
            this.winner = winner;
            this.type = type;
            this.delaerPoints = delaerPoints;
            this.playerPoints = playerPoints;
            this.winAmount = winAmount;
        }
    }
    public class Game {
        List<Card> deck;
        List<Card> dealerCards;
        List<Card> playerCards;
		double chips;
        bool canDouble;
        bool playerOver;
        int playerPoints;
        int dealerPoints;
        public Game(List<Card> deck) {
            this.deck = deck;
            chips = 0;
            playerOver = false;
            canDouble = true;
			playerCards = new List<Card> {
                takeNextCard(),
                takeNextCard()
            };
			playerPoints = countPoints (playerCards);
            dealerCards = new List<Card> {
                takeNextCard(),
                takeNextCard()
            };
			dealerPoints = countPoints (dealerCards);
        }
        public List<Decision> getDecisions() {
            List<Decision> decisions = new List<Decision>();
            if (!playerOver) {
                if (canDouble) {
                    decisions.Add(Decision.Double);
                }
                if (playerPoints <= 21) {
                    decisions.Add(Decision.Hit);
                }
                decisions.Add(Decision.Stand);
            }
            return decisions;
        }
        public bool isGameOver() {
            return playerOver;
        }
        public Card[] getDealerCards() {
            return dealerCards.Select(card => card).ToArray();
        }
        public Card[] getPlayerCards() {
            return playerCards.Select(card => card).ToArray();
        }
        public GameResult getGameResult() {
            User winner;
            WinType type;
			double winnings;
            if (dealerPoints == 21 && playerPoints == 21) {
                winner = User.None;
                type = WinType.Draw;
                winnings = chips;
            } else if(dealerPoints == 21 && playerPoints != 21) {
                winner = User.Dealer;
                type = WinType.Blackjack;
                winnings = 0;
            } else if(dealerPoints != 21 && playerPoints == 21) {
                winner = User.Player;
                type = WinType.Blackjack;
                winnings = chips + chips * 1.5;
            } else if(dealerPoints > 21 && playerPoints > 21) {
                winner = User.None;
                type = WinType.Draw;
                winnings = chips;
            } else if(dealerPoints > 21 && playerPoints < 21) {
                winner = User.Player;
                type = WinType.Standard;
                winnings = chips * 2;
            } else if(dealerPoints < 21 && playerPoints > 21) {
                winner = User.Dealer;
                type = WinType.Standard;
                winnings = 0;
            } else if(dealerPoints > playerPoints) {
                winner = User.Dealer;
                type = WinType.Standard;
                winnings = 0;
            } else {
                winner = User.Player;
                type = WinType.Standard;
                winnings = chips * 2;
            }
            return new GameResult(winner, type, dealerPoints, playerPoints, winnings);
        }
        public int getPlayerPoints() {
            return playerPoints;
        }
		public int getDealerPoints() {
			return dealerPoints;
		}
        // Decisions
        public Card playerHit() {
            canDouble = false;
            Card card = takeNextCard();
            playerCards.Add(card);
            playerPoints = countPoints(playerCards);
            if (playerPoints >= 21) {
                playerOver = true;
				//Man škiet, ka nevajag neko darīt, ja spēlētājs jau ir zaudējis. Vismaz tādus noteikumus atradu i-netā.
                //dealerTurn();
            }
            return card;
        }
        public void playerStands() {
            playerOver = true;
            playerPoints = countPoints(playerCards);
            dealerTurn();
        }
        public void playerDouble() {
            if (canDouble && !playerOver) {
                 chips *= 2;
                 canDouble = false;
                 playerCards.Add(takeNextCard());
                 playerPoints = countPoints(playerCards);
                 playerOver = true;
                 dealerTurn();
            }
        }
		public void addChips(double value) {
            chips += value;
        }
		public void removeChips(double value) {
            chips -= value;
            if (chips < 0) {
                chips = 0;
            }
        }
		public double getCurrentChips() {
            return chips;
        }
        //private methods
        private Card takeNextCard() {
            Card card = deck[0];
            deck.RemoveAt(0);
            return card;
        }
        public int countPoints(List<Card> cards) {
            int result = 0;
            List<int[]> aces= new List<int[]>();
            foreach (Card card in cards) {
                if (card.type == CardType.Ace) {
                    aces.Add(card.values);
                } else {
                    result += card.values[0];
                }
            }
            foreach (int[] aceValues in aces) {
                if (result + aceValues[0] < 21) {
                    result += aceValues[0];
                } else {
                    result += aceValues[1];
                }
            }
            return result;
        }
        private void dealerTurn() {
            while(dealerPoints < 17) {
                dealerCards.Add(takeNextCard());
                dealerPoints = countPoints(dealerCards);
            }
        }
    }
}